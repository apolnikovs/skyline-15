# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.154');


# ---------------------------------------------------------------------- #
# Modify table "appointment"                                                     #
# ---------------------------------------------------------------------- #
ALTER TABLE `appointment` ADD COLUMN `Latitude` FLOAT(6,4) NULL DEFAULT NULL AFTER `CompletionDateTime`;
ALTER TABLE `appointment`  ADD COLUMN `Longitude` FLOAT(7,4) NULL DEFAULT NULL AFTER `Latitude`;




# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.155');




