<?php

require_once('CustomModel.class.php');
require_once('TableFactory.class.php');

/**
 * Description
 *
 * This class is used for handling database actions of Stock Page in Lookup Tables section under System Admin
 *
 * @author      Andris Polnikovs <a.polnikovs@gmail.com>
 * @version     1.0
 */
class StockControl extends CustomModel {

    public function __construct($controller) {

        parent::__construct($controller);

        $this->conn = $this->Connect($this->controller->config['DataBase']['Conn'], $this->controller->config['DataBase']['Username'], $this->controller->config['DataBase']['Password']);
        $this->SQLGen = $this->controller->loadModel('SQLGenerator');
        $this->fields = [
            "ServiceProviderID",
            "PartNumber",
            "PurchaseCost",
            "Description",
            "MinStock",
            "MakeUpTo",
            "ServiceProviderColourID",
            "PrimaryServiceProviderModelID",
            "DefaultServiceProviderSupplierID",
            "Sundry",
            "OEMPartNo",
            "OEMPartDesc",
            "InitialQty",
            "ShelfLocation",
            "BinLocation",
            "PartUsageLimit",
            "Accessory",
            "AccessoryCost",
            "AccessoryWarrantyPeriod",
            "AccessorySerialNumber",
            "AllowDuplicatePartOnExchange",
            "RetailItem",
            "SuspendPart",
            "RedundantPart",
            "ReturnFaultySpare",
            "ChargeablePartOnly",
            "AttachBySolder",
            "VATExempt",
            "SerialisedStock",
            "ShelfLifeExpiryDate",
            "UseStockLevelTrigger",
            "UseAutomaticReOrderPeriod",
            "AutoReOrderPartEvery",
            "AutoReOrderQty",
            "PercentageMarkupMainStore",
            "UsePercentageMarkupForWarranty",
            "WarrantyPercentageMarkupValue",
            "SalesPrice",
            "UsePercentageMarkupForChargeable",
            "TradeCost",
            "ChargeablePercentageMarkupValue",
            "VATRateRepair",
            "VATRateSale",
            "Status"
        ];
    }

    ////stock functions 
    public function getStockList($utID = false) {

        $sql = "select * from stock where Status='Active'";

        $res = $this->query($this->conn, $sql);
        return $res;
    }

    public function insertPartStockTemplate($P, $spid) {
        if (!isset($P["DefaultServiceProviderSupplierID"])) {
            $P["DefaultServiceProviderSupplierID"] = $P['ServiceProviderSupplierID'];
        }
        $val =
                [
                    $spid,
                    $P["PartNumber"],
                    $P["PurchaseCost"],
                    $P["Description"],
                    $P["MinStock"],
                    $P["MakeUpTo"],
                    $P["ServiceProviderColourID"],
                    $P["PrimaryServiceProviderModelID"],
                    $P["DefaultServiceProviderSupplierID"],
                    isset($P["Status"]) ? "In-active" : "Active"
        ];
        $id = $this->SQLGen->dbInsert('sp_part_stock_template', $this->fields, $val);
        return $id;
    }

    public function updatePartStockTemplate($P, $spid) {
        if (!isset($P["DefaultServiceProviderSupplierID"])) {
            $P["DefaultServiceProviderSupplierID"] = $P['ServiceProviderSupplierID'];
        }
        $val =
                [
                    $spid,
                    $P["PartNumber"],
                    $P["PurchaseCost"],
                    $P["Description"],
                    $P["MinStock"],
                    $P["MakeUpTo"],
                    $P["ServiceProviderColourID"],
                    $P["PrimaryServiceProviderModelID"],
                    $P["DefaultServiceProviderSupplierID"],
                    isset($P["Status"]) ? "In-active" : "Active"
        ];
        $this->SQLGen->dbUpdate('sp_part_stock_template', $this->fields, $val, "SpPartStockTemplateID=" . $P['SpPartStockTemplateID']);
    }

    //fetch all infromation for part stock template
    public function getAllTemplateData($id, $spid) {
        $sql = "select spst.*,sps.CompanyName from sp_part_stock_template spst
            left join service_provider_supplier sps on sps.ServiceProviderSupplierID=spst.DefaultServiceProviderSupplierID
        where SpPartStockTemplateID=$id and spst.ServiceProviderID=$spid";
        $res = $this->Query($this->conn, $sql);


        if (isset($res[0])) {
            return $res[0];
        } else {
            false;
        }
    }

    public function deleteStock($id) {
        $this->SQLGen->dbMakeInactive('sp_part_stock_template', "SpPartStockTemplateID=$id");
    }

    public function checkOrderNo($OrderNo, $spid) {
        $sql = "select * from sp_part_order spo 
           left join sp_part_stock_item spsi on spsi.SPPartOrderID=spo.SPPartOrderID
           left join part p on p.PartID=spsi.PartID
          left  join sp_part_stock_template spst on spst.SpPartStockTemplateID=spsi.SPPartStockTemplateID
          where spo.OrderNo='$OrderNo' and spo.ServiceProviderID=$spid
            ";
        $res = $this->Query($this->conn, $sql);

        if (isset($res[0])) {
            return $res[0];
        } else {
            false;
        }
    }

    public function getSpPartTemplates($spid) {
        $sql = "select * from sp_part_stock_template where ServiceProviderID=$spid and Status='Active'";
        $res = $this->Query($this->conn, $sql);
        return $res;
    }

    public function getSpPartTemplateData($spid, $number) {
        $sql = "select * from sp_part_stock_template
            
                where ServiceProviderID=$spid and PartNumber='$number'";
        $res = $this->Query($this->conn, $sql);
        if (isset($res[0])) {
            return $res[0];
        } else {
            false;
        }
    }

    public function getSpPartTemplateDataFromId($spid, $id) {
        $sql = "select * from sp_part_stock_template
            
                where ServiceProviderID=$spid and SpPartStockTemplateID='$id'";
        $res = $this->Query($this->conn, $sql);
        if (isset($res[0])) {
            return $res[0];
        } else {
            false;
        }
    }

    //this is used when receiving order witch is not linked to any jobs
    public function insertStockItem($spid, $p, $statusID = false, $orderID = false) {
        if (!$orderID) {
            //check if order exists
            if (!$orderID = $this->checkOrderNo($p['OrderNo'], $spid)) {
                //if no create one

                $fields = [
                    "ServiceProviderSupplierID",
                    "OrderNo",
                    "ReceviedDate",
                    "Received",
                    "ReceivedByUserID",
                    "ServiceProviderID",
                ];

                $val = [
                    $p["ServiceProviderSupplierID"],
                    $p["OrderNo"],
                    date('Y-m-d H:i:s'),
                    "Y",
                    $this->controller->user->UserID,
                    $spid
                ];
                $orderID = $this->SQLGen->dbInsert('sp_part_order', $fields, $val);
            } else {
                $orderID = $orderID['SPPartOrderID'];
            }
        }


        //check if stock template exist exist
        if (!$template_ID = $this->getSpPartTemplateData($spid, $p['PartNumber'])) {
            //if no create one
            $template_ID = $this->insertPartStockTemplate($p, $spid);
        } else {
            $template_ID = $template_ID['SpPartStockTemplateID'];
        }

        if (!$statusID) {
            $statusID = 1;
        }

        //inserting items and creating history entry
        for ($i = 0; $i < $p['qty']; $i++) {
            $f = [
                "SPPartStockTemplateID",
                "SPPartStatusID",
                "PurchaseCost",
                "SPPartOrderID"
            ];
            $v = [
                $template_ID,
                $statusID,
                $p['PurchaseCost'],
                $orderID
            ];
            $itemID = $this->SQLGen->dbInsert('sp_part_stock_item', $f, $v);


            //add history record
            $this->insertStockItemHistory($itemID, $statusID, $p['OrderNo']);
        }
    }

    //decrement parts from stock
    public function makePartsUsed($p) {
        $limit = $p['Quantity'];
        if ($limit == '') {
            $limit = 0;
        }
        if ($p['PartID'] == "") {
            //insert part

            $sql = "select * from sp_part_stock_item s 
           where 
           s.SPPartStockTemplateID=:SPPartStockTemplateID 
          
           and SPPartStatusID in (1,9) limit $limit";

            $param = [
                "SPPartStockTemplateID" => $p['SpPartStockTemplateID']
            ];

            $res = $this->Query($this->conn, $sql, $param);

            foreach ($res as $r) {

                //set item status
                $this->updateStockPartItemStatus($r['SPPartStockItem'], 11, $p['JobID']);
                //add history record
                $this->insertStockItemHistory($r['SPPartStockItem'], 11, $p['JobID']);
            }
            if (sizeof($res) < $p['Quantity']) {
                //order missing parts
                $orderNeeded = $p['Quantity'] - sizeof($res); //getting mising parts count

                $pa = [
                    'qty' => $orderNeeded,
                    'SpPartStockTemplateID' => $p['SpPartStockTemplateID'],
                    'JobID' => $p['JobID']
                ];

                return true; //part items cretaed with status 2 parts order required;
            }
            return true; //no parts ordering required
        } else {
            //update part
            $jobid = $p['JobID'];

            $sql = "select count(*) as c from sp_part_stock_item spsh where spsh.JobID=$jobid and spsh.SPPartStatusID=11 and SPPartStockTemplateID=:SPPartStockTemplateID ";
            $param = [
                "SPPartStockTemplateID" => $p['SpPartStockTemplateID']
            ];
            $res = $this->Query($this->conn, $sql, $param);
            $dif = $res[0]['c'] - $limit; //getting difrence between parts already used ont this job and parts used after update
            if ($dif < 0) {
                $limit = $dif * -1;
                //more parts being used
                $sql = "select * from sp_part_stock_item s 
           where 
           s.SPPartStockTemplateID=:SPPartStockTemplateID 
           and JobID is null
           and SPPartStatusID in (1,9) limit $limit";

                $param = [
                    "SPPartStockTemplateID" => $p['SpPartStockTemplateID']
                ];
                //$this->controller->log($_POST);
                $res = $this->Query($this->conn, $sql, $param);
                if (sizeof($res) == $limit) {//check if selected enought parts
                    foreach ($res as $r) {
                        //set item status
                        $this->updateStockPartItemStatus($r['SPPartStockItem'], 11, $p['JobID']);
                        //add history record
                        $this->insertStockItemHistory($r['SPPartStockItem'], 11, $p['JobID']);
                    }
                    return true;
                } else {
                    return false;
                }
            }


            if ($dif > 0) {
                //less parts used
                $limit = $dif;
                //more parts being used
                $sql = "select * from sp_part_stock_item s 
           where 
           s.SPPartStockTemplateID=:SPPartStockTemplateID 
           and JobID=:JobID
           and SPPartStatusID=11 limit $limit";

                $param = [
                    "SPPartStockTemplateID" => $p['SpPartStockTemplateID'],
                    "JobID" => $p['JobID'],
                ];

                $res = $this->Query($this->conn, $sql, $param);

                foreach ($res as $r) {

                    //set item status
                    $this->updateStockPartItemStatus($r['SPPartStockItem'], 1, $p['JobID']);
                    //add history record
                    $this->insertStockItemHistory($r['SPPartStockItem'], 1, $p['JobID']);
                }
            }
            return true;
        }
    }

    //if part deleted from job need make it available in stock
    public function puttPartBackToStock($partID, $JobID) {
        $sql = "select SpPartStockTemplateID,Quantity,PartStatusID from part where PartID=$partID";

        $resPart = $this->Query($this->conn, $sql);
        $limit = $resPart[0]['Quantity'];
        $sql = "select * from sp_part_stock_item where SPPartStockTemplateID=:SPPartStockTemplateID and SPPartStatusID=:PartStatusID and JobID=:JobID limit $limit";
        $param = [
            "SPPartStockTemplateID" => $resPart[0]['SpPartStockTemplateID'],
            "JobID" => $JobID,
            "PartStatusID" => $resPart[0]['PartStatusID'],
        ];

        $res = $this->Query($this->conn, $sql, $param);
        switch ($resPart[0]['PartStatusID']) {

            case 11: {

                    foreach ($res as $r) {

                        //set item status
                        $this->updateStockPartItemStatus($r['SPPartStockItem'], 1);
                        //add history record
                        $this->insertStockItemHistory($r['SPPartStockItem'], 1, $r['JobID']);
                    }
                    break;
                }
            case 3:
            case 2: {
                    //if part with status 02 order required is deleted from job, delete any items with same status 
                    foreach ($res as $r) {
                        $sql = "delete from sp_part_stock_history where SPPartStockItemID=:SPPartStockItem";

                        $params = [
                            "SPPartStockItem" => $r['SPPartStockItem']
                        ];
                        $this->Execute($this->conn, $sql, $params);
                        $sql = "delete from sp_part_order where SPPartOrderID=(select SPPartOrderID from sp_part_stock_item where SPPartStockItem=:SPPartStockItem)";
                        $this->Execute($this->conn, $sql, $params);
                        $sql = "delete from sp_part_stock_item where SPPartStockItem=:SPPartStockItem";
                        $this->Execute($this->conn, $sql, $params);
                    }
                    break;
                }
        }
    }

    public function getOrderHistoryData($id, $sp) {
        $sql = "select *,concat_ws(' ',u.ContactFirstName,u.ContactLastName) as ReceivedBy,
            date_format(spo.CreatedDate,'%d/%m/%Y (%H:%i)') as ReceivedDate
            from sp_part_order  spo 
            join service_provider_supplier sps on sps.ServiceProviderID=spo.ServiceProviderID
            join user u on u.UserID=spo.ReceivedByUserID
            where SPPartOrderID=$id and spo.ServiceProviderID=$sp";
        $res = $this->Query($this->conn, $sql);
        return $res;
    }

    public function getStockItemQty($id) {
        if ($id == '') {
            return 999999; //if part is inserted manualy(part template id is not set) ignore qty check
        }
        $sql = "select count(*) as available from sp_part_stock_item sppsi 
                join part_status sps on sps.PartStatusID=sppsi.SPPartStatusID
                where 
                sppsi.SPPartStockTemplateID=$id
                and sps.Available='Y' and sps.InStock='Y'";
        $res = $this->Query($this->conn, $sql);
        if (isset($res[0])) {
            return $res[0]['available'];
            //return 1;
        } else {
            return 0;
        }
    }

    public function getExportData($type, $sp, $dFrom = false, $dTo = false) {

        $dbtmp = explode(';', $this->controller->config['DataBase']['Conn']);
        $db = substr($dbtmp[1], 7);
        $dbhost = substr($dbtmp[0], 11);
        $dbc = mysql_connect($dbhost, $this->controller->config['DataBase']['Username'], $this->controller->config['DataBase']['Password']) or die(mysql_error());
        mysql_select_db($db);


        if ($type == 2) {
            $where = " and (select count(*) from sp_part_stock_item sppsi 
                join part_status sps on sps.PartStatusID=sppsi.SPPartStatusID
                where 
                sppsi.SPPartStockTemplateID=st.SpPartStockTemplateID
                 and sps.InStock='Y'    
                ) <= st.MinStock and st.MinStock!='0'";
        } else {
            $where = "";
        }

        if ($type == 2 || $type == 1) {
            $q = "select 
            st.PartNumber as `Stock No`,
            st.Description as `Description`,
            (select count(*) from sp_part_stock_item sppsi 
                join part_status sps on sps.PartStatusID=sppsi.SPPartStatusID
                where 
                sppsi.SPPartStockTemplateID=st.SpPartStockTemplateID
                 and sps.InStock='Y'    
                )as `In Stock`,
            st.MinStock as `Minimum Level`,
            st.MakeUpTo as `Make up To`,
            (select PurchaseCost from sp_part_stock_item spsi where spsi.SPPartStockTemplateID=st.SpPartStockTemplateID order by spsi.CreatedDate desc limit 1) as `Last Purchase Cost`,
            m.ModelNumber as `Model`,
            c.ColourName as `Colour`,
            
            (if((st.MakeUpTo-(select count(*) from sp_part_stock_item sppsi 		
                join part_status sps on sps.PartStatusID=sppsi.SPPartStatusID		
                where 		
                sppsi.SPPartStockTemplateID=st.SpPartStockTemplateID		
                 and sps.InStock='Y'    		
                ))<0	,0,	st.MakeUpTo-(select count(*) from sp_part_stock_item sppsi 		
                join part_status sps on sps.PartStatusID=sppsi.SPPartStatusID		
                where 		
                sppsi.SPPartStockTemplateID=st.SpPartStockTemplateID		
                 and sps.InStock='Y'    		
                ))
                ) as `Reorder Quantity`	
     from sp_part_stock_template st
    left join service_provider_model m on m.ServiceProviderModelID=st.PrimaryServiceProviderModelID
    left join service_provider_colour c on c.ServiceProviderColourID=st.ServiceProviderColourID
     WHERE st.Status='Active' AND st.ServiceProviderID=$sp $where";
        }

        if ($type == 3) {

            $timestamp = strtotime(str_replace('/', '.', $dFrom));
            $dFrom = date('Y-m-d', $timestamp);
            $timestamp = strtotime(str_replace('/', '.', $dTo));
            $dTo = date('Y-m-d', $timestamp);



            $q = "select 
              PartNumber as `Stock Code`,
              Description as `Description`
              ,(select count(*) from sp_part_stock_item s 
join sp_part_stock_history sh on sh.SPPartStockItemID=s.SPPartStockItem
where  s.SPPartStockTemplateID=st.SpPartStockTemplateID
and s.SPPartStatusID in(11) 
and s.JobID is not null 
and sh.DateTime>='$dFrom'
and sh.DateTime<='$dTo' ) as `Qty Used`
              from sp_part_stock_template st";
        }



        $qr = mysql_query($q) or die(mysql_error());
        //$qr=$this->Query($this->conn, $q); //this used for xls export
        return $qr;
    }

    public function checkStockAvailable($spid) {
        $sql = "select * from sp_part_stock_template where ServiceProviderID =$spid";
        $res = $this->Query($this->conn, $sql);
        if (isset($res[0])) {
            return 'true';
            //return 1;
        } else {
            return false;
        }
    }

    ///creating single items for inserted part
    //spid
    //p=array(qty,SpPartStockTemplateID,$JobID)
    public function insertStockOrderReqItem($spid, $p, $partid = false) {
        //check if stock template exist exist
        $template_ID = $p['SpPartStockTemplateID'];
        //if no create one
        //inserting items and creating history entry
        for ($i = 0; $i < $p['qty']; $i++) {
            $f = [
                "SPPartStockTemplateID",
                "SPPartStatusID",
                "PurchaseCost",
                "SPPartOrderID",
                "JobID"
            ];
            if ($partid) {
                $f[] = "PartID";
            }
            if (!isset($p['SPPartOrderID'])) {
                $p['SPPartOrderID'] = null;
            }
            $v = [
                $template_ID,
                2,
                isset($p['PurchaseCost']) ? $p['PurchaseCost'] : null,
                $p['SPPartOrderID'],
                $p['JobID']
            ];
            if ($partid) {
                $v[] = $partid;
            }
            $itemID = $this->SQLGen->dbInsert('sp_part_stock_item', $f, $v);
            //history

            $this->insertStockItemHistory($itemID, 2, $p['JobID']);
        }
    }

    //gets list of statuses user can chnage in status dropdown for ordering table returns array
    public function getOrderingStatuses() {
        $sql = "select * from part_status where PartStatusID in (2,6)";
        return $this->Query($this->conn, $sql);
    }

    //gets list of all statuses that can be in pending order table for ordering table returns array
    public function getAllOrderingStatuses() {
        $sql = "select * from part_status where PartStatusID in (2,6,18,19)";
        return $this->Query($this->conn, $sql);
    }

    //used to change part status
    //arg1=array of parts ids
    //arg2=part status id

    public function changePartsStatus($p, $s) {
        $part_model = $this->controller->loadModel('Part');


        foreach ($p as $f) {
            $part_data = $part_model->getPartByID($f);
            if ($f != "0") {
                $items = $this->getAllPartItemsByPartID($f);
            } else {
                break;
            }

            //updating stock item status
            foreach ($items as $t) {

                if (in_array($s, [6, 2])) {
                    $sql = "update sp_part_stock_item spsi set SPPartRequisitionID=null where SPPartStockItem=" . $t['SPPartStockItem'];
                    $this->Execute($this->conn, $sql);
                }


                //set item status
                $this->updateStockPartItemStatus($t['SPPartStockItem'], $s, false, false);
                //add history record
                $this->insertStockItemHistory($t['SPPartStockItem'], $s, $part_data['JobID']);
            }

            //updating parts
            switch ($s) {
                case 2: {
                        $sql = "update part set PartStatusID=:PartStatusID,SPPartRequisitionID=null where PartID=:PartID";
                        break;
                    }
                case 6: {
                        $sql = "update part set PartStatusID=:PartStatusID,Quantity=0,SPPartRequisitionID=null where PartID=:PartID";
                        //if part no more available change open job status to 98 
                        $job_model = $this->controller->loadModel('Job');
                        $job_model->setJobStatusByID($part_data['JobID'], 48);
                        break;
                    }
                default:
                    $sql = "update part set PartStatusID=:PartStatusID where PartID=:PartID";
                    break;
            }

            $params = [
                "PartStatusID" => $s,
                "PartID" => $f,
            ];
            $this->Execute($this->conn, $sql, $params);
            //updating stockItemsStatuses
        }
    }

    //used to insert history record for items status change
    //arg1=part stock item id
    //arg2=part status id
    //arg3=reference number

    public function insertStockItemHistory($i, $s, $r = null) {
        $f = [
            "SPPartStockItemID", "SPPartStatusID", "UserID", "Reference", "DateTime"
        ];
        $v = [
            $i, $s, $this->controller->user->UserID, $r, date('Y-m-d H:i:s')
        ];

        $this->SQLGen->dbInsert('sp_part_stock_history', $f, $v, false);
    }

    //used to insert history record for items status change
    //arg1=part stock item id
    //arg2=part status id
    //arg3=jobid
    //arg4=orderid
    //arg5=requisitionid
    //arg6=receiving history id
    public function updateStockPartItemStatus($i, $s, $j = false, $o = false, $r = false, $h = false) {
        $params = [
            "SPPartStockItem" => $i,
            "SPPartStatusID" => $s
        ];
        $job = "";
        $order = "";
        $req = "";
        if ($j) {
            $job = ",JobID=:JobID";
            $params["JobID"] = $j;
        }
        if ($o) {
            $order = ",SPPartOrderID=:SPPartOrderID";
            $params["SPPartOrderID"] = $o;
        }
        if ($r) {
            $req = ",SPPartRequisitionID=:SPPartRequisitionID";
            $params["SPPartRequisitionID"] = $r;
        }
        if ($h) {
            $req = ",SpStockReceivingHistoryID=:SpStockReceivingHistoryID";
            $params["SpStockReceivingHistoryID"] = $h;
        }
        $sql = "update sp_part_stock_item set SPPartStatusID=:SPPartStatusID $job $order $req where SPPartStockItem=:SPPartStockItem";

        $this->Execute($this->conn, $sql, $params);
    }

///getting all template data
    public function getAllPartData($pid) {
        $sql = "select * from part p 
    join sp_part_stock_template spst on spst.SpPartStockTemplateID=p.SpPartStockTemplateID     
    where p.PartID=$pid";
        $res = $this->Query($this->conn, $sql);
        return $res[0];
    }

    //get all stock items related to this part record
    //arg1=jobid
    //arg2=templateid
    //arg3=statusid
    public function getAllPartItems($j, $t, $s) {
        //geting stock items related to this part
        $sql = "select * from sp_part_stock_item spsi where 
              spsi.JobID=:JobID 
              and spsi.SPPartStockTemplateID=:SPPartStockTemplateID 
              and spsi.SPPartStatusID=:SPPartStatusID";
        $params = [
            "JobID" => $j,
            "SPPartStockTemplateID" => $t,
            "SPPartStatusID" => $s,
        ];
        $items = $this->Query($this->conn, $sql, $params);
        return $items;
    }

    public function getAllPartItemsByPartID($pid) {
        $sql = "select * from sp_part_stock_item where PartID=$pid";
        return $this->Query($this->conn, $sql);
    }

    public function setItemPartID($partID, $partStatus) {
        $p = $this->getAllPartData($partID);
        $items = $this->getAllPartItems($p['JobID'], $p['SpPartStockTemplateID'], $partStatus);
        foreach ($items as $i) {
            $itemId = $i['SPPartStockItem'];
            $sql = "update sp_part_stock_item set PartID=$partID where SPPartStockItem=$itemId";
            $this->Execute($this->conn, $sql);
        }
    }

    public function createSingleGSPNPartOrder($SPSupplierID) {

        $f = [
            "ServiceProviderSupplierID", "OrderedDate", "ServiceProviderID", "SPPartOrderStatusID"
        ];
        $v = [
            $SPSupplierID, date('Y-m-d H:i:s'), $this->controller->user->ServiceProviderID, 1
        ];
        $orderID = $this->SQLGen->dbInsert('sp_part_order', $f, $v);
        //adding order number to order
        $f = ["OrderNo"];
        $v = [$orderID];
        $this->SQLGen->dbUpdate('sp_part_order', $f, $v, "SPPartOrderID=$orderID");
        return $orderID;
    }

    public function setSamsungOrderNo($samsungNo, $oldOrderNo, $partid, $ExpectedDate) {
        $ExpectedDate = date('Y-m-d', strtotime($ExpectedDate));
        //updating order record
        $sql = "update sp_part_order set OrderNo='$samsungNo' where OrderNo='$oldOrderNo'";
        $this->Execute($this->conn, $sql);
        //updating part record
        $sql = "update part set OrderNo=$samsungNo,OrderDate=now(),DueDate='$ExpectedDate' where PartID=$partid";
        $this->Execute($this->conn, $sql);
    }

    public function receiveGSPNStockItem($p, $o) {
        //SPPartOrderID
        //TODO need include quantity check
        if ($o['JobID'] == "") {
            $status = 1; //item in stock
        } else {
            $status = 4; //item delivered for specific job
        }
        $this->changePartsStatus(array($o['PartID']), $status);
        $sql = "update sp_part_order set ReceviedDate=now(),Received='Y',ReceivedByUserID=:ReceivedByUserID,SPPartOrderStatusID=2 where SPPartOrderID=:SPPartOrderID";
        $param = [
            'ReceivedByUserID' => $this->controller->user->UserID,
            'SPPartOrderID' => $o['SPPartOrderID']
        ];
        $this->Execute($this->conn, $sql, $param);
        $sql = "update part set ReceivedDate=now() where PartID=:PartID";
        $param = [

            'PartID' => $o['PartID']
        ];
        $this->Execute($this->conn, $sql, $param);
        //check if job got other parts with status ordered or order required

        if ($o['JobID'] != "") {
            $sql = "select * from part where JobID=:JobID and PartStatusID in (2,3)";
            $param = ["JobID" => $o['JobID']];
            $res = $this->Query($this->conn, $sql, $param);
            //if there is no other parts with status 2 or 3 change job status to parts received
            if (!isset($res[0])) {
                $job_model = $this->controller->loadModel('Job');
                $job_model->setJobStatusByID($o['JobID'], 20);
            }
        }
    }

    public function getPartDataFromOrderNo($orderNo) {
        $sql = "select * from part where OrderNo='$orderNo'";
        $res = $this->Query($this->conn, $sql);
        if (isset($res[0])) {
            return $res[0];
            //return 1;
        } else {
            return false;
        }
    }

    //returns associated suppliers for given stock template id
    public function getPartTemplateAssocSuppliers($id, $spid) {
        $sql = "select sps.ServiceProviderSupplierID,sps.CompanyName from service_provider_supplier_to_sp_part_stock_template tg
        join service_provider_supplier sps on sps.ServiceProviderSupplierID=tg.ServiceProviderSupplierID
        where tg.SpPartStockTemplateID=$id and sps.ServiceProviderID=$spid
        ";
        return $res = $this->Query($this->conn, $sql);
    }

    //creating part and item records with status pending order

    public function createPendingItems($spid, $templateID, $qty, $supplier, $jobID = null) {
        $templateData = $this->getAllTemplateData($templateID, $spid);
        $fields = [
            "PartNo",
            "PartDescription",
            "Quantity",
            "SupplierID",
            "UnitCost",
            "SpPartStockTemplateID",
            "JobID",
            "PartStatusID"
        ];

        $val = [
            $templateData['PartNumber'],
            $templateData['Description'],
            $qty,
            $supplier,
            $templateData['PurchaseCost'],
            $templateID,
            $jobID,
            2//order required
        ];

        $partID = $this->SQLGen->dbInsert('part', $fields, $val);
        $p = [
            'qty' => $qty,
            'SpPartStockTemplateID' => $templateID,
            'JobID' => $jobID,
            'PurchaseCost' => $templateData['PurchaseCost']
        ];

        $this->insertStockOrderReqItem($spid, $p, $partID);
    }

    public function updateTemplateCost($SpPartStockTemplateID, $PurchaseCost) {
        $sql = "update sp_part_stock_template set PurchaseCost=:PurchaseCost where SpPartStockTemplateID=$SpPartStockTemplateID";
        $param = [
            "PurchaseCost" => $PurchaseCost
        ];
        $this->Execute($this->conn, $sql, $param);
    }

    //creating requisition grouped by supplier
    public function setRequisition($p, $s) {




        $part_model = $this->controller->loadModel('Part');
        foreach ($p as $f) {
            $part_data = $part_model->getPartByID($f);


            //grouping parts by suppliers
            $sup[$part_data['SupplierID']][] = $f;
        }

        foreach ($sup as $k => $g) {
            $rID = $this->createRequisition($k, $this->controller->user->ServiceProviderID);
            $reqIDs[] = $rID;
            foreach ($g as $f) {
                $part_data = $part_model->getPartByID($f);
                if ($f != "0") {
                    $items = $this->getAllPartItemsByPartID($f);
                } else {
                    break;
                }

                //updating stock item status
                foreach ($items as $t) {



                    //set item status and reqid
                    $this->updateStockPartItemStatus($t['SPPartStockItem'], $s, false, false, $rID);
                    //add history record
                    $this->insertStockItemHistory($t['SPPartStockItem'], $s, $rID);
                }

                //updating parts


                $sql = "update part set PartStatusID=:PartStatusID, SPPartRequisitionID=$rID where PartID=:PartID";

                if ($part_data['JobID'] != "") {
                    $job_model = $this->controller->loadModel('Job');
                    $job_model->setJobStatusByID($part_data['JobID'], 48);
                }

                $params = [
                    "PartStatusID" => $s,
                    "PartID" => $f,
                ];
                $this->Execute($this->conn, $sql, $params);
            }
        }
        return $reqIDs;
    }
    //fetch all infromation for part stock template
    public function getData($id){
   $sql="select *,date_format(spo.ReceviedDate,'%d/%m/%Y') as RecDate,
           
                (select count(*) from sp_part_stock_item sppsi 
                join part_status sps on sps.PartStatusID=sppsi.SPPartStatusID
                
                where 
                sppsi.SPPartStockTemplateID=$id
                 and sps.InStock='Y'    
                )as InStock,
                (select count(*) from sp_part_stock_item sppsi 
                join part_status sps on sps.PartStatusID=sppsi.SPPartStatusID
                where 
                sppsi.SPPartStockTemplateID=$id
                and sps.Available='Y' and sps.InStock='Y'    
                )as `InStockAvailable`
                ,t.PurchaseCost
            from sp_part_stock_template t
            left join sp_part_stock_item spsi on spsi.SPPartStockTemplateID=t.SPPartStockTemplateID
            left    join sp_part_order spo on spo.SPPartOrderID=spsi.SPPartOrderID
        where t.SpPartStockTemplateID=$id 
            and spsi.SPPartStatusID in (1,9)
           
            order by spsi.SPPartStockItem asc 
            limit 1";
       $res=$this->Query($this->conn, $sql);
    if(sizeof($res)==0){
       $sql="select *,date_format(spo.ReceviedDate,'%d/%m/%Y') as RecDate,  (select count(*) from sp_part_stock_item sppsi 
                join part_status sps on sps.PartStatusID=sppsi.SPPartStatusID
                
                where 
                sppsi.SPPartStockTemplateID=$id
                 and sps.InStock='Y'    
                )as InStock,
                (select count(*) from sp_part_stock_item sppsi 
                join part_status sps on sps.PartStatusID=sppsi.SPPartStatusID
                where 
                sppsi.SPPartStockTemplateID=$id
                and sps.Available='Y' and sps.InStock='Y'    
                )as `InStockAvailable`
                 ,t.PurchaseCost
            from $table t
            left join sp_part_stock_item spsi on spsi.SPPartStockTemplateID=t.SPPartStockTemplateID
            left    join sp_part_order spo on spo.SPPartOrderID=spsi.SPPartOrderID
        where t.SpPartStockTemplateID=$id 
           
           
            order by spsi.SPPartStockItem asc 
            limit 1";
       $res=$this->Query($this->conn, $sql);
    }
       
     if(isset($res[0])){
         return $res[0];
     }else{
         false;
     }
   }

    public function createRequisition($supid, $ServiceProviderID) {
        $f = [
            "ServiceProviderSupplierID", "ServiceProviderID",
        ];
        $v = [
            $supid, $ServiceProviderID
        ];

        $id = $this->SQLGen->dbInsert('sp_part_requisition', $f, $v);
        return $id;
    }

    public function createOrder($supid, $ServiceProviderID) {
        $f = [
            "ServiceProviderSupplierID", "ServiceProviderID", "OrderedDate", "Received", "SPPartOrderStatusID"
        ];
        $v = [
            $supid, $ServiceProviderID, date('Y-m-d H:i:s'), "N", 1
        ];

        $id = $this->SQLGen->dbInsert('sp_part_order', $f, $v);
        return $id;
    }

    //create orders for selected parts grouped by suppliers
    public function MakeTaggedOrders($p) {


        $s = 3; //ordered
        $part_model = $this->controller->loadModel('Part');
        foreach ($p as $f) {
            $part_data = $part_model->getPartByID($f);


            //grouping parts by suppliers
            $sup[$part_data['SupplierID']][] = $f;
        }

        foreach ($sup as $k => $g) {
            $rID = $this->createOrder($k, $this->controller->user->ServiceProviderID);
            $reqIDs[] = $rID;
            foreach ($g as $f) {
                $part_data = $part_model->getPartByID($f);
                if ($f != "0") {
                    $items = $this->getAllPartItemsByPartID($f);
                } else {
                    break;
                }

                //updating stock item status
                foreach ($items as $t) {



                    //set item status and reqid
                    $this->updateStockPartItemStatus($t['SPPartStockItem'], $s, false, $rID);
                    //add history record
                    $this->insertStockItemHistory($t['SPPartStockItem'], $s, $rID);
                }

                //updating parts


                $sql = "update part set PartStatusID=:PartStatusID, SPPartOrderID=$rID where PartID=:PartID";

                if ($part_data['JobID'] != "") {
                    $job_model = $this->controller->loadModel('Job');
                    $job_model->setJobStatusByID($part_data['JobID'], 48);
                }

                $params = [
                    "PartStatusID" => $s,
                    "PartID" => $f,
                ];
                $this->Execute($this->conn, $sql, $params);
            }
        }
        return $reqIDs; //returning created orders ids
    }

    //getting all data about Requisition to print pdf
    //param1 = array(Requisitionid);
    //params2 = serviceProviderID
    public function getRequisitionData($reqids, $spid) {
        $currencyModel = $this->controller->loadModel('Currency');
        $reqids = explode(",", $reqids);
        foreach ($reqids as $r) {

            $sql = "select
                SUM(p.Quantity) as TotalQuantity,
                p.*,sp.*,
                sps.CompanyName as supllier,
                sps.PostCode as supPostcode,
                sps.BuildingNameNumber as supBuildingNameNumber,
                sps.Street as supStreet,
                sps.LocalArea as supLocalArea,
                sps.TownCity as supTownCity,
                sps.TelephoneNo as supTelephoneNo,
                sps.FaxNo as supFaxNo,
                sps.DefaultCurrencyID as supCurrency,
                c.Name as supCountryName,
                c2.Name as spCountryName
                    
                from part p 
            join sp_part_requisition spr on spr.SPPartRequisitionID=p.SPPartRequisitionID
            join service_provider_supplier sps on sps.ServiceProviderSupplierID=p.SupplierID
            join service_provider sp on sp.ServiceProviderID=spr.ServiceProviderID
            left join country c on c.CountryID=sps.CountryID
            left join country c2 on c2.CountryID=sp.CountryID
            where 
            p.SPPartRequisitionID=$r and spr.ServiceProviderID=$spid
                group by p.SpPartStockTemplateID
                     ";
            $res = $this->Query($this->conn, $sql);
            $colours = $this->getDocColours($spid);
            //supplier
            $return[$r]['Supplier']['name'] = $res[0]['supllier'];
            $return[$r]['Supplier']['PostCode'] = $res[0]['supPostcode'];
            $return[$r]['Supplier']['BuildingNameNumber'] = $res[0]['supBuildingNameNumber'];
            $return[$r]['Supplier']['Street'] = $res[0]['supStreet'];
            $return[$r]['Supplier']['LocalArea'] = $res[0]['supLocalArea'];
            $return[$r]['Supplier']['TownCity'] = $res[0]['supTownCity'];
            $return[$r]['Supplier']['TelephoneNo'] = $res[0]['supTelephoneNo'];
            $return[$r]['Supplier']['FaxNo'] = $res[0]['supFaxNo'];
            $return[$r]['Supplier']['CountryName'] = $res[0]['supCountryName'];
            $return[$r]['Supplier']['CurencyCode'] = $currencyModel->getCurrencyCode($res[0]['supCurrency']);
            //service provider
            $return[$r]['SP']['CompanyName'] = $res[0]['CompanyName'];
            $return[$r]['SP']['PostalCode'] = $res[0]['PostalCode'];
            $return[$r]['SP']['BuildingNameNumber'] = $res[0]['BuildingNameNumber'];
            $return[$r]['SP']['Street'] = $res[0]['Street'];
            $return[$r]['SP']['LocalArea'] = $res[0]['LocalArea'];
            $return[$r]['SP']['TownCity'] = $res[0]['TownCity'];
            $return[$r]['SP']['ContactPhone'] = $res[0]['ContactPhone'];
            $return[$r]['SP']['ContactPhoneExt'] = $res[0]['ContactPhoneExt'];
            $return[$r]['SP']['CountryName'] = $res[0]['spCountryName'];
            $return[$r]['SP']['DocColour1'] = $colours[0]['DocColour1'];
            $return[$r]['SP']['DocColour2'] = $colours[0]['DocColour2'];
            $return[$r]['SP']['DocColour3'] = $colours[0]['DocColour3'];
            $return[$r]['SP']['DocLogo'] = $res[0]['DocLogo'];
            $return[$r]['SP']['CurencyCode'] = $currencyModel->getCurrencyCode($res[0]['CurrencyID']);
            $return[$r]['SP']['ExRate'] = $currencyModel->getSPtoSupplierExchangeRate($res[0]['ServiceProviderID'], $res[0]['SupplierID']);
            foreach ($res as $p) {
                $return[$r]['items'][] = [
                    "qty" => $p['TotalQuantity'],
                    "PartNo" => $p['PartNo'],
                    "PartDescription" => $p['PartDescription'],
                    "UnitCost" => $p['UnitCost'],
                ];
            }
        }
        return $return;
    }

    public function getDocColours($spid) {
        $sql = "select DocColour1,DocColour2,DocColour3,DocLogo,DocumentCustomColorScheme from service_provider where ServiceProviderID=$spid";

        $res = $this->Query($this->conn, $sql);
        if ($res[0]['DocumentCustomColorScheme'] == "Skyline") {
            $res[0]['DocColour1'] = "25AAE1";
            $res[0]['DocColour2'] = "0F75BC";
            $res[0]['DocColour3'] = "0F75BC";
        }
        return $res;
    }

    //getting all data about Order to print pdf
    //param1 = array(Requisitionid);
    //params2 = serviceProviderID
    public function getOrdersData($reqids, $spid) {
        $currencyModel = $this->controller->loadModel('Currency');
        $reqids = explode(",", $reqids);
        foreach ($reqids as $r) {

            $sql = "select
                SUM(p.Quantity) as TotalQuantity,
                p.*,sp.*,
                sps.CompanyName as supllier,
                sps.PostCode as supPostcode,
                sps.BuildingNameNumber as supBuildingNameNumber,
                sps.Street as supStreet,
                sps.LocalArea as supLocalArea,
                sps.TownCity as supTownCity,
                sps.TelephoneNo as supTelephoneNo,
                sps.FaxNo as supFaxNo,
                sps.DefaultCurrencyID as supCurrency,
                c.Name as supCountryName,
                c2.Name as spCountryName,
                spo.OrderedDate
                    
                from part p 
            join sp_part_order spo on spo.SPPartOrderID=p.SPPartOrderID
            join service_provider_supplier sps on sps.ServiceProviderSupplierID=p.SupplierID
            join service_provider sp on sp.ServiceProviderID=spo.ServiceProviderID
            left join country c on c.CountryID=sps.CountryID
            left join country c2 on c2.CountryID=sp.CountryID
            where 
            p.SPPartOrderID=$r and spo.ServiceProviderID=$spid
                group by p.SpPartStockTemplateID
                     ";
            $res = $this->Query($this->conn, $sql);
            $colours = $this->getDocColours($spid);

            //supplier
            $return[$r]['Supplier']['name'] = $res[0]['supllier'];
            $return[$r]['Supplier']['PostCode'] = $res[0]['supPostcode'];
            $return[$r]['Supplier']['BuildingNameNumber'] = $res[0]['supBuildingNameNumber'];
            $return[$r]['Supplier']['Street'] = $res[0]['supStreet'];
            $return[$r]['Supplier']['LocalArea'] = $res[0]['supLocalArea'];
            $return[$r]['Supplier']['TownCity'] = $res[0]['supTownCity'];
            $return[$r]['Supplier']['TelephoneNo'] = $res[0]['supTelephoneNo'];
            $return[$r]['Supplier']['FaxNo'] = $res[0]['supFaxNo'];
            $return[$r]['Supplier']['CountryName'] = $res[0]['supCountryName'];
            $return[$r]['Supplier']['CurencyCode'] = $currencyModel->getCurrencyCode($res[0]['supCurrency']);
            //service provider
            $return[$r]['SP']['CompanyName'] = $res[0]['CompanyName'];
            $return[$r]['SP']['PostalCode'] = $res[0]['PostalCode'];
            $return[$r]['SP']['BuildingNameNumber'] = $res[0]['BuildingNameNumber'];
            $return[$r]['SP']['Street'] = $res[0]['Street'];
            $return[$r]['SP']['LocalArea'] = $res[0]['LocalArea'];
            $return[$r]['SP']['TownCity'] = $res[0]['TownCity'];
            $return[$r]['SP']['ContactPhone'] = $res[0]['ContactPhone'];
            $return[$r]['SP']['ContactPhoneExt'] = $res[0]['ContactPhoneExt'];
            $return[$r]['SP']['CountryName'] = $res[0]['spCountryName'];
            $return[$r]['SP']['CurencyCode'] = $currencyModel->getCurrencyCode($res[0]['CurrencyID']);
            $return[$r]['SP']['ExRate'] = $currencyModel->getSPtoSupplierExchangeRate($res[0]['ServiceProviderID'], $res[0]['SupplierID']);
            $return[$r]['SP']['DocColour1'] = $colours[0]['DocColour1'];
            $return[$r]['SP']['DocColour2'] = $colours[0]['DocColour2'];
            $return[$r]['SP']['DocColour3'] = $colours[0]['DocColour3'];
            $return[$r]['SP']['DocLogo'] = $res[0]['DocLogo'];
            $return[$r]['SP']['OrderedDate'] = $res[0]['OrderedDate'];
            foreach ($res as $p) {
                $return[$r]['items'][] = [
                    "qty" => $p['TotalQuantity'],
                    "PartNo" => $p['PartNo'],
                    "PartDescription" => $p['PartDescription'],
                    "UnitCost" => $p['UnitCost'],
                ];
            }
        }
        return $return;
    }

    public function checkRequisition($p) {
        $parts = "0";
        foreach ($p as $pp) {
            $parts.=",$pp";
        }
        $sql = "select PartID from part where SPPartRequisitionID is not null  and PartID in ($parts)";
        $res = $this->Query($this->conn, $sql);
        return $res;
    }

    public function validateReq($req, $existOnly) {
        $sql = "select count(*) as c from sp_part_requisition where SPPartRequisitionID=$req";
        $res = $this->Query($this->conn, $sql);
        if ($res[0]['c'] == 0) {
            //check if requisition exist
            return "1"; //not exist
        } else {
            if (!$existOnly) {
                //check if all req parts got status 18
                $sql = "select count(*) as c from part where SPPartRequisitionID=$req and PartStatusID!=18";
                $res = $this->Query($this->conn, $sql);
                if ($res[0]['c'] != 0) {

                    return "2"; //status is not 18 
                } else {
                    return "0"; // all fine validtaion pass
                }
            } else {
                return "0"; //exist , status check not required
            }
        }
    }

    public function approveReq($req) {
        $sql = "select PartID from part where SPPartRequisitionID=$req";
        $res = $this->Query($this->conn, $sql);
        foreach ($res as $k) {
            $res2[] = $k['PartID'];
        }
        $this->changePartsStatus($res2, 19);
    }

    public function checkIfCanApplyNewStatus($parts) {
        $parts = implode(",", $parts);

        $sql = "select PartID from part where PartStatusID not in(18,2) and PartID in ($parts)";
        $res = $this->Query($this->conn, $sql);
        return $res;
    }

    public function validateMakeTaggedOrders($parts) {
        $parts = implode(",", $parts);
        $sql = "select PartID from part where PartStatusID not in(19)  and PartID in ($parts)";
        $res = $this->Query($this->conn, $sql);
        return $res;
    }

    public function getColourKey($sp) {

        $sql = "select PartStatusName,Colour from part_status
            left join sp_part_status_colour ps on ps.PartStatusID=part_status.PartStatusID and ps.ServiceProviderID=$sp
            ";
        $res = $this->Query($this->conn, $sql);
        return $res;
    }

    public function getALLSPOrders($spid, $orderNum) {
        $sql = "select SPPartOrderID from sp_part_order where ServiceProviderID=$spid and SPPartOrderID LIKE '%" . $orderNum . "%'";
        $res = $this->Query($this->conn, $sql);
        return $res;
    }

    public function getPartReceiveData($items) {
        $sql = "select spsi.SPPartOrderID as 'OrderNo',
spst.PartNumber,
spst.Description as 'PartDescription',
sps.CompanyName as 'Supplier',
count(*) as 'QTYOrdered',
c.CurrencyCode as'spCurrency',
'Main Store' as 'PartLocation',
c2.CurrencyCode as 'supCurrency',
spsi.PurchaseCost as 'PurchasePricePerItem',
p.SupplierID as 'DefaultServiceProviderSupplierID',
sps.ServiceProviderID,
'$items' as 'items'
from sp_part_stock_item spsi
join sp_part_stock_template spst on spst.SpPartStockTemplateID=spsi.SPPartStockTemplateID
join part p on p.PartID=spsi.PartID
join service_provider_supplier sps on sps.ServiceProviderSupplierID=p.SupplierID
join service_provider sp on sp.ServiceProviderID=spst.ServiceProviderID
join currency c on c.CurrencyID=sp.CurrencyID
join currency c2 on c2.CurrencyID=sps.DefaultCurrencyID
where spsi.SPPartStockItem in ($items)
";
        $res = $this->Query($this->conn, $sql);
        return isset($res[0]) ? $res[0] : false;
    }

    public function ReceiveParts($p, $items, $itemsList) {
        $sql = "insert into sp_stock_receiving_history 
         (
         MovementType,
         DateReceived,
         SPPartOrderID,
         UserID,
         ServiceProviderSupplierID,
         QtyReceived,
         InvoiceNo,
         UnitPrice,
         ExchangeRate,
         Notes,
         QtyOrdered
         ) 
         values
         (
         :MovementType,
         now(),
         :SPPartOrderID,
         :UserID,
         :ServiceProviderSupplierID,
         :QtyReceived,
         :InvoiceNo,
         :UnitPrice,
         :ExchangeRate,
         :Notes,
         :QtyOrdered
         )
    ";
        $notes = "";
        if ($p['supCurrency'] != $p['spCurrency']) {
            $notes = "
            Unit Purchase Price = " . $p['PurchasePricePerItemC'] . " " . $p['supCurrency'] . "<br>
            Exhange Rate: = 1 " . $p['spCurrency'] . " =" . $p['currencyRate'] . "
            ";
        }

        $params = [
            "MovementType" => "Incoming",
            "SPPartOrderID" => $p['SPPartOrderID'],
            "UserID" => $this->controller->user->UserID,
            "ServiceProviderSupplierID" => $p['ServiceProviderSupplierID'],
            "QtyReceived" => $p['QTYReceived'],
            "InvoiceNo" => $p['SupplierInvoiceNo'],
            "UnitPrice" => $p['PurchasePricePerItem'],
            "ExchangeRate" => $p['currencyRate'],
            "Notes" => $notes,
            "QtyOrdered" => $p['QtyOrdered']
        ];

        $this->Execute($this->conn, $sql, $params);
        $hID = $this->conn->lastInsertId();

        for ($i = 0; $i < $p['QTYReceived']; $i++) {
            //check if items got job id on it
            $sql = "select JobID from sp_part_stock_item where SPPartStockItem=" . $items[$i];
            $res = $this->Query($this->conn, $sql);
            if ($res[0]['JobID'] != "") {
                $st = 4; //delivered
            } else {
                $st = 1; //in stock
            }
            //set item status
            $this->updateStockPartItemStatus($items[$i], $st, false, false, false, $hID);
            //add history record
            $this->insertStockItemHistory($items[$i], $st, $p['SPPartOrderID']);
        }
        //check if all ordered items have been received
        $po = isset($items[0]) ? $items[0] : 0;
        $sql = "select count(*) as 'c1',p.PartID,p.JobID from sp_part_stock_item spsi 
            join part p on p.PartID=spsi.PartID
            where spsi.SPPartStatusID=3 and p.PartID in (select PartID from sp_part_stock_item s2 where s2.SPPartStockItem in ($itemsList) )
                group by PartID
            ";

        $res = $this->Query($this->conn, $sql);
        //all items received
        foreach ($res as $r) {
            if ($r['c1'] == 0) {
                $st = 1;
                if ($r['JobID'] != "") {
                    $st = 4;
                }
                $sql = "update part set PartStatusID=:PartStatusID where PartID=:PartID";
                $param = ["PartStatusID" => $st];
                $this->Execute($this->conn, $sql, $param);
            }
        }
    }

    public function ReceivePartsMulti($post, $items) {

        //get part ids
        $imtelist = implode(",", $items);
       
        $sql = "select *,count(SPPartStockItem) as c from sp_part_stock_item spsi 
        join sp_part_order spo on spo.SPPartOrderID=spsi.SPPartOrderID
        where spsi.SPPartStockItem in ($imtelist)  group by PartID";
         $this->controller->log($sql);
        $partArr = $this->Query($this->conn, $sql);

        foreach ($partArr as $p) {
            $sql = "insert into sp_stock_receiving_history 
         (
         MovementType,
         DateReceived,
         SPPartOrderID,
         UserID,
         ServiceProviderSupplierID,
         QtyReceived,
         InvoiceNo,
         UnitPrice,
         ExchangeRate,
         Notes,
         QtyOrdered
         ) 
         values
         (
         :MovementType,
         now(),
         :SPPartOrderID,
         :UserID,
         :ServiceProviderSupplierID,
         :QtyReceived,
         :InvoiceNo,
         :UnitPrice,
         :ExchangeRate,
         :Notes,
         :QtyOrdered
         )
    ";
            $notes = "";
            if ($post['supcurrencyCode'] != $post['spCurrency']) {
                $notes = "
            Unit Purchase Price = " . number_format($p['PurchaseCost'] * $post['currencyRate'], 2, '.', '') . " " . $post['supcurrencyCode'] . "<br>
            Exhange Rate: = 1 " . $post['spCurrency'] . " =" . number_format($post['currencyRate'], 2, '.', '') . "
            ";
            }

            $params = [
                "MovementType" => "Incoming",
                "SPPartOrderID" => $p['SPPartOrderID'],
                "UserID" => $this->controller->user->UserID,
                "ServiceProviderSupplierID" => $p['ServiceProviderSupplierID'],
                "QtyReceived" => $p['c'],
                "InvoiceNo" => $post['invoiceNo'],
                "UnitPrice" => $p['PurchaseCost'],
                "ExchangeRate" => $post['currencyRate'],
                "Notes" => $notes,
                "QtyOrdered" => $p['c']
            ];

            $this->Execute($this->conn, $sql, $params);
            $hID = $this->conn->lastInsertId();


            //check if items got job id on it
            $sql = "select JobID,SPPartStockItem from sp_part_stock_item where SPPartStockItem in ($imtelist) and PartID=:PartID";
            $param = ["PartID" => $p['PartID']];
            $res = $this->Query($this->conn, $sql, $param);
            if ($res[0]['JobID'] != "") {
                $st = 4; //delivered
            } else {
                $st = 1; //in stock
            }
            for ($i = 0; $i < sizeof($res); $i++) {
                //set item status
                $this->updateStockPartItemStatus($res[$i]['SPPartStockItem'], $st, false, false, false, $hID);
                //add history record
                $this->insertStockItemHistory($res[$i]['SPPartStockItem'], $st, $p['SPPartOrderID']);
            }


            $sql = "update part set PartStatusID=:PartStatusID where PartID=:PartID";
            $param = ["PartStatusID" => $st,
                "PartID" => $p['PartID']
            ];
            $this->Execute($this->conn, $sql, $param);
        }
    }

    public function getStockItemSupplierData($itemID) {
        $sql = "select * 
from sp_part_stock_item spsi 
join sp_part_order spo on spo.SPPartOrderID=spsi.SPPartOrderID
join service_provider_supplier sps on sps.ServiceProviderSupplierID=spo.ServiceProviderSupplierID
join service_provider sp on sp.ServiceProviderID=sps.ServiceProviderID
where spsi.SPPartStockItem=$itemID";
        $res = $this->Query($this->conn, $sql);
        return isset($res[0]) ? $res[0] : false;
    }

    public function createGRN($p) {

        $orderid = $p['OrderNo'];
        $suppID = $p['ServiceProviderSupplierID'];
        $GRNdate = explode("/", $p['GoodsRecDate']);
        $GRNdate = $GRNdate[2] . "-" . $GRNdate[1] . "-" . $GRNdate[0];
        $sql = "select count(*) as 'c' from sp_part_stock_item spsi where spsi.SPPartOrderID=:SPPartOrderID and spsi.SpGRNID is null and spsi.SPPartStatusID!=3";
        $param = ["SPPartOrderID" => $orderid];
        $res = $this->Query($this->conn, $sql, $param);
        $this->controller->log($res);
        if ($res[0]['c'] > 0) {
            //for some items GRN is not printed, so we create GRN record
            $sql = "insert into sp_grn (PrintedDate,PrintedUserID,SPPartOrderID,ServiceProviderID)
            values
                (now(),:PrintedUserID,:SPPartOrderID,:ServiceProviderID)
            ";
            $param = [
                "PrintedUserID" => $this->controller->user->UserID,
                "SPPartOrderID" => $orderid,
                "ServiceProviderID" => $this->controller->user->ServiceProviderID
            ];

            $this->Execute($this->conn, $sql, $param);
            $GRNID = $this->conn->lastInsertId();
            $sql = "update sp_part_stock_item set SpGRNID=:SpGRNID where SPPartOrderID=:SPPartOrderID and SpGRNID is null and SPPartStatusID!=3";
            $param = [
                "SpGRNID" => $GRNID,
                "SPPartOrderID" => $orderid
            ];
            $this->Execute($this->conn, $sql, $param); //set items grn no
            return $GRNID;
        } else {

            return false;
        }
    }

    public function getGRNData($GRNid, $spid) {
        $currencyModel = $this->controller->loadModel('Currency');
        if (!$GRNid) {
            $GRNid = 0;
        }



         $sql = "select 
             count(1) as TotalQuantity, 
p.*, 
sp.*,
spo.*,
sps.CompanyName as supllier,
 sps.PostCode as supPostcode,
  sps.BuildingNameNumber as supBuildingNameNumber, 
  sps.Street as supStreet,
   sps.LocalArea as supLocalArea,
	 sps.TownCity as supTownCity,
	  sps.TelephoneNo as supTelephoneNo,
          sps.ServiceProviderSupplierID,
	   sps.FaxNo as supFaxNo,
           spst.PartNumber as 'PartNo',
           spst.Description as 'PartDescription',
		sps.DefaultCurrencyID as supCurrency,
		 c.Name as supCountryName,
		  c2.Name as spCountryName,
		   spo.OrderedDate 
			from sp_part_stock_item p
 join sp_part_order spo on spo.SPPartOrderID=p.SPPartOrderID 
 join sp_part_stock_template spst on spst.SpPartStockTemplateID=p.SPPartStockTemplateID
			 join service_provider_supplier sps on sps.ServiceProviderSupplierID=spo.ServiceProviderSupplierID 
			 join service_provider sp on sp.ServiceProviderID=spo.ServiceProviderID 
			 left join country c on c.CountryID=sps.CountryID 
			 left join country c2 on c2.CountryID=sp.CountryID 
            where 
            p.SpGRNID=$GRNid and spo.ServiceProviderID=$spid
                group by p.SpPartStockTemplateID
                     ";
        $res = $this->Query($this->conn, $sql);
        $colours = $this->getDocColours($spid);
        if (isset($res[0])) {
            $r=$GRNid;
            //supplier
            $return[$r]['Order']['number'] = $res[0]['SPPartOrderID'];
            $return[$r]['Order']['orderDate'] = $res[0]['OrderedDate'];
            $return[$r]['Supplier']['name'] = $res[0]['supllier'];
            $return[$r]['Supplier']['PostCode'] = $res[0]['supPostcode'];
            $return[$r]['Supplier']['BuildingNameNumber'] = $res[0]['supBuildingNameNumber'];
            $return[$r]['Supplier']['Street'] = $res[0]['supStreet'];
            $return[$r]['Supplier']['LocalArea'] = $res[0]['supLocalArea'];
            $return[$r]['Supplier']['TownCity'] = $res[0]['supTownCity'];
            $return[$r]['Supplier']['TelephoneNo'] = $res[0]['supTelephoneNo'];
            $return[$r]['Supplier']['FaxNo'] = $res[0]['supFaxNo'];
            $return[$r]['Supplier']['CountryName'] = $res[0]['supCountryName'];
            $return[$r]['Supplier']['CurencyCode'] = $currencyModel->getCurrencyCode($res[0]['supCurrency']);
            //service provider
            $return[$r]['SP']['CompanyName'] = $res[0]['CompanyName'];
            $return[$r]['SP']['PostalCode'] = $res[0]['PostalCode'];
            $return[$r]['SP']['BuildingNameNumber'] = $res[0]['BuildingNameNumber'];
            $return[$r]['SP']['Street'] = $res[0]['Street'];
            $return[$r]['SP']['LocalArea'] = $res[0]['LocalArea'];
            $return[$r]['SP']['TownCity'] = $res[0]['TownCity'];
            $return[$r]['SP']['ContactPhone'] = $res[0]['ContactPhone'];
            $return[$r]['SP']['ContactPhoneExt'] = $res[0]['ContactPhoneExt'];
            $return[$r]['SP']['CountryName'] = $res[0]['spCountryName'];
            $return[$r]['SP']['CurencyCode'] = $currencyModel->getCurrencyCode($res[0]['CurrencyID']);
            $return[$r]['SP']['ExRate'] = $currencyModel->getSPtoSupplierExchangeRate($res[0]['ServiceProviderID'], $res[0]['ServiceProviderSupplierID']);
            $return[$r]['SP']['DocColour1'] = $colours[0]['DocColour1'];
            $return[$r]['SP']['DocColour2'] = $colours[0]['DocColour2'];
            $return[$r]['SP']['DocColour3'] = $colours[0]['DocColour3'];
            $return[$r]['SP']['DocLogo'] = $res[0]['DocLogo'];
            $return[$r]['SP']['OrderedDate'] = $res[0]['OrderedDate'];
            foreach ($res as $p) {
                $return[$r]['items'][] = [
                    "qty" => $p['TotalQuantity'],
                    "PartNo" => $p['PartNo'],
                    "PartDescription" => $p['PartDescription'],
                    "UnitCost" => $p['PurchaseCost'],
                ];
            }


            return $return;
        } else {
            return false;
        }
    }
    
    
  public function getTemplateHistoryData($id){
    $sql="
        select 
spsh.DateTime as 'ihDate',
spsrh.DateReceived  as 'rgDate',
spsi.SPPartOrderID as 'OrderNo',
spsi.JobID as 'JobID',
concat_ws(' ',u1.ContactFirstName,u1.ContactLastName) as 'User',
sps.CompanyName as 'Supplier',
count(1) as 'Quantity',
DespatchNo,
InvoiceNo,
(spsrh.UnitPrice*count(1)) as 'Cost',
ps.PartStatusName as 'Comment'
from sp_part_stock_template spst 
join sp_part_stock_item spsi  on spsi.SPPartStockTemplateID=spst.SpPartStockTemplateID
join sp_part_order spo on spo.SPPartOrderID=spsi.SPPartOrderID
join service_provider_supplier sps on sps.ServiceProviderSupplierID=spo.ServiceProviderSupplierID

join sp_part_stock_history spsh on spsh.SPPartStockItemID=spsi.SPPartStockItem
join part_status ps on ps.PartStatusID=spsh.SPPartStatusID
join user u1 on u1.UserID=spsh.UserID
left join sp_stock_receiving_history spsrh on spsrh.SpStockReceivingHistoryID=spsi.SpStockReceivingHistoryID
where spst.SpPartStockTemplateID=$id 
group by  spsh.SPPartStatusID, date_format(spsh.DateTime,'%d/%m/%d %H:%i')
order by ihDate

        ";  
    $res = $this->Query($this->conn, $sql);
    return $res;
  }

  
  public function showHistoryNote($id){
      $sql="select Notes from sp_stock_receiving_history spsrh where SpStockReceivingHistoryID=$id";
      $res = $this->Query($this->conn, $sql);
    return $res[0]['Notes'];
  }
}

?>