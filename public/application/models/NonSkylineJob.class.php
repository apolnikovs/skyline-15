<?php
/**
 * NonSkylineJob.class.php
 * 
 * Routines for interaction with the non_skyline_job table
 *
 * @author     Andrew Williams <a.williams@pccsuk.com>
 * @copyright  2012 - 2013 PC Control Systems
 * @link       
 * @version    1.04
 * 
 * Changes
 * Date        Version Author                Reason
 * 22/10/2012  1.00    Andrew J. Williams    Initial Version
 * 31/01/2013  1.01    Andrew J. Williams    Trackerbase VMS Log 157 - Added JobIsNowSkyline and getIdFromNetworkRef
 * 14/02/2013  1.02    Andrew J. Williams    Trackerbase VMS Log 174 - Move Non Skyline Appointments over on job import 
 * 26/02/2013  1.03    Brian Etherington     Comment out duplicate JobIsNowSkylineSp that is preveniting model from loading
 * 04/03/2013  1.04    Andris Polnikovs      Addet putAppointment Details API call
 ******************************************************************************/

require_once('CustomModel.class.php');
require_once('TableFactory.class.php');

class NonSkylineJob extends CustomModel { 
    private $table;                                                             /* For Table Factory Class */
    private $conn;                                                              /* Database Connection */
    
    public function __construct($Controller) {
                  
        parent::__construct($Controller);
        
        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] ); 
        
        $this->table = TableFactory::NonSkylineJob();
    }
    
    
    /**
     * create 
     *  
     * Create a non skyline job record
     * 
     * @param array $args   Associative array of field values for the creation of
     *                      the new appointment
     * 
     * @return array    (status - Status Code, message - Status message, id - Id of inserted item
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function create($args) {
        $cmd = $this->table->insertCommand( $args );
        if ($this->Execute($this->conn, $cmd, $args)) {
            $result =  array(                                                   /* Job successfully created */
                             'status' => 'SUCCESS',
                             'id' => $this->conn->lastInsertId()     /* Return the newly created appointment's ID */
                            );
        } else {
            $result = array(
                            'status' => 'FAIL',
                            'id' => 0,                               /* Not created no ID to return */
                            'message' => $this->lastPDOError()                  /* Return the error */
                           );
        }
        return $result;
    }
    
    /**
     * delete
     *  
     * Delete a part from the parts table. We can delete on either PartID the 
     * native SkyLine Part ID or SBPartID the service base Part ID. To achive 
     * this we pass an array as a paremeter. The index of the array should match
     * the field we are deleting on (PartID or SBPartID) and the Value an integer
     * of the ID. The array passed should only have one item.
     * 
     * @param array $args (Field { PartID, SBPartID } => Value )
     * 
     * @return (status - Status Code, message - Status message, rows_affected number of rows deleted)
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function delete($args) {
        $index  = array_keys($args); 
        
        $cmd = $this->table->deleteCommand( $index[0].' = '.$args[$index[0]] );
        
        $rows_affected = $this->Execute($this->conn, $cmd, $args);
        
        if ( $rows_affected == 0 ) {                                             /* No rows affected may be error */
            $code = $this->conn->errorCode();
            $message = $this->conn->errorInfo();
        } else {
            $code = 'OK';
            $message = 'Deleted';
        }
        
        return (
                array(
                      'status' => $code,
                      'message' => $message,
                      'rows_affected' => $rows_affected
                     )
               );
    }
    
    /**
     * JobIsNowSkylineNet
     *  
     * An NonSkylineJob is noe skyline. We matcha non skyline job on its
     * NetworkRefNo and NetworkID, update the appointments to be linked to the
     * new JobID rather than it's NonSkylineJobID and then we delete the job 
     * from NonSkylinbeJobs.
     * 
     * @param integer $jId      Job ID
     *        integer $nId      Network ID
     *        string $nRefNo    NetworkRefernceNumber
     * 
     * @return array    (status - Status Code, message - Status message, id - Id of inserted item
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function JobIsNowSkylineNet($jId, $nId, $nRefNo) {
        
        $sqlNsj = "
                   SELECT
			   `NonSkylineJobID`
		   FROM
		           `non_skyline_job`
		   WHERE
		           `NetworkID` = $nId
		           AND `NetworkRefNo` = '$nRefNo'
                  ";
        
        $resultNsj = $this->Query($this->conn, $sqlNsj);
        
        if ( count($resultNsj) > 0 ) {                                          /* Do we have a match? */
            $nsjId = $resultNsj[0]['NonSkylineJobID'];                          /* Yes procede with updating attached appointments */
            
            $sqlA = "
                       UPDATE
				`appointment`
                       SET
				`JobID` = $jId,
				`NonSkylineJobID` = NULL
		       WHERE
                                `NonSkylineJobID` = $nsjId
                    ";
            
            $this->execute($this->conn, $sqlA);
                     $sql="
                         select AppointmentID as `apID`
				
                       from `appointment` 
				
		       WHERE
                                `NonSkylineJobID` = $nsjId or `JobID` = $jId
                        ";   
                      $this->controller->log($sql,"One_touch_put_appDet");
                     /* Now delete the original non skyline job */
            /*$sqlD = "                                                           
                      DELETE FROM
				`non_skyline_job`
		      WHERE
                                `NonSkylineJobID` = $nsjId              
                    ";
            
            $this->Query($this->conn, $sqlD);*/
                     
            $res=$this->Query($this->conn, $sql);
             $this->controller->log($res,"One_touch_put_appDet");
          //  $this->delete(array('NonSkylineJobID' => $nsjId   ));
               $apiA=$this->loadModel('APIAppointments');
               if(isset($res[0]['apID'])){
                   
                 
        $apiA->PutAppointmentDetails($res[0]['apID']);
                  $this->controller->log("One_touch_put_appDet - ".$res[0]['apID']."","One_touch_put_appDet");
               }
        }
        
        
     
        
    }
    
/*  NOTE:  This method commented out 26/02/2013 bercause it is a duplicate method
 *         which appears to have been replaced by the code following it.
 *         This problem is preveting the model from loading.
 */    
    
#    /**
#     * JobIsNowSkylineSp
#     *  
#     * A NonSkylineJob is now skyline. We match a non skyline job on its
#     * Service Provider Job No and Service Provider  ID, update the appointments
#     * to be linked to the new JobID rather than it's NonSkylineJobID and then 
#     * we delete the job from NonSkylinbeJobs.
#     * 
#     * @param integer $jId      Job ID
#     *        integer $spId     Service Provider ID
#     *        string $scJobNo   Servicebase Job Number
#     * 
#     * @return array    (status - Status Code, message - Status message, id - Id of inserted item
#     * 
#     * @author Andrew Williams <Andrew.Williams@awcomputech.com>  
#     **************************************************************************/
#    public function JobIsNowSkylineSp($jId, $spId, $scJobNo) {
#        $sqlNsj = "
#                   SELECT
#			   `NonSkylineJobID`
#		   FROM
#		           `non_skyline_job`
#		   WHERE
#		           `ServiceProviderID` = $spId
#		           AND `ServiceProviderJobNo` = $scJobNo
#                  ";
#        
#        $resultNsj = $this->Query($this->conn, $sqlNsj);
#        
#        if ( count($resultNsj) > 0 ) {                                          /* Do we have a match? */
#            $nsjId = $resultNsj[0]['NonSkylineJobID'];                          /* Yes procede with updating attached appointments */
#            if ($this->debug) $this->controller->log('NonSkylineJob.class.php: Non SkylineJobID :'.$nsjId);
#            
#            $sqlA = "
#                       UPDATE
#				`appointment`
#                       SET
#				`JobID` = $jId,
#				`NonSkylineJobID` = NULL
#		       WHERE
#                                `NonSkylineJobID` = $nsjId
#                    ";
#            
#            $resultA = $this->Query($this->conn, $sqlA);
#                                                                                /* Now delete the original non skyline job */
#            /*$sqlD = "                                                           
#                      DELETE FROM
#				`non_skyline_job`
#		      WHERE
#                                `NonSkylineJobID` = $nsjId              
#                    ";
#            
#            $resultD = $this->Query($this->conn, $sqlD);*/
#            $this->delete(array('NonSkylineJobID' => $nsjId ));
#        }
#        
#    }
    
    /**
     * JobIsNowSkylineSp
     *  
     * A NonSkylineJob is now skyline. We match a non skyline job on its
     * Service Provider Job No and Service Provider  ID, update the appointments
     * to be linked to the new JobID rather than it's NonSkylineJobID and then 
     * we delete the job from NonSkylinbeJobs.
     * 
     * @param integer $jId      Job ID
     *        integer $spId     Service Provider ID
     *        string $scJobNo   Servicebase Job Number
     * 
     * @return array    (status - Status Code, message - Status message, id - Id of inserted item
     * 
     * @author Andrew Williams <Andrew.Williams@awcomputech.com>  
     **************************************************************************/
    public function JobIsNowSkylineSp($jId, $spId, $scJobNo) {
        $sqlNsj = "
                   SELECT
			   `NonSkylineJobID`
		   FROM
		           `non_skyline_job`
		   WHERE
		           `ServiceProviderID` = $spId
		           AND `ServiceProviderJobNo` = $scJobNo
                  ";
        
        $resultNsj = $this->Query($this->conn, $sqlNsj);
        
        if ( count($resultNsj) > 0 ) {                                          /* Do we have a match? */
            $nsjId = intval($resultNsj[0]['NonSkylineJobID']);                  /* Yes procede with updating attached appointments */
            
            $sqlA = "
                       UPDATE
				`appointment`
                       SET
				`JobID` = $jId,
				`NonSkylineJobID` = NULL
		       WHERE
                                `NonSkylineJobID` = $nsjId
                    ";
            
            $this->execute($this->conn, $sqlA);
                                                                                /* Now delete the original non skyline job */
            $sqlD = "                                                           
                      DELETE FROM
				`non_skyline_job`
		      WHERE
                                `NonSkylineJobID` = $nsjId              
                    ";
            
            $this->execute($this->conn, $sqlD);
             $sql="
                         select AppointmentID as `apID`
				
                       from `appointment` 
				
		       WHERE
                                `NonSkylineJobID` = $nsjId or `JobID` = $jId
                        ";   
                    $this->controller->log($sql,"One_touch_put_appDet");
            
                     
           
            
            $res=$this->Query($this->conn, $sql);
            
             $this->controller->log($res,"One_touch_put_appDet");
           // $this->delete(array('NonSkylineJobID' => $nsjId   ));
            
            
            
            
            
               $apiA=$this->loadModel('APIAppointments');
               if(isset($res[0]['apID'])){
                   
                  $this->controller->log("One_touch_put_appDet - ".$res[0]['apID']."","One_touch_put_appDet");
        $apiA->PutAppointmentDetails($res[0]['apID']);
               }
               
               
               
               
               
        }
        
    }
    
    /**
     * getIdFromNetworkRef
     * 
     * Returns the JobID from the Network Ref (if the job exists) 
     * 
     * @param string $nID   The network ID of the job
     *        integer $rNo  The Network Reference number of the job
     * 
     * @return integer  The ID of the Job
     * 
     * @author Andrew Williams <a.williams@pccsuk.com> 
     **************************************************************************/
    
    public function getIdFromNetworkRef($nId, $rNo) {
        $sql = "
                SELECT
			`NonSkylineJobID`
		FROM
			`non_skyline_job`
		WHERE
			`NetworkID` = $nId
			AND `NetworkRefNo` = '$rNo'
               ";
        
        $result = $this->Query($this->conn, $sql);
        
        if ( count($result) > 0 ) {
            return($result[0]['NonSkylineJobID']);                              /* Job exists so return Job ID */
        } else {
            return(null);                                                       /* Not found return null */
        }
    }
    
    
    /**
     * Description
     * 
     * This method is used for to fetch a row from database.
     *
     * @param  array $args
     * 
     * @return array It contains row of the given primary key.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
     public function fetchRow($args) {
        
        
        /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT * FROM `non_skyline_job` WHERE NonSkylineJobID=:NonSkylineJobID';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        
        
        $fetchQuery->execute(array(':NonSkylineJobID' => $args['NonSkylineJobID']));
        $result = $fetchQuery->fetch();
        
        return $result;
     }
    
    
    
}

?>