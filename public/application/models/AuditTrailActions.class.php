<?php

require_once('CustomModel.class.php');
require_once('TableFactory.class.php');

/**
 * Description
 *
 * This class is used for handling database actions of Audit Trail Actions Page in Lookup Tables section under System Admin
 *
 * @author      Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
 * @version     1.0
 */


class AuditTrailActions extends CustomModel {
    
    private $conn;
    private $dbColumns = array('t1.AuditTrailActionID', 't1.ActionCode', 't1.Action',  't1.Type', 't1.Status', 't2.BrandName', 't1.BrandID');
    private $tables    = "audit_trail_action AS t1 LEFT JOIN brand AS t2 ON t1.BrandID=t2.BrandID";

    // Record Codes
    const JOB_CREATED          = 1;
    const JOB_CANCELLED        = 2;
    const JOB_CLOSED           = 3;
    const CUSTOMER_NAME        = 4;
    const CUSTOMER_ADDRESS     = 5; 
    const PHONE                = 6;
    const EMAIL                = 7; 
    const PRODUCT_DETAILS      = 8; 
    const ADDITIONAL_INFO      = 9; 
    const SERVICE_INSTRUCTIONS = 10; 
    const SERVICE_CENTRE       = 11; 
    const SERVICE_REPORT       = 12;
    const REPLICATE_JOB        = 13;
   
    public function __construct($controller) {
    
        parent::__construct($controller); 

        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] );  
        
        # get a list of audit_trail_action for this Brand

    }
    
   
    
    /**
     * Description
     * 
     * This method is for fetching data from database
     * 
     * @param array $args Its an associative array contains where clause, limit and order etc.
     * @global $this->conn
     * @global $this->tables
     * @global $this->dbColumns
     * @return array 
     * 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */   
    
    public function fetch($args) {
        
       
        /*
        
        if (( isset($args['StockCodeSearchStr']) && $args['StockCodeSearchStr'] != '')  || 
                (isset($args['ModelNoSearchStr']) && $args['ModelNoSearchStr'] != '' )) {
           
            $args['where'] = '(';
            
            if ( $args['StockCodeSearchStr'] != '') {
                
                for ( $i=0 ; $i<count($columns) ; $i++ ) {
		$args['where'] .= $columns[$i].' LIKE '.$this->conn->quote( '%'.$args['StockCodeSearchStr'].'%' ).' OR ';
                }
                
            }
            else
            {
                for ( $i=0 ; $i<count($columns) ; $i++ ) {
		$args['where'] .= $columns[$i].' LIKE '.$this->conn->quote( '%'.$args['ModelNoSearchStr'].'%' ).' OR ';
                }
            }
            
            
            $args['where'] = substr_replace( $args['where'], "", -3 );
            $args['where'] .= ')';
	}
        
        */
        
       // $output = $this->ServeDataTables($this->conn, $this->tables, $this->dbColumns, $args);
        
        if($this->controller->user->SuperAdmin)
        {
            $output = $this->ServeDataTables($this->conn, $this->tables, $this->dbColumns, $args);
        }
        else if(is_array($this->controller->user->Brands))
        {    
            
           $brandsList  = implode(",", array_keys($this->controller->user->Brands));
           
           
           
           if($brandsList)
           {
                $brandsList .= ",".$this->controller->SkylineBrandID;
           }    
          
          
            $args['where'] = "t1.BrandID IN (".$brandsList.")";

            $output = $this->ServeDataTables($this->conn, $this->tables, $this->dbColumns, $args);
       
        }
       
        return  $output;
        
    }
    
    
     /**
     * Description
     * 
     * This method calls update method if the $args contains primary key.
     * 
     * @param array $args Its an associative array contains all elements of submitted form.
    
     * @return array It contains status and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */   
    
    
     public function processData($args) {
         
         if(!isset($args['AuditTrailActionID']) || !$args['AuditTrailActionID'])
         {
               return $this->create($args);
         }
         else
         {
             return $this->update($args);
         }
     }
    
     /**
     * Description
     * 
     * This method finds the maximum action code for given brand in database table.
     * 
     * @param interger $BrandID This is primary key of brand table.
    
     * @return integer It returns maximum action code if it finds in the database table otherwise it returns 0.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */   
     public function getActionCode($BrandID) {
        
        
        /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT ActionCode FROM audit_trail_action WHERE BrandID=:BrandID ORDER BY AuditTrailActionID DESC LIMIT 0,1';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        
        
        //$this->controller->log(var_export($BrandID, true));
        
        $fetchQuery->execute(array(':BrandID' => $BrandID));
        $result = $fetchQuery->fetch();
        if(isset($result[0]))
        {
           return $result[0];
        }
        else
        {
             return 0;
        }
       
    }
     
    
    
    /**
     * Description
     * 
     * This method is used for to validate action code for given brand.
     *
     * @param interger $ActionCode  
     * @param interger $BrandID.
     * @param interger $AuditTrailActionID.
    
     * @return boolean.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function isValidAction($ActionCode, $BrandID, $AuditTrailActionID) {
        
         /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT AuditTrailActionID FROM audit_trail_action WHERE ActionCode=:ActionCode AND BrandID=:BrandID AND AuditTrailActionID!=:AuditTrailActionID';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $fetchQuery->execute(array(':ActionCode' => $ActionCode, ':BrandID' => $BrandID, ':AuditTrailActionID' => $AuditTrailActionID));
        $result = $fetchQuery->fetch();
        
        if(is_array($result) && $result['AuditTrailActionID'])
        {
                return false;
        }
        
        return true;
    
    }
    
    
    /**
     * Description
     * 
     * This method is used for to insert data into database.
     *
     * @param array $args  
     * @return array It contains status of operation and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    
    public function create($args) {
        
        
        /* Execute a prepared statement by passing an array of values */
//        $sql = 'INSERT INTO audit_trail_action (Action, ActionCode, Type, Status, BrandID)
//            VALUES(:Action, :ActionCode, :Type, :Status, :BrandID)';

        $fields = explode(', ', 'Action, ActionCode, Type, Status, BrandID');
        $fields = array_combine($fields , $fields);
        
        $sql = TableFactory::AuditTrailAction()->insertCommand($fields);
        
//        $update = $this->conn->prepare( $sql );
//        
//        $update->execute( array(
//            ':JobID' => '34589120',
//            ':ItemLocation' => 'Test'
//        ) );        
        
        if(!isset($args['ActionCode']) || !$args['ActionCode'])
        {
            $args['ActionCode'] = $this->getActionCode($args['BrandID'])+1;//Preparing next action code.
        }
       // $this->controller->log(var_export('tessss', true));
        
        if($this->isValidAction($args['ActionCode'], $args['BrandID'], 0))
        {
            $insertQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));


            $insertQuery->execute(array(':Action' => $args['Action'], ':ActionCode' => $args['ActionCode'], ':Type' => $args['Type'], ':Status' => $args['Status'], ':BrandID' => $args['BrandID']));


            return array('status' => 'OK',
                        'message' => $this->controller->page['data_inserted_msg']);
        }
        else
        {
            
            return array('status' => 'ERROR',
                        'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang));
        }
    }
    
    
    /**
     * Description
     * 
     * This method is used for to fetch a row from database.
     *
     * @param array $args
     * @return array It contains row of the given primary key.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    
    public function fetchRow($args) {
        
        
        /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT AuditTrailActionID, ActionCode, Action, Type, Status, BrandID FROM audit_trail_action WHERE AuditTrailActionID=:AuditTrailActionID';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        
        
        $fetchQuery->execute(array(':AuditTrailActionID' => $args['AuditTrailActionID']));
        $result = $fetchQuery->fetch();
        
        return $result;
    }
    
    /**
     * 
     * setAuditCode fn
     * @param string $audit_action
     * @return void
     * @author Simon
     * @version obsolete
     ********************************************/    
    public function setAuditCode( $audit_action = '' ){
//        if(isset($this->audit_action_constant[ $audit_action ])){
//            $this->audit_code = $this->audit_action_constant[ $audit_action ];
//        }else{
//            $this->controller->log("audit action '$audit_action' is not listed in AuditTrailAction");
//        }
        
    }
    
    
    /**
     * Description
     * 
     * This method is used for to udpate a row into database.
     *
     * @param array $args
     * @return array It contains status of operation and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function update($args) {
        
        if($this->isValidAction($args['ActionCode'], $args['BrandID'], $args['AuditTrailActionID']))
        {        
            /* Execute a prepared statement by passing an array of values */
//            $sql = 'UPDATE audit_trail_action SET Action=:Action, ActionCode=:ActionCode, Type=:Type, Status=:Status, BrandID=:BrandID
//                WHERE AuditTrailActionID=:AuditTrailActionID';
            
            $fields = explode(', ', 'Action, ActionCode, Type, Status, BrandID');
            $fields = array_combine($fields , $fields);
            
            $where = 'AuditTrailActionID=:AuditTrailActionID';

            $sql = TableFactory::AuditTrailAction()->updateCommand($fields, $where);
                    

            $updateQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $updateQuery->execute(array(':Action' => $args['Action'], ':ActionCode' => $args['ActionCode'], ':Type' => $args['Type'], ':Status' => $args['Status'], ':BrandID' => $args['BrandID'], ':AuditTrailActionID' => $args['AuditTrailActionID']));


            return array('status' => 'OK',
                        'message' => $this->controller->page['data_updated_msg']);
        }
        else
        {
             return array('status' => 'ERROR',
                        'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang));
        }
    }
    
    public function delete(/*$args*/) {
        return array('status' => 'OK',
                     'message' => $this->controller->page['data_deleted_msg']);
    }
    
    
    
}
?>
