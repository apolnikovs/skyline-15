{extends "DemoLayout.tpl"}

{block name=config}

{$PageId = $DiaryDefaultsPage}
{$def=0}
{if $SuperAdmin}{$def=1}{/if}
 {if !isset($allocOnly)||$allocOnly!='AllocationOnly'}
     {$def=1}
     {/if}
    {if $allocOnly=='NoViamente'}
     {$def=2}
     {/if}
{$PageId = $AppointmentDiaryPage}
{/block}

{block name=afterJqueryUI}
   <script type="text/javascript" src="{$_subdomain}/js/jquery.combobox.js"></script>
   <script type="text/javascript" src="{$_subdomain}/js/ui/jquery.ui.timepicker.js?v=0.3.1"></script>
   <script src="{$_subdomain}/js/jpicker-1.1.6.js" type="text/javascript"></script>
   
   <link rel="Stylesheet" type="text/css" href="{$_subdomain}/css/themes/pccs/jPicker-1.1.6.css" />
   <link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/jquery.ui.timepicker.css?v=0.3.1" type="text/css" />
   <link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/style.css" type="text/css" media="screen" charset="utf-8" /> 
   

   
   <style type="text/css" >
      
    .ui-combobox-input {
        width:270px;
     }   
   
    </style>

    <script type="text/javascript">
    $.fn.jPicker.defaults.images.clientPath = "{$_subdomain}/css/themes/pccs/images/colorPicker/";
    </script>

{/block}

{block name=scripts}
{*<link rel="stylesheet" href="{$_subdomain}/css/colorbox/colorbox.css" type="text/css" charset="utf-8" />*} 

{*<script type="text/javascript" src="{$_subdomain}/js/jquery.dataTables.min.js"></script>*}
{*<script type="text/javascript" src="{$_subdomain}/js/jquery.dataTablesPCCS.js"></script>*}
<script type="text/javascript" src="{$_subdomain}/js/functions.js"></script>
{*<script type="text/javascript" src="{$_subdomain}/js/jquery.jlabel-1.3.min.js"></script>*} 

{*<script type="text/javascript" src="{$_subdomain}/js/jquery.form.min.js"></script>*}
{*<script type="text/javascript" src="{$_subdomain}/js/jquery.validate.min.js"></script>*}
{*<script type="text/javascript" src="{$_subdomain}/js/additional-methods.min.js"></script>*}
        



<script type="text/javascript">
{* The below Code added by Raju for TBL 432 code Starts Here *}
$(document).on('click', '#fDayHistroyDetails', function() {
    var routeDate=$("#ViamenteConsolidation").val().replace("/","-");
    routeDate=routeDate.replace("/","-");
    $.colorbox({
        href :	'{$_subdomain}/AppointmentDiary/getFinalisedDayHistoryPopup/'+routeDate,
        title: "{$page['Text']['finalisedday_history_page_legend']|escape:'html'}",
        width:	'800px',
        scrolling:	false,
        overlayClose: false,
        fixed: true
    });
    return false;
});
{* The below Code added by Raju for TBL 432 code Ends Here *}
    
       $(document).on('change', '#csize', 
                      function() {
                      ab=69.034*($('#csize').val()*1).toFixed(2);
                                         ab=ab.toFixed(2);
                                     $('#csizeEx').val(ab+" X "+ab+" M")
    
    });
    $(document).ready(function() {
    $('.helpTextIconQtip').each(function()
    {
        $HelpTextCode =  $(this).attr("id");
        // We make use of the .each() loop to gain access to each element via the "this" keyword...
        $(this).qtip(
        {
            hide: 'unfocus',
            events: {
         
                hide: function(){
               
                  $(this).qtip('api').set('content.text', '<img src="{$_subdomain}/images/ajax-loader.gif" '); // Direct API method 
                }
            },
            content: {
                // Set the text to an image HTML string with the correct src URL to the loading image you want to use
                text: '<img src="{$_subdomain}/images/ajax-loader.gif" >',
                ajax: {
                    url: '{$_subdomain}/Popup/helpText/' + urlencode($HelpTextCode) + '/Qtip=1/' + Math.random(),
                    once: false // Re-fetch the content each time I'm shown
                },
                title: {
                    text: 'Help', // Give the tooltip a title using each elements text
                    button: true
                }
            },
            position: {
                at: 'bottom center', // Position the tooltip above the link
                my: 'top center',
                viewport: $(window), // Keep the tooltip on-screen at all times
                effect: true // Disable positioning animation
            },
            show: {
                event: 'click',
               
                solo: true // Only show one tooltip at a time
            },
             
            style: {
                classes: 'qtip-tipped  qtip-shadow'
            }
        })
    })
    var  oTable3 = $('#bank_holdiay_table').dataTable( {
        sDom: '<"left"lr><"top"f>t<"bottom"><"center"pi><"clear">',
        bPaginate: true,
        sPaginationType: "full_numbers",
        aLengthMenu: [[ 10, 15, 25, 50, 100 , -1], [10, 15, 25, 50, 100, "All"]],
        iDisplayLength : 10,
        bFilter: false,
        bInfo: true,
        aoColumns: [
            { bVisible:    false },
            { sType: "date-eu" },
            null,
            { bVisible:    true,  bSortable: false }
        ],
        aaSorting: [ [1,'asc'] ]
    });
    $('#bank_holdiay_table_paginate').css("margin-right","45px");
    $('#bank_holdiay_table_info').css("margin-right","45px");
    $("#bhInsertBtn").click(function() {
        $.post("{$_subdomain}/AppointmentDiary/showBankHolidayCard",{ cId:{$cId}, o:false }, function(data) {
            $.colorbox({ html:data, title:"Insert Bank Holiday",escKey: false,overlayClose: false, 
                onComplete:function(data){
                    $('#bholidayDate').datepicker( { dateFormat: "dd/mm/yy"});
                }
            });
        });
});
    
     
                                         ab=69.034*($('#csize').val()*1).toFixed(2);
                                         ab=ab.toFixed(2);
                                     $('#csizeEx').val(ab+" X "+ab+" M")
     $.validator.addMethod("time", function(value, element) {  
                return this.optional(element) || /^(([0-1]?[0-9])|([2][0-3])):([0-5]?[0-9])?$/i.test(value);  
                }, "{$page['Errors']['time']|escape:'html'}");
    
     
    function tpAMStartOnHourShowCallback(hour) {
        var tpEndHour = $('#AMTimeTo').timepicker('getHour');
        // all valid if no end time selected
        if ($('#AMTimeTo').val() == '') { return true; }
        // Check if proposed hour is prior or equal to selected end time hour
        if (hour <= tpEndHour) { return true; }
        // if hour did not match, it can not be selected
        return false;
    }
    function tpAMStartOnMinuteShowCallback(hour, minute) {
        var tpEndHour = $('#AMTimeTo').timepicker('getHour');
        var tpEndMinute = $('#AMTimeTo').timepicker('getMinute');
        // all valid if no end time selected
        if ($('#AMTimeTo').val() == '') { return true; }
        // Check if proposed hour is prior to selected end time hour
        if (hour < tpEndHour) { return true; }
        // Check if proposed hour is equal to selected end time hour and minutes is prior
        if ( (hour == tpEndHour) && (minute < tpEndMinute) ) { return true; }
        // if minute did not match, it can not be selected
        return false;
    }

    function tpAMEndOnHourShowCallback(hour) {
        var tpStartHour = $('#AMTimeFrom').timepicker('getHour');
        // all valid if no start time selected
        if ($('#AMTimeFrom').val() == '') { return true; }
        // Check if proposed hour is after or equal to selected start time hour
        if (hour >= tpStartHour) { return true; }
        // if hour did not match, it can not be selected
        return false;
    }
    function tpAMEndOnMinuteShowCallback(hour, minute) {
        var tpStartHour = $('#AMTimeFrom').timepicker('getHour');
        var tpStartMinute = $('#AMTimeFrom').timepicker('getMinute');
        // all valid if no start time selected
        if ($('#AMTimeFrom').val() == '') { return true; }
        // Check if proposed hour is after selected start time hour
        if (hour > tpStartHour) { return true; }
        // Check if proposed hour is equal to selected start time hour and minutes is after
        if ( (hour == tpStartHour) && (minute > tpStartMinute) ) { return true; }
        // if minute did not match, it can not be selected
        return false;
    }
    
    
    
    function tpPMStartOnHourShowCallback(hour) {
        var tpEndHour = $('#PMTimeTo').timepicker('getHour');
        // all valid if no end time selected
        if ($('#PMTimeTo').val() == '') { return true; }
        // Check if proposed hour is prior or equal to selected end time hour
        if (hour <= tpEndHour) { return true; }
        // if hour did not match, it can not be selected
        return false;
    }
    function tpPMStartOnMinuteShowCallback(hour, minute) {
        var tpEndHour = $('#PMTimeTo').timepicker('getHour');
        var tpEndMinute = $('#PMTimeTo').timepicker('getMinute');
        // all valid if no end time selected
        if ($('#PMTimeTo').val() == '') { return true; }
        // Check if proposed hour is prior to selected end time hour
        if (hour < tpEndHour) { return true; }
        // Check if proposed hour is equal to selected end time hour and minutes is prior
        if ( (hour == tpEndHour) && (minute < tpEndMinute) ) { return true; }
        // if minute did not match, it can not be selected
        return false;
    }

    function tpPMEndOnHourShowCallback(hour) {
        var tpStartHour = $('#PMTimeFrom').timepicker('getHour');
        // all valid if no start time selected
        if ($('#PMTimeFrom').val() == '') { return true; }
        // Check if proposed hour is after or equal to selected start time hour
        if (hour >= tpStartHour) { return true; }
        // if hour did not match, it can not be selected
        return false;
    }
    function tpPMEndOnMinuteShowCallback(hour, minute) {
        var tpStartHour = $('#PMTimeFrom').timepicker('getHour');
        var tpStartMinute = $('#PMTimeFrom').timepicker('getMinute');
        // all valid if no start time selected
        if ($('#PMTimeFrom').val() == '') { return true; }
        // Check if proposed hour is after selected start time hour
        if (hour > tpStartHour) { return true; }
        // Check if proposed hour is equal to selected start time hour and minutes is after
        if ( (hour == tpStartHour) && (minute > tpStartMinute) ) { return true; }
        // if minute did not match, it can not be selected
        return false;
    }
    
    
    
     function adjustPanelHeights ()
     {
         
        $('#secondDiv').css("height", '');
        $('#firstDiv').css("height", ''); 
        $('#innerFirstDiv').css("height", '');  
         
        $height1 = $('#firstDiv').height();
        $height2 = $('#secondDiv').height();

        $maxHeight = 400;
        
        if($height1>$maxHeight)
        {
           $maxHeight = $height1;
        } 
        
        if($height2>$maxHeight)
        {
           $maxHeight = $height2;
        } 

        $maxHeight = $maxHeight+50;
        
        $('#firstDiv').css("height", $maxHeight+"px");
        $('#secondDiv').css("height", $maxHeight+"px");
        $('#innerFirstDiv').css("height", ($maxHeight-30)+"px");
     }
     
     
    
     
     function showHomePage()
     {
        
        $("#formDiv").hide();
       // $('#innerFirstDiv').css("height", '');
      //  $('#firstDiv').css("height", '');
       // $('#secondDiv').css("height", '');
        $("#defaultDiv").show();
        adjustPanelHeights();
     }
    
    
     //  adjustPanelHeights();
     
     
       
       
       //Click handler for Travel Speed Info.
        $(document).on('click', '#TravelSpeedInfo', 
                      function() {

                        //showDataForm();
                        
                        
                         $("#defaultDiv").hide();
                         $("#Default_ID1_Div").hide();
                         $("#Default_ID2_Div").hide();
                         $("#Default_ID3_Div").hide();
                         $("#Default_ID4_Div").hide();
                         $("#Default_ID5_Div").hide();
                         $("#Default_ID6_Div").hide();
                         $("#Default_ID7_Div").hide();
                         $("#Default_ID8_Div").hide();
                         $("#Default_ID9_Div").hide();
                         
                         $("#formDiv").show();
                         adjustPanelHeights();

                        //  $("#saveRecord").show();
                        // $("#cancelChanges").show();
                        
                        
                        return false;
                          
                      });
                      
        
        //Click handler for add engineer.
        $(document).on('click', '#closeInfo', 
                      function() {

                       // showHomePage();
                       
                         $("#defaultDiv").hide();
                         $("#Default_ID1_Div").hide();
                         $("#Default_ID3_Div").hide();
                         $("#Default_ID4_Div").hide();
                         $("#Default_ID5_Div").hide();
                         $("#Default_ID6_Div").hide();
                         $("#Default_ID7_Div").hide();
                         $("#Default_ID8_Div").hide();
                         $("#Default_ID9_Div").hide();
                         
                         $("#Default_ID2_Div").show();
                         $("#formDiv").hide();
                         adjustPanelHeights();
                        
                        return false;
                          
                      });
                      
                      
                      
          $('#AMTimeFrom').timepicker({
                defaultTime: '00:00',
                onHourShow: tpAMStartOnHourShowCallback,
                onMinuteShow: tpAMStartOnMinuteShowCallback
            });

            $('#AMTimeTo').timepicker({
                defaultTime: '00:00',    
                onHourShow: tpAMEndOnHourShowCallback,
                onMinuteShow: tpAMEndOnMinuteShowCallback
            });
            {if  isset($timeDefRow)}
            {for $e=0 to $timeDefRow|@count-1}
            $('#EarliestWorkday_{$e}').timepicker({ defaultTime: '00:00' });
            $('#LatestWorkday_{$e}').timepicker({ defaultTime: '00:00' });
            $('#EarliestWeekend_{$e}').timepicker({ defaultTime: '00:00' });
            $('#LatestWeekend_{$e}').timepicker({ defaultTime: '00:00' });
            {/for}
                {/if}


            $('#PMTimeFrom').timepicker({
                defaultTime: '00:00',
                onHourShow: tpPMStartOnHourShowCallback,
                onMinuteShow: tpPMStartOnMinuteShowCallback
            });

            $('#PMTimeTo').timepicker({
                defaultTime: '00:00',
                onHourShow: tpPMEndOnHourShowCallback,
                onMinuteShow: tpPMEndOnMinuteShowCallback
            });            
                      
              $('#engDefStart').timepicker({
                defaultTime: '00',
                 showMinutes: false,
    showPeriod: false,
    showLeadingZero: false
                
            });

            $('#engDefEnd').timepicker({
                defaultTime: '00',
                 showMinutes: false,
    showPeriod: false,
    showLeadingZero: false
               
            });          
             
             
           $('#PasswordProtectNextDayBookingsTime').timepicker({ 
                    defaultTime: '00:00'
                }); 
             
           $('#SendRouteMapsEmail').timepicker({ 
                    defaultTime: '00:00'
                });           
                      
                      
        //Click handler for cancelChanges.
        $(document).on('click', '.cancelChanges', 
                      function() {
                      
                        
                        {if $DiaryWizardSetup}
                            
                              document.location.href = "{$_subdomain}/AppointmentDiary/diaryDefaults/spID="+$("#ServiceProviderID").val()+"/sPage={$sPage}";
                            
                        {else}    
                            
                              document.location.href = "{$_subdomain}/AppointmentDiary/diaryDefaults/spID="+$("#ServiceProviderID").val();
                      
                        {/if}
                        return false;
                          
                      }); 
                      
                      
        //Click handler for AutoSelectDayChkBox.
        $(document).on('click', '#AutoSelectDayChkBox', 
                      function() {
                      
                        displayTableFields();
                          
                      }); 
                      
        //Click handler for AutoDisplayTableChkBox.
        $(document).on('click', '#AutoDisplayTableChkBox', 
                      function() {
                      
                       clearAutoDisplayTableFields();
                          
                      });             
                      
        
        function clearAutoDisplayTableFields()
        {
            
            if ($('#AutoDisplayTableChkBox').is(':checked')) 
            {

            } 
            else 
            {

               $('input[name="AutoDisplayTable"]').removeAttr("checked"); 

            } 
        }              
                      
        //Click handler for AutoSelectDay.
        $(document).on('click', 'input[name="AutoSelectDay"]', 
                      function() {
                      
                      $('#AutoSelectDayChkBox').attr("checked", "checked"); 
                      displayTableFields();    
                          
                      }); 
                      
                      
                      
                      
                      
                      
        //Click handler for AutoDisplayTable.
         $(document).on('click', 'input[name="AutoDisplayTable"]', 
                      function() {
                      
                      $('#AutoDisplayTableChkBox').attr("checked", "checked"); 
                      displayTableFields();    
                          
                      });            
                      
          function displayTableFields()
          {
                //alert($('#AutoSelectDayChkBox').is(':checked'));
                
                if ($('#AutoSelectDayChkBox').is(':checked')) 
                {
                   $("#AutoDisplayTableChkBox").removeAttr("disabled");  
                   $('input[name="AutoDisplayTable"]').removeAttr("disabled"); 

                } 
                else 
                {
                   $("#AutoDisplayTableChkBox").removeAttr("checked"); 
                   $("#AutoDisplayTableChkBox").attr("disabled", "disabled");  
                   $('input[name="AutoDisplayTable"]').removeAttr("checked"); 
                   $('input[name="AutoDisplayTable"]').attr("disabled", "disabled"); 
                   $('input[name="AutoSelectDay"]').removeAttr("checked"); 
                } 
            
          }    
                      
                      
        $( "#ViamenteConsolidation" ).datepicker({
			dateFormat: "dd/mm/yy",
                        showOn: "button",
			buttonImage: "{$_subdomain}/css/Skins/{$_theme}/images/calendar.png",
			buttonImageOnly: true,
                        onClose: function(dateText, inst) { 
                        
                        if($("#ViamenteConsolidation").val()!="dd/mm/yyyy")
                        {    
                            $("#ViamenteConsolidation").removeClass("auto-hint"); 
                        }
                        
                        },
                        onSelect: function(dateText, inst) { 
                            $.post("{$_subdomain}/AppointmentDiary/getFinalizedDayDetails",{ fDate:$("#ViamenteConsolidation").val() },
                            function(data) {
                                if(data >= 1)
                                    $("#fDayHistroyDetails").show();
                                else
                                    $("#fDayHistroyDetails").hide();
                            });
                        }
                    });              
                      
                      
                      
       
     
        
        
      {if $tInfo} 
          
           $("#centerInfoText").html("{$page['Text']['data_saveed_msg']|escape:'html'}").fadeIn('slow').delay("5000").fadeOut('slow');  
      
      {/if}
          
        
         {*             
     function showDataForm()
     {
        $("#defaultDiv").hide();
       // $('#secondDiv').css("height", '');
        $("#formDiv").show();
        adjustPanelHeights();

        $("#saveRecord").show();
       // $("#cancelChanges").show();

     }*}
     
    
    
    
    
     {if $SuperAdmin}
     
        $( "#ServiceProviderID" ).combobox( { change: function() { document.location.href="{$_subdomain}/AppointmentDiary/diaryDefaults/spID="+$("#ServiceProviderID").val(); } } );
    
     {/if}
 
 
 
     //Click handler for TagAllJobs check box.
        $(document).on('click', "#TagAllJobs", function() { 


            if ($(this).is(':checked')) 
            {
                $(".checkBoxPopupPage").attr("checked", "checked");
            } 

            $("#UnTagAllJobs").removeAttr("checked");
        });



        //Click handler for UnTagAllJobs check box.
        $(document).on('click', "#UnTagAllJobs", function() { 


            if ($(this).is(':checked')) 
            {
               $(".checkBoxPopupPage").removeAttr("checked"); 
            } 


            $("#TagAllJobs").removeAttr("checked");

         });
 
 
 
     $(document).on('click', "#insert_save_btn", function() { 
                
                        $("#insert_save_btn").hide();
                        $("#cancel_btn").hide();
                        $("#processDisplayText").show();
                 
                        $('#individual_engineers_values').html('');
                 
                        $html_str = '';
                 
                         $('.checkBoxPopupPage').each(function() {
                         
                            if(this.checked)
                            {    
                               $html_str += '<input type="hidden" name="IndividualEngineers[]" value="'+$(this).val()+'" > ';
                            }
                            
                        });
                        
                        
                        $('#individual_engineers_values').html($html_str);
                        
                     $.colorbox.close();   
                        
                     return false;   
                
                }); 
      
      
      
        
               $(document).on('click', "#cancel_btn", function() { 
                
                 $.colorbox.close();
                 
                });
 
    
    
    $(document).on('click', "#SetupUniqueTimeSlotPostcodes", function() { 
                
                    if ($('#SetupUniqueTimeSlotPostcodes').is(':checked')) {
                    
                        if($('#OriginalSetupUniqueTimeSlotPostcodes').val()=="No")
                        {    
                            if(confirm("{$page['Errors']['switch_to_ampm']|escape:'html'}"))
                            {
                                return true;        
                            } 
                            else
                            {
                                return false;
                            }    
                        }
                    } 
                    else {
                     
                        if($('#OriginalSetupUniqueTimeSlotPostcodes').val()=="Yes")
                        {    
                            if(confirm("{$page['Errors']['switch_to_allday']|escape:'html'}"))
                            {
                                return true;        
                            } 
                            else
                            {
                                return false;
                            }    
                        }
                    
                    } 
                 
                });
    
    
    
   
    $(document).on('click', '#individual_engineers', 
                            function() {
                            
                                if($("#ViamenteConsolidation").val())
                                {
                                        
                                        
                                        $.post("{$_subdomain}/Data/isViamenteEndDayDateExists/"+$("#ServiceProviderID").val()+"/",        

                                                        $("#diaryDefaultsForm").serialize(),      
                                                        function(data){
                                                            
                                                            var p = eval("(" + data + ")");

                                                                    if(p['status']=="ERROR")
                                                                    {
                                                                        
                                                                       alert("{$page['Errors']['consolidated_date_notvalid']|escape:'html'}"); 
                                                                       $("#ViamenteConsolidation").focus();
                                      
                                                                    }
                                                                    else if(p['status']=="OK")
                                                                    {
                                                                             //It opens color box popup page.              
                                                                                $.colorbox( {   inline:true,
                                                                                                href:"#SelectIndividualEngineers",
                                                                                                title: '',
                                                                                                opacity: 0.75,
                                                                                                height:540,
                                                                                                width:720,
                                                                                                overlayClose: false,
                                                                                                escKey: false,
                                                                                                onLoad: function() {
                                                                                                   // $('#cboxClose').remove();
                                                                                                },
                                                                                                onClosed: function() {

                                                                                                   //location.href = "#EQ7";
                                                                                                   
                                                                                                   $("#insert_save_btn").show();
                                                                                                   $("#cancel_btn").show();
                                                                                                   $("#processDisplayText").hide();

                                                                                                   
                                                                                                   
                                                                                                },
                                                                                                onComplete: function()
                                                                                                {
                                                                                                    $.colorbox.resize();
                                                                                                }

                                                                                             }); 
                                                                       
                                                                    }
                                                        }); //Post ends here...
                                        
                                        
                                       
                                }
                                else
                                {
                                        
                                      alert("{$page['Errors']['date_first']|escape:'html'}"); 
                                      $("#ViamenteConsolidation").focus();
                                }
                            
                                return false;
                            });
   
     
    
    
     
        $(document).on('click', '.DefaultsRadioButton', 
                      function() {
                      
                         
                        //$DefaultID = $(this).attr("id").replace("Default_ID", ""); ;
                       
                        $DefaultID = $(this).val();
                        document.location.href = "{$_subdomain}/AppointmentDiary/diaryDefaults/spID="+$("#ServiceProviderID").val()+"/sPage="+$DefaultID;
                       
                       /*
                         $("#defaultDiv").hide();
                         $("#formDiv").hide();
                         $("#Default_ID1_Div").hide();
                         $("#Default_ID2_Div").hide();
                         $("#Default_ID3_Div").hide();
                         $("#Default_ID4_Div").hide();
                         
                         
                         if($DefaultID)
                         {
                             
                             $("#"+$DefaultID+"_Div").show();
                            
                         }    
                         else
                         {
                             $("#defaultDiv").show();
                         }    
                        
                         adjustPanelHeights();
                         
                         */
                        return false;
                          
                      }); 
    
    
    
    
    
    
    
     $(document).on('click', '#saveRecord2', 
                            function() {
                            
                                $("#GeneralDefaults").val('GeneralDefaults2');
                                
                                
                                 
                                if($("label.fieldError:visible").length>0)
                                {
                                    
                                }
                                else
                                {
                                    $("#processDisplayText2").show();
                                    $("#saveRecord2").hide();
                                    $("#cancelChanges2").hide();
                                }
                            
                                 $('#diaryDefaultsForm').validate({
                                         
                                         
                                            ignore: '',
                                        
                                            rules:  {
                                                        DefaultTravelTime:
                                                        {
                                                            required: true,
                                                            digits: true
                                                        },
                                                        DefaultTravelSpeed:
                                                        {
                                                            required: true,
                                                            digits: true,
                                                            max: 200,
                                                            min:50
                                                        },
                                                        ViamenteConsolidation:
                                                        {
                                                            dateITA: true
                                                        },
                                                        RunViamenteToday:
                                                        {
                                                            required: function (element) {
                                                                if($("#ViamenteConsolidation").val()=="{$CurrentDate}")
                                                                {
                                                                    
                                                                    return true;
                                                                }
                                                                else
                                                                {
                                                                    
                                                                    return false;
                                                                }
                                                            }
                                                        },
                                                        SendRouteMapsEmail:
                                                        {
                                                            time:true
                                                        }
                                                    
                                                   },
                                            messages: {
                                            
                                                        DefaultTravelTime:
                                                        {
                                                            required: "{$page['Errors']['default_travel_time']|escape:'html'}",
                                                            digits: "{$page['Errors']['digits']|escape:'html'}"
                                                            
                                                        },
                                                        DefaultTravelSpeed:
                                                        {
                                                            required: "{$page['Errors']['default_travel_speed']|escape:'html'}",
                                                            digits: "{$page['Errors']['digits']|escape:'html'}"
                                                        },
                                                        ViamenteConsolidation:
                                                        {
                                                            dateITA: "{$page['Errors']['reset_date_format']|escape:'html'}"
                                                        },
                                                        RunViamenteToday:
                                                        {
                                                            required: "{$page['Errors']['run_viamente_today']|escape:'html'}"
                                                        },
                                                        SendRouteMapsEmail:
                                                        {
                                                            time: "{$page['Errors']['time']|escape:'html'}"
                                                        }
                                                      
                                                    },
                                           
                                           errorPlacement: function(error, element) {
                                                
        
                                                error.insertAfter( element );
                                              
                                                
                                                $("#processDisplayText2").hide();
                                                $("#saveRecord2").show();
                                                $("#cancelChanges2").show();
                                                
                                                adjustPanelHeights();
                                                
                                            },
                                            errorClass: 'fieldError',
                                            onkeyup: false,
                                            onblur: false,
                                            errorElement: 'label',

                                            submitHandler: function() {
                                                
                                               var $submitDetails = true; 
                                               
                                               if($("#ViamenteConsolidation").val()!="")
                                               {
                                                    $("#processDisplayText2").hide();
                                                    $("#saveRecord2").show();
                                                    $("#cancelChanges2").show();
                                                   
                                                   
                                                   if($("#ServiceProviderID").val()==64){
                                                  
                                                  $submitDetails = false;
                                                  $.colorbox({ 
 
                        html:$('#visualendayDiv').html(),
                        title: "End Day",
                        opacity: 75,
                        
                        overlayClose: false,
                        escKey: false,
                        title:false,
                        onComplete:function(data){
                        
                        
                         
                        }

                });
                                                  
                                                   }else{
                                                   
                                                   
                                                    if(confirm("{$page['Errors']['confirm_consolidation']|escape:'html'}"))
                                                    {
                                                        $submitDetails = true; 
                                                        
                                                        $("#processDisplayText2").show();
                                                        $("#saveRecord2").hide();
                                                        $("#cancelChanges2").hide();
                                                        
                                                    }
                                                    else
                                                    {
                                                        $submitDetails = false; 
                                                        
                                                        $("#processDisplayText2").hide();
                                                        $("#saveRecord2").show();
                                                        $("#cancelChanges2").show();
                                                        
                                                    }
                                               }
                                               }
                                               if($submitDetails)
                                               {
                                           
                                                   
                                           
                                                    $.post("{$_subdomain}/AppointmentDiary/ProcessData/ServiceProviderSkillsSet/",        

                                                            $("#diaryDefaultsForm").serialize(),      
                                                            function(data){
                                                                // DATA NEXT SENT TO COLORBOX
                                                                var p = eval("(" + data + ")");

                                                                    if(p['status']=="ERROR")
                                                                        {

                                                                            $("#errorCenterInfoText").html(p['message']).css('color','red').fadeIn('slow').delay("5000").fadeOut('slow');  

                                                                            $("#processDisplayText2").hide();
                                                                            $("#saveRecord2").show();
                                                                            $("#cancelChanges2").show();

                                                                        }
                                                                        else if(p['status']=="OK")
                                                                        {

                                                                             {if $DiaryWizardSetup}
                                                                           
                                                                             if($("#navigatePage").val()=="1")
                                                                             {
                                                                                 document.location.href = "{$_subdomain}/AppointmentDiary/diaryDefaults/spID="+$("#ServiceProviderID").val()+"/sPage=3";    
                                                                             }
                                                                             else if($("#navigatePage").val()=="-1")
                                                                             {
                                                                                 document.location.href = "{$_subdomain}/AppointmentDiary/diaryDefaults/spID="+$("#ServiceProviderID").val()+"/sPage=1";    
                                                                             }
                                                                             else
                                                                             {
                                                                                document.location.href = "{$_subdomain}/AppointmentDiary/diaryDefaults/spID="+$("#ServiceProviderID").val()+"/sPage=2";
                                                                             }
                                                                           
                                                                           {else}
                                                                           
                                                                            
                                                                             document.location.href = "{$_subdomain}/AppointmentDiary/diaryDefaults/spID="+$("#ServiceProviderID").val()+"/tInfo=1";
                                                                            
                                                                           {/if} 


                                                                          // document.location.href = "{$_subdomain}/AppointmentDiary/diaryDefaults/spID="+$("#ServiceProviderID").val()+"/tInfo=1";


                                                                        }
                                                            }); //Post ends here...

                                                }
                                                else
                                                {
                                                    $("#processDisplayText2").hide();
                                                    $("#saveRecord2").show();
                                                    $("#cancelChanges2").show();
                                                
                                                }
                                                
                                            }
                                                    
                            
                            
                            });
    
    });
    
    
    
    
    
    $(document).on('click', '#saveRecord4', 
                            function() {
                            
                                $("#GeneralDefaults").val('GeneralDefaults4');
                                
                                
                                 
                                if($("label.fieldError:visible").length>0)
                                {
                                    
                                }
                                else
                                {
                                    $("#processDisplayText4").show();
                                    $("#saveRecord4").hide();
                                    $("#cancelChanges4").hide();
                                }
                            
                                 $('#diaryDefaultsForm').validate({
                                         
                                         
                                            ignore: '',
                                        
                                            rules:  {
                                            
                                                        DiaryAdministratorEmail:
                                                        {
                                                            required: true,
                                                            email: true
                                                        },
                                                        UnlockingPassword:
                                                        {
                                                            required: true
                                                        },
                                                        PasswordProtectNextDayBookingsTime:
                                                        {
                                                            time: true
                                                        }
                                                   },
                                            messages: {
                                            
                                                        DiaryAdministratorEmail:
                                                        {
                                                            required: "{$page['Errors']['email']|escape:'html'}",
                                                            email: "{$page['Errors']['valid_email']|escape:'html'}"
                                                        },
                                                        UnlockingPassword:
                                                        {
                                                            required: "{$page['Errors']['unlocking_password']|escape:'html'}"
                                                            
                                                        },
                                                        PasswordProtectNextDayBookingsTime:
                                                        {
                                                            time: "{$page['Errors']['valid_time']|escape:'html'}"
                                                        }
                                                      
                                                    },
                                           
                                           errorPlacement: function(error, element) {
                                                
        
                                                error.insertAfter( element );
                                              
                                                
                                                $("#processDisplayText4").hide();
                                                $("#saveRecord4").show();
                                                $("#cancelChanges4").show();
                                                $("label.fieldError").css("margin-left", "295px");
                                                adjustPanelHeights();
                                                
                                            },
                                            errorClass: 'fieldError',
                                            onkeyup: false,
                                            onblur: false,
                                            errorElement: 'label',

                                            submitHandler: function() {
                                                
                                                    var $submitDetails = true; 
                                               
                                                    $.post("{$_subdomain}/AppointmentDiary/ProcessData/ServiceProviderSkillsSet/",        

                                                            $("#diaryDefaultsForm").serialize(),      
                                                            function(data){
                                                                // DATA NEXT SENT TO COLORBOX
                                                                var p = eval("(" + data + ")");

                                                                    if(p['status']=="ERROR")
                                                                        {

                                                                            $("#errorCenterInfoText").html(p['message']).css('color','red').fadeIn('slow').delay("5000").fadeOut('slow');  

                                                                            $("#processDisplayText4").hide();
                                                                            $("#saveRecord4").show();
                                                                            $("#cancelChanges4").show();

                                                                        }
                                                                        else if(p['status']=="OK")
                                                                        {


                                                                            {if $DiaryWizardSetup}
                                                                           
                                                                             if($("#navigatePage").val()=="1")
                                                                             {
                                                                                 document.location.href = "{$_subdomain}/AppointmentDiary/diaryDefaults/spID="+$("#ServiceProviderID").val()+"/sPage=6";    
                                                                             }
                                                                             else if($("#navigatePage").val()=="-1")
                                                                             {
                                                                                 document.location.href = "{$_subdomain}/AppointmentDiary/diaryDefaults/spID="+$("#ServiceProviderID").val()+"/sPage=3";    
                                                                             }
                                                                             else
                                                                             {
                                                                                document.location.href = "{$_subdomain}/AppointmentDiary/diaryDefaults/spID="+$("#ServiceProviderID").val()+"/sPage=4";
                                                                             }
                                                                           
                                                                           {else}
                                                                           
                                                                            
                                                                             document.location.href = "{$_subdomain}/AppointmentDiary/diaryDefaults/spID="+$("#ServiceProviderID").val()+"/tInfo=1";
                                                                            
                                                                           {/if} 


                                                                           //document.location.href = "{$_subdomain}/AppointmentDiary/diaryDefaults/spID="+$("#ServiceProviderID").val()+"/tInfo=1";


                                                                        }
                                                            }); //Post ends here...

                                                
                                                
                                            }
                                                    
                            
                            
                            });
    
    });
    
    
    $(document).on('click', '#retryButton', 
                            function() {
    
    
          document.location.href = "{$_subdomain}/AppointmentDiary/diaryDefaults/spID="+$("#ServiceProviderID").val()+"/sPage=7";  
          return false;
    
     });
    
    $(document).on('click', '#nextButton', 
                            function() {
                            
                            
                            {if $sPage eq 3}
                                
                                  document.location.href = "{$_subdomain}/AppointmentDiary/diaryDefaults/spID="+$("#ServiceProviderID").val()+"/sPage=4";  
                                  
                            {elseif $sPage eq 8}      
                            
                                {if $sk_sb_test and $sk_va_test}
                                    
                                    document.location.href = "{$_subdomain}/AppointmentDiary/diaryDefaults/spID="+$("#ServiceProviderID").val()+"/sPage=1";  
                                {else}
                                    
                                    alert("{$page['Errors']['communication_test']|escape:'html'}");
                                    
                                {/if}
                            
                            {elseif $sPage eq 7}
                                
                                
                               document.location.href = "{$_subdomain}/Login/logout";  
                            
                            {else}    
                                
                                 $("#navigatePage").val("1");
                                 $('#saveRecord{$sPage}').trigger('click');
                            
                            {/if} 
                            
                            return false;
                            
                            });
    
    $(document).on('click', '#previousButton', 
                            function() {
                            
                            {if $sPage eq 3}
                                
                                document.location.href = "{$_subdomain}/AppointmentDiary/diaryDefaults/spID="+$("#ServiceProviderID").val()+"/sPage=2";    
                                  
                            {elseif $sPage eq 8}
                                
                                document.location.href = "{$_subdomain}/AppointmentDiary/diaryDefaults/spID="+$("#ServiceProviderID").val()+"/sPage=5";
                            
                            {elseif $sPage eq 7}
                                
                                alert("This feature has not been ready yet.");   
                                
                            {else} 
                            
                             $("#navigatePage").val("-1");
                             $('#saveRecord{$sPage}').trigger('click');
                             
                            {/if}  
                            
                            return false;
                            });
    
    
    $(document).on('click', '#saveRecord5', 
                            function() {
                            
                                $("#GeneralDefaults").val('GeneralDefaults5');
                                
                                
                                 
                                if($("label.fieldError:visible").length>0)
                                {
                                    
                                }
                                else
                                {
                                    $("#processDisplayText5").show();
                                    $("#saveRecord5").hide();
                                    $("#cancelChanges5").hide();
                                }
                            
                                 $('#diaryDefaultsForm').validate({
                                         
                                         
                                            ignore: '',
                                        
                                            rules:  {
                                            
                                                        IPAddress:
                                                        {
                                                            required: true
                                                            
                                                        },
                                                        Port:
                                                        {
                                                            required: true,
                                                            digits: true
                                                        },
                                                        
                                                        NumberOfVehicles:
                                                        {
                                                            //required: true,
                                                            digits: true
                                                        },
                                                        NumberOfWaypoints:
                                                        {
                                                            //required: true,
                                                            digits: true
                                                        }
                                                   },
                                            messages: {
                                            
                                                        IPAddress:
                                                        {
                                                            required: "{$page['Errors']['ip_address']|escape:'html'}"
                                                        },
                                                        Port:
                                                        {
                                                            required: "{$page['Errors']['port']|escape:'html'}"
                                                            
                                                        },
                                                        
                                                        NumberOfVehicles:
                                                        {
                                                           // required: "{$page['Errors']['number_of_vehicles']|escape:'html'}",
                                                            digit: "{$page['Errors']['digits']|escape:'html'}"
                                                        },
                                                        NumberOfWaypoints:
                                                        {
                                                            digit: "{$page['Errors']['digits']|escape:'html'}"
                                                        }
                                                      
                                                    },
                                           
                                           errorPlacement: function(error, element) {
                                                
        
                                                error.insertAfter( element );
                                              
                                                
                                                $("#processDisplayText5").hide();
                                                $("#saveRecord5").show();
                                                $("#cancelChanges5").show();
                                                $("label.fieldError").css("margin-left", "295px");
                                                adjustPanelHeights();
                                                
                                            },
                                            errorClass: 'fieldError',
                                            onkeyup: false,
                                            onblur: false,
                                            errorElement: 'label',

                                            submitHandler: function() {
                                                
                                                
                                                  
                                                 //Checking for ViamenteKey for duplication...
                                                 
                                                 
                                                $.post("{$_subdomain}/Data/isViamenteKeyExists/",         
                                                { ViamenteKey: $('#ViamenteKey').val(), ViamenteKey2: $('#ViamenteKey2').val(), ServiceProviderID: $("#ServiceProviderID").val() },      
                                                function(data){

                                                    var $WarningMessage = '';
                                                     
                                                    var $vKeyExists;
                                                    var $vKey2Exists;
                                                 
                                                    vExistsArray  = eval("(" + data + ")");
                                                    
                                                    $vKeyExists  = vExistsArray[0];
                                                    $vKey2Exists = vExistsArray[1];
                                                    
                                                    
                                                    if($vKeyExists && $vKey2Exists)
                                                    {
                                                        $WarningMessage = 'WARNING: The Viamente Licence and Viamente Licence (White) Keys you have entered are already in use. Contact PCCS Skyline Team immediately to confirm correct Viamente Licence Key has been provided.';
                                                    }
                                                    else if($vKeyExists)
                                                    {
                                                        $WarningMessage = 'WARNING: The Viamente Licence Key you have entered is already in use. Contact PCCS Skyline Team immediately to confirm correct Viamente Licence Key has been provided.';
                                                    }
                                                    else if($vKey2Exists)
                                                    {
                                                        $WarningMessage = 'WARNING: The Viamente Licence (White) Key you have entered is already in use. Contact PCCS Skyline Team immediately to confirm correct Viamente Licence Key has been provided.';
                                                    }
                                                    else if($('#ViamenteKey').length > 0 && $('#ViamenteKey2').length > 0 && $('#ViamenteKey').val()!='' &&  $('#ViamenteKey').val()==$('#ViamenteKey2').val())
                                                    {
                                                            $WarningMessage = 'WARNING: The Viamente Licence and Viamente Licence (White) Keys you have entered are same. Contact PCCS Skyline Team immediately to confirm correct Viamente Licence Key has been provided.';
                                                    }




                                                   if(($WarningMessage!='' && confirm($WarningMessage)) || $WarningMessage=='')
                                                   {

                                                        var $submitDetails = true; 

                                                        $.post("{$_subdomain}/AppointmentDiary/ProcessData/ServiceProviderSkillsSet/",        

                                                                $("#diaryDefaultsForm").serialize(),      
                                                                function(data){
                                                                    // DATA NEXT SENT TO COLORBOX
                                                                    var p = eval("(" + data + ")");

                                                                        if(p['status']=="ERROR")
                                                                            {

                                                                                $("#errorCenterInfoText").html(p['message']).css('color','red').fadeIn('slow').delay("5000").fadeOut('slow');  

                                                                                $("#processDisplayText5").hide();
                                                                                $("#saveRecord5").show();
                                                                                $("#cancelChanges5").show();

                                                                            }
                                                                            else if(p['status']=="OK")
                                                                            {

                                                                               {if $DiaryWizardSetup}

                                                                                 if($("#navigatePage").val()=="1")
                                                                                 {
                                                                                     document.location.href = "{$_subdomain}/AppointmentDiary/diaryDefaults/spID="+$("#ServiceProviderID").val()+"/sPage=8";    
                                                                                 }
    //                                                                             else if($("#navigatePage").val()=="-1")
    //                                                                             {
    //                                                                                 document.location.href = "{$_subdomain}/AppointmentDiary/diaryDefaults/spID="+$("#ServiceProviderID").val()+"/sPage=4";    
    //                                                                             }
                                                                                 else
                                                                                 {
                                                                                    document.location.href = "{$_subdomain}/AppointmentDiary/diaryDefaults/spID="+$("#ServiceProviderID").val()+"/sPage=5";
                                                                                 }

                                                                               {else}


                                                                                 document.location.href = "{$_subdomain}/AppointmentDiary/diaryDefaults/spID="+$("#ServiceProviderID").val()+"/tInfo=1";

                                                                               {/if}    

                                                                            }
                                                                }); //Post ends here...


                                                    }
                                                    else
                                                    {
                                                        $("#processDisplayText5").hide();
                                                        $("#saveRecord5").show();
                                                        $("#cancelChanges5").show();
                                                        
                                                        document.location.href = "{$_subdomain}/AppointmentDiary/diaryDefaults/spID="+$("#ServiceProviderID").val()+"/";
                                                    }
                                                    
                                                    

                                                    
                                                });

                                              
                                              
                                            }
                                                    
                            
                            
                            });
    
    });
    
    
    
    
    
    $(document).on('click', '#saveRecord6', 
                            function() {
                            
                                $("#GeneralDefaults").val('GeneralDefaults6');
                                
                                
                                 
                                if($("label.fieldError:visible").length>0)
                                {
                                    
                                }
                                else
                                {
                                    $("#processDisplayText6").show();
                                    $("#saveRecord6").hide();
                                    $("#cancelChanges6").hide();
                                }
                                
                                
                              
                                
                            
                                 $('#diaryDefaultsForm').validate({
                                         
                                         
                                            ignore: '',
                                        
                                            rules:  {
                                            
                                                        AutoSelectDay:
                                                        {
                                                            required: function (element) {
                                                                if($('#AutoSelectDayChkBox').is(':checked') && !$('input[name="AutoSelectDay"]:checked').val())
                                                                {
                                                                    
                                                                    return true;
                                                                }
                                                                else
                                                                {
                                                                    
                                                                    return false;
                                                                }
                                                            }
                                                            
                                                        },
                                                        AutoDisplayTable:
                                                        {
                                                           required: function (element) {
                                                                if($('#AutoDisplayTableChkBox').is(':checked') && !$('input[name="AutoDisplayTable"]:checked').val())
                                                                {
                                                                    
                                                                    return true;
                                                                }
                                                                else
                                                                {
                                                                    
                                                                    return false;
                                                                }
                                                            }
                                                            
                                                        }
                                                   },
                                            messages: {
                                            
                                                        AutoSelectDay:
                                                        {
                                                            required: "{$page['Errors']['auto_select_day']|escape:'html'}"
                                                        },
                                                        AutoDisplayTable:
                                                        {
                                                            required: "{$page['Errors']['auto_display_table']|escape:'html'}"
                                                        }
                                                      
                                                    },
                                           
                                           errorPlacement: function(error, element) {
                                                
        
                                                if(element.attr("name")=="AutoSelectDay")
                                                {
                                                    $("#AutoSelectDayError").append( error );
                                                }
                                                else if(element.attr("name")=="AutoDisplayTable")
                                                {
                                                    $("#AutoDisplayTableError").append( error );
                                                }
                                                else
                                                {
                                                    error.insertAfter( element );
                                                }
                                              
                                                
                                                $("#processDisplayText6").hide();
                                                $("#saveRecord6").show();
                                                $("#cancelChanges6").show();
                                                $("label.fieldError").css("margin-left", "150px");
                                                adjustPanelHeights();
                                                
                                            },
                                            errorClass: 'fieldError',
                                            onkeyup: false,
                                            onblur: false,
                                            errorElement: 'label',

                                            submitHandler: function() {
                                                
                                                    var $submitDetails = true; 
                                               
                                                    $.post("{$_subdomain}/AppointmentDiary/ProcessData/ServiceProviderSkillsSet/",        

                                                            $("#diaryDefaultsForm").serialize(),      
                                                            function(data){
                                                                // DATA NEXT SENT TO COLORBOX
                                                                var p = eval("(" + data + ")");

                                                                    if(p['status']=="ERROR")
                                                                        {

                                                                            $("#errorCenterInfoText").html(p['message']).css('color','red').fadeIn('slow').delay("5000").fadeOut('slow');  

                                                                            $("#processDisplayText6").hide();
                                                                            $("#saveRecord6").show();
                                                                            $("#cancelChanges6").show();

                                                                        }
                                                                        else if(p['status']=="OK")
                                                                        {
                                                                        
                                                                             {if $DiaryWizardSetup}
                                                                           
                                                                             if($("#navigatePage").val()=="1")
                                                                             {
                                                                                 document.location.href = "{$_subdomain}/AppointmentDiary/skillsSetSP/spID="+$("#ServiceProviderID").val();    
                                                                             }
                                                                             else if($("#navigatePage").val()=="-1")
                                                                             {
                                                                                 document.location.href = "{$_subdomain}/AppointmentDiary/diaryDefaults/spID="+$("#ServiceProviderID").val()+"/sPage=4";    
                                                                             }
                                                                             else
                                                                             {
                                                                                document.location.href = "{$_subdomain}/AppointmentDiary/diaryDefaults/spID="+$("#ServiceProviderID").val()+"/sPage=6";
                                                                             }
                                                                           
                                                                           {else}
                                                                           
                                                                            
                                                                             document.location.href = "{$_subdomain}/AppointmentDiary/diaryDefaults/spID="+$("#ServiceProviderID").val()+"/tInfo=1";
                                                                            
                                                                           {/if} 

                                                                          // document.location.href = "{$_subdomain}/AppointmentDiary/diaryDefaults/spID="+$("#ServiceProviderID").val()+"/tInfo=1";


                                                                        }
                                                            }); //Post ends here...

                                                
                                                
                                            }
                                                    
                            
                            
                            });
    
    });
    
    
    
    
     $(document).on('click', '#saveRecord1', 
                            function() {
                            
                                 $("#GeneralDefaults").val('GeneralDefaults1');
                                 
                                 
                                if($("label.fieldError:visible").length>0)
                                {

                                }
                                else
                                {
                                    $("#saveRecord1").hide();
                                    $("#cancelChanges1").hide();
                                    $("#processDisplayText1").show();

                                }
                                 
                            
                                 $('#diaryDefaultsForm').validate({
                                         
                                         
                                            ignore: '',
                                        
                                            rules:  {
                                                        
                                                        AMTimeFrom:
                                                        {
                                                            required: true,
                                                            time: true
                                                        },
                                                        AMTimeTo:
                                                        {
                                                            required: true,
                                                            time: true
                                                        },
                                                        PMTimeFrom:
                                                        {
                                                            required: true,
                                                            time: true
                                                        },
                                                        PMTimeTo:
                                                        {
                                                            required: true,
                                                            time: true
                                                        }
                                                   },
                                            messages: {
                                            
                                                        AMTimeTo:
                                                        {
                                                            required: "{$page['Errors']['am_time_slot_2']|escape:'html'}",
                                                            time: "{$page['Errors']['am_valid_time_2']|escape:'html'}"
                                                        },
                                                        AMTimeFrom:
                                                        {
                                                            required: "{$page['Errors']['am_time_slot_1']|escape:'html'}",
                                                            time: "{$page['Errors']['am_valid_time_1']|escape:'html'}"
                                                        },
                                                        PMTimeTo:
                                                        {
                                                            required: "{$page['Errors']['pm_time_slot_2']|escape:'html'}",
                                                            time: "{$page['Errors']['pm_valid_time_2']|escape:'html'}"
                                                        },
                                                        PMTimeFrom:
                                                        {
                                                            required: "{$page['Errors']['pm_time_slot_1']|escape:'html'}",
                                                            time: "{$page['Errors']['pm_valid_time_1']|escape:'html'}"
                                                        }
                                                      
                                                    },
                                           
                                           errorPlacement: function(error, element) {
                                                
                                                
                                                if(element.attr("id")=="AMTimeFrom")
                                                {
                                                    $("#AMTimeFromError").append( error );
                                                }
                                                else if(element.attr("id")=="PMTimeFrom")
                                                {
                                                    $("#PMTimeFromError").append( error );
                                                }
                                                else
                                                {
                                                    error.insertAfter( element );
                                                }
                                                
                                                
                                               $("#saveRecord1").show();
                                               $("#cancelChanges1").show();
                                               $("#processDisplayText1").hide();
                                               
                                                
                                               adjustPanelHeights();
                                                
                                            },
                                            errorClass: 'fieldError',
                                            onkeyup: false,
                                            onblur: false,
                                            errorElement: 'label',

                                            submitHandler: function() {
                                                
                                                
                                                
                                                $.post("{$_subdomain}/AppointmentDiary/ProcessData/ServiceProviderSkillsSet/",        

                                                        $("#diaryDefaultsForm").serialize(),      
                                                        function(data){
                                                            // DATA NEXT SENT TO COLORBOX
                                                            var p = eval("(" + data + ")");

                                                                if(p['status']=="ERROR")
                                                                    {
                                                                        
                                                                        $("#errorCenterInfoText").html(p['message']).css('color','red').fadeIn('slow').delay("5000").fadeOut('slow');  
                                                                        
                                                                        $("#saveRecord1").show();
                                                                        $("#cancelChanges1").show();
                                                                        $("#processDisplayText1").hide();

                                                                    }
                                                                    else if(p['status']=="OK")
                                                                    {
                                                                          {if $DiaryWizardSetup}
                                                                           
                                                                            if($("#navigatePage").val()=="1")
                                                                            {
                                                                                document.location.href = "{$_subdomain}/AppointmentDiary/diaryDefaults/spID="+$("#ServiceProviderID").val()+"/sPage=2";    
                                                                            }
                                                                            else if($("#navigatePage").val()=="-1")
                                                                            {
                                                                                document.location.href = "{$_subdomain}/AppointmentDiary/diaryDefaults/spID="+$("#ServiceProviderID").val()+"/sPage=8";    
                                                                            }
                                                                            else
                                                                            {
                                                                               document.location.href = "{$_subdomain}/AppointmentDiary/diaryDefaults/spID="+$("#ServiceProviderID").val()+"/sPage=1";
                                                                            }

                                                                          {else}

                                                                        if($('#retainpage').val()=="yes"){
                                                                        document.location.href = "{$_subdomain}/AppointmentDiary/diaryDefaults/spID="+$("#ServiceProviderID").val()+"/sPage=1";
                                                                        }else{
                                                                            document.location.href = "{$_subdomain}/AppointmentDiary/diaryDefaults/spID="+$("#ServiceProviderID").val()+"/tInfo=1";
                                                                            }
                                                                          {/if} 
                                                                     
                                                                       //document.location.href = "{$_subdomain}/AppointmentDiary/diaryDefaults/spID="+$("#ServiceProviderID").val()+"/tInfo=1";
                                                                       
                                                                       
                                                                    }
                                                        }); //Post ends here...
                                                
                                                
                                                
                                            }
                                                    
                            
                            
                            });
    
    });
    
    
    $(document).on('click', '#step2', 
                            function() {
    
    
    
                if ($(this).is(':checked')) 
                {
                   $("#step2Text").show();
                   $("#step2TextHeading").show();
                   adjustPanelHeights();
                  // $(".proceed").show();
                } 
                else 
                {
                    $("#step2Text").hide();
                    $("#step2TextHeading").hide();
                    adjustPanelHeights();
                   // $(".proceed").hide();
                }              
    
   });
    
    $("#step2Text").hide();
    $("#step2TextHeading").hide();
    //$(".proceed").hide();
        
    /* =======================================================
    *
    * set tab on return for input elements with form submit on auto-submit class...
    *
    * ======================================================= */

    $('input[type=text],input[type=password]').keypress( function( e ) {
            if (e.which == 13) {
                $(this).blur();
                if ($(this).hasClass('auto-submit')) {
                    $('.auto-hint').each(function() {
                        $this = $(this);
                        if ($this.val() == $this.attr('title')) {
                            $this.val('').removeClass('auto-hint').addClass('auto-hint-hide');
                            if ($this.hasClass('auto-pwd')) {
                                $this.prop('type','password');
                            }
                        }
                    } );
                    $(this).get(0).form.onsubmit();
                } else {
                    $next = $(this).attr('tabIndex') + 1;
                    $('[tabIndex="'+$next+'"]').focus();
                }
                return false;
            }
        } );        

    
    
                    
        $('#TradeAccountResults').PCCSDataTable( {

            "aoColumns": [ 
                /* StatusID   */ { "bVisible": false } ,    
                /* Trade Account	    */  null,
                /* Postcode	    */  null,
                /* Mon    */  { "bSortable": false },
                /* Tue    */  { "bSortable": false },
                /* Wed    */  { "bSortable": false },
                /* Thu    */  { "bSortable": false },
                /* Fri    */  { "bSortable": false },
                /* Sat    */  { "bSortable": false },
                /* Sun    */  { "bSortable": false }
            
            ],

                "aaSorting": [[ 1, "asc" ]],




                displayButtons:  "UA",
                addButtonId:     'addButtonId',
                addButtonText:   '{$page['Buttons']['insert']|escape:'html'}',
                createFormTitle: '{$page['Text']['insert_page_legend']|escape:'html'}',
                createAppUrl:    '{$_subdomain}/AppointmentDiary/spTradeAccount/insert/'+$("#ServiceProviderID").val(),
                createDataUrl:   '{$_subdomain}/AppointmentDiary/ProcessData/SPTradeAccount/',
                formInsertButton:'ta_insert_save_btn',

                frmErrorRules:   {
                                        TradeAccount:
                                        {
                                            required: true
                                        },
                                        Postcode:
                                        {
                                            required: true
                                        },  
                                        Status:
                                        {
                                            required: true
                                        }     
                                            

                                 },

               frmErrorMessages: {

                                        TradeAccount:
                                        {
                                            required: "{$page['Errors']['trade_account_error']|escape:'html'}"
                                        },
                                        Postcode:
                                        {
                                            required: "{$page['Errors']['postcode_error']|escape:'html'}"
                                        },
                                        Status:
                                        {
                                                required: "{$page['Errors']['status_error']|escape:'html'}"
                                        }   
                                  },                     

                popUpFormWidth:  715,
                popUpFormHeight: 330,


                updateButtonId:  'updateButtonId',
                updateButtonText:'{$page['Buttons']['edit']|escape:'html'}',
                updateFormTitle: '{$page['Text']['update_page_legend']|escape:'html'}',
                updateAppUrl:    '{$_subdomain}/AppointmentDiary/spTradeAccount/update/'+$("#ServiceProviderID").val()+"/",
                updateDataUrl:   '{$_subdomain}/AppointmentDiary/ProcessData/SPTradeAccount/',
                formUpdateButton:'ta_update_save_btn',

                colorboxFormId:  "TradeAccountForm",
                frmErrorMsgClass:"fieldError",
                frmErrorElement: "label",
                htmlTablePageId: 'TradeAccountResultsPanel',
                htmlTableId:     'TradeAccountResults',
                fetchDataUrl:    '{$_subdomain}/AppointmentDiary/ProcessData/SPTradeAccount/fetch/'+$("#ServiceProviderID").val(),
                formCancelButton:'ta_cancel_btn',
                sDom: 'ft<"#dataTables_command">rpi',
                dblclickCallbackMethod: 'gotoEditPage',
                fnRowCallback:          'inactiveRow',
                searchCloseImage:'{$_subdomain}/css/Skins/{$_theme}/images/close.png',
                tooltipTitle:    "{$page['Text']['tooltip_title']|escape:'html'}",
                iDisplayLength:  6,
                formDataErrorMsgId: "taSuggestText",
                frmErrorSugMsgClass:"formCommonError"


            });
    
    
    
        $("#defaultDiv").hide();
        $("#formDiv").hide();
        $("#Default_ID1_Div").hide();
        $("#Default_ID2_Div").hide();
        $("#Default_ID3_Div").hide();
        $("#Default_ID4_Div").hide();    
        $("#Default_ID5_Div").hide();
        $("#Default_ID6_Div").hide();
        $("#Default_ID7_Div").hide();
        $("#Default_ID8_Div").hide();
        $("#Default_ID9_Div").hide();
        
    
        {if $sPage}
          $("#Default_ID{$sPage}_Div").show();
         
        {else}
        $("#defaultDiv").show();
        {/if}
            
         adjustPanelHeights();   
         
         
          $('#AMColour').jPicker( {
                window:
                {
                  expandable: true,
                              position:
                              {
                                x: 'screenCenter', // acceptable values "left", "center", "right", "screenCenter", or relative px value
                                y: '150px' // acceptable values "top", "bottom", "center", or relative px value
                              }
                }
              }

          );
              
          
         $('#PMColour').jPicker( {
                window:
                {
                  expandable: true,
                              position:
                              {
                                x: 'screenCenter', // acceptable values "left", "center", "right", "screenCenter", or relative px value
                                y: '150px' // acceptable values "top", "bottom", "center", or relative px value
                              }
                }
              }

          ); 
         
         
         
          $(".Color").css({ width: "25px", height: "24px", padding: "0px" });
        
          displayTableFields();  
          
          
          
         {if $DiaryWizardSetup}
         
            $('#menu').after('<div class="customOverlay" style="display: block; opacity: 0.25; top:0; cursor: default; width:955px;height:200px;"></div><div class="customOverlay" style="display: block; opacity: 0.25; top:200px; cursor: default; width:300px;height:500px;"></div>');
        
         {/if}
          
    });
    
    
    function gotoEditPage($sRow)
    {
        
         
        $('#updateButtonId').removeAttr('disabled').removeClass('gplus-blue-disabled').addClass('gplus-blue');
        $('#updateButtonId').trigger('click');
       
       
    }
    
    
    function inactiveRow(nRow, aData)
    {
       
       
        if (aData[10]=='In-active')
        {  
           

            $('td:eq(0)', nRow).html(aData[1]+'<img src="{$_subdomain}/css/Skins/{$_theme}/images/red_cross.png" style="float:right;"  width="20" height="20" >' );
        }
        else
        {
            
            $('td:eq(0)', nRow).html( aData[1] + '<img src="{$_subdomain}/css/Skins/{$_theme}/images/green_tick.png" style="float:right;" width="20" height="20" >' );
        }
    }
    
  function saveGridDef(f){
 $.post("{$_subdomain}/Diary/saveGridDefaults",
{ spid:$("#ServiceProviderID").val(),
    lat1:$('#glat1').val(),
    lng1:$('#glng1').val(),
    lat2:$('#glat2').val(),
    lng2:$('#glng2').val(),
    csize:$('#csize').val()
},
function(data) {
alert("Grid Defaults updated, if grid size or start,end locations changed all allocations need to be resetupt.");

});
  }
  
  function mapPrewiev(){
$.post("{$_subdomain}/Diary/saveGridDefaults",
{ spid:$("#ServiceProviderID").val(),
    lat1:$('#glat1').val(),
    lng1:$('#glng1').val(),
    lat2:$('#glat2').val(),
    lng2:$('#glng2').val(),
    csize:$('#csize').val()
},
function(data) {
  window.location="{$_subdomain}/Diary/loadGridMap/prew=true/sp="+$("#ServiceProviderID").val();
  });
  }
  
  function checkViamenteRunType(){
  if($('#AutoSpecifyEngineerByPostcode').attr('checked')=='checked'){
  $('#ViamenteRunTypeP').show();
  }else{
  $('#ViamenteRunTypeP').hide();
  $('#ViamenteRunType1').attr('checked','checked');
  
  }
  }
</script>


    
{/block}

{block name=body}

    
<div class="breadcrumb">
    
    
    
    <div>
        <a href="{$_subdomain}/index/index" >{$page['Text']['home_page']|escape:'html'}</a> / 
        <a href="{$_subdomain}/index/siteMap" >{$page['Text']['site_map']|escape:'html'}</a>  / 
        <a href="{$_subdomain}/AppointmentDiary/" >{$page['Text']['page_title']|escape:'html'}</a> / 
        {$page['Text']['defaults']|escape:'html'}
    </div>
</div>
 
{include file='include/site_map_menu.tpl'}
        
<div class="main" id="appointmentDiary" >   
   
   <div class="siteMapPanel" >
         <form id="diaryDefaultsForm" name="diaryDefaultsForm" method="post"  action="#" class="inline">
       
             
            
            
             
                        <div id="firstDiv" class="firstDiv borderDiv">
                           
                           <div id="innerFirstDiv" >
                            
                            {if $SuperAdmin}
                            
                             <select  name="ServiceProviderID" id="ServiceProviderID"  class="text auto-hint" >
                                {foreach from=$serviceProvidersList item=sp}
                                 <option value="{$sp.ServiceProviderID}" {if $ServiceProviderID eq $sp.ServiceProviderID}selected="selected"{/if} >{$sp.CompanyName|escape:'html'}</option>
                                 {/foreach}

                             </select>  
                                 
                            {else}
                            
                                <input type="hidden" name="ServiceProviderID" id="ServiceProviderID" value="{$ServiceProviderID|escape:'html'}" >
                                
                             {/if}
                                 
                            <table id="SPDefaultsResults" border="0" cellpadding="0" cellspacing="0" class="browse"  >
                                <thead>
                                        <tr>
                                                
                                                <th title="{$page['Text']['diary_defaults']|escape:'html'}"  >{$page['Text']['diary_defaults']|escape:'html'}</th>
                                                
                                                
                                        </tr>
                                </thead>
                                
                                
                                
                                <tbody>
                                    {if $def neq 0}
                                    <tr class="odd"  >
                                       
                                        <td  title="{$page['Text']['click_appointment_time_default']|escape:'html'}" >
                                            <input type="radio" class="DefaultsRadioButton" id="Default_ID1" name="Default_ID1" value="1"  {if $sPage eq 1} checked="checked" {/if}  >
                                            {$page['Labels']['appointment_time_default']|escape:'html'}
                                        </td>
                                        
                                    </tr>
                                    
                                    
                                    <tr class="even" {if $def==2}style="display:none"{/if}>
                                        
                                        <td  title="{$page['Text']['click_route_optimisation_defaults']|escape:'html'}" >
                                             <input type="radio" class="DefaultsRadioButton"  id="Default_ID2" name="Default_ID2" value="2"  {if $sPage eq 2} checked="checked" {/if}  >
                                            {$page['Labels']['route_optimisation_defaults']|escape:'html'}
                                        </td>
                                        
                                    </tr>
                                  
                                    <tr class="odd" >
                                        
                                        <td title="{$page['Text']['click_trade_account_defaults']|escape:'html'}" >
                                            <input type="radio" class="DefaultsRadioButton" id="Default_ID3" name="Default_ID3" value="3"  {if $sPage eq 3} checked="checked" {/if}  >
                                            {$page['Labels']['trade_account_defaults']|escape:'html'}
                                        </td>
                                        
                                    </tr>
                                    
                                    <tr class="even" >
                                        
                                        <td title="{$page['Text']['click_route_administrator_defaults']|escape:'html'}" >
                                             <input type="radio" class="DefaultsRadioButton" id="Default_ID4" name="Default_ID4" value="4"  {if $sPage eq 4} checked="checked" {/if}  >
                                            {$page['Labels']['administrator_defaults']|escape:'html'}
                                        </td>
                                        
                                    </tr>
                                    <tr class="odd" >
                                        
                                        <td title="{$page['Text']['click_configuration_defaults']|escape:'html'}" >
                                            <input type="radio" class="DefaultsRadioButton" id="Default_ID5" name="Default_ID5" value="5"  {if $sPage eq 5} checked="checked" {/if}  >
                                            {$page['Labels']['configuration_defaults']|escape:'html'}
                                        </td>
                                        
                                    </tr>
                                    <tr class="even" >
                                        
                                        <td title="{$page['Text']['click_insert_appointment_defaults']|escape:'html'}" >
                                            <input type="radio" class="DefaultsRadioButton" id="Default_ID6" name="Default_ID6" value="6"  {if $sPage eq 6} checked="checked" {/if}  >
                                            {$page['Labels']['insert_appointment_defaults']|escape:'html'}
                                        </td>
                                        
                                    </tr>
                                     <tr class="odd" {if $dat!="GridMapping"}style="display:none"{/if}>
                                        
                                        <td title="{$page['Labels']['grid_mapping_defaults']|escape:'html'}" >
                                            <input type="radio" class="DefaultsRadioButton" id="Default_ID9" name="Default_ID9" value="9"  {if $sPage eq 9} checked="checked" {/if}  >
                                            {$page['Labels']['grid_mapping_defaults']|escape:'html'}
                                        </td>
                                        
                                    </tr>
                                     <tr class="odd" >
                                        
                                        <td title="{$page['Labels']['bank_holiday_defaults']|escape:'html'}" >
                                            <input type="radio" class="DefaultsRadioButton" id="Default_ID10" name="Default_ID10" value="10"  {if $sPage eq 10} checked="checked" {/if}  >
                                            {$page['Labels']['bank_holiday_defaults']|escape:'html'}
                                        </td>
                                        
                                    </tr>
                                    <tr class="even" {if $def|default:''==2}style="display:none"{/if}>
                                        
                                        <td title="{$page['Labels']['finalise_day_defaults']|escape:'html'}" >
                                            <input type="radio" class="DefaultsRadioButton" id="Default_ID11" name="Default_ID11" value="11"  {if $sPage eq 11} checked="checked" {/if}  >
                                            {$page['Labels']['finalise_day_defaults']|escape:'html'}
                                        </td>
                                        
                                    </tr>
                                    {else if $def eq 0}
                                        <tr class="odd" >
                                        
                                        <td title="{$page['Labels']['bank_holiday_defaults']|escape:'html'}" >
                                            <input type="radio" class="DefaultsRadioButton" id="Default_ID10" name="Default_ID10" value="10"  {if $sPage eq 10} checked="checked" {/if}  >
                                            {$page['Labels']['bank_holiday_defaults']|escape:'html'}
                                        </td>
                                        
                                    </tr>
                                    {/if}
                                    {* Ignore div 7 and 8, add div 9 if needed *}
                                    
                                </tbody>
                                
                            </table> 
                                                
                                                
                           </div>
                          
                           
                        </div>
                        
                        <div id="secondDiv" class="secondDiv borderDiv" > 
                            
                            <div style="width:100%;" id="defaultDiv" > 
                                    
                                        {if $def==1}
                                    <p style="text-align:center;" >
                                        
                                         <img src="{$_subdomain}/css/Skins/{$_theme}/images/viamente-skyline.png" width="560" height="278" style="margin:0px; padding:0px;" >
                                    </p>
                                    
                                    <p style="margin-left:0px;text-align:center;padding:0px 100px 0px 100px;font-size:14px; line-height:21px; font-weight:normal; font-family:'Lucida Sans Unicode', 'Lucida Grande', sans-serif; color:#737373;" >
                                        <span style="color:#336699;padding-left:20px;" >{$page['Text']['default_text1']|escape:'html'} </span>{$page['Text']['default_text2']|escape:'html'}

                                    </p>
        {/if}
         
        {if $def==2}
                                    <p style="text-align:center;" >
                                       
                                         <img src="{$_subdomain}/images/diary/skyline_diary.png" width="558px"  style="margin:0px; padding:0px;" >
                                    </p>
                                    
                                    <p >
                                        <img src="{$_subdomain}/images/diary/skyline_diary_circles.png" width="568px"   style="margin:0px; padding:0px;" >
                                         
                                    </p>
        {/if}
        


                                    <div id="centerInfoText" style="diplay:none;padding-top:20px;" class="centerInfoText" ></div>
                            
                            </div>
                                
                           
                           <div style="width:100%;display:none;" id="Default_ID1_Div" > 
                               
                               
                               <h3 class="SiteMapPageHeader" >{$page['Labels']['appointment_time_default']|escape:'html'} </h3>
                               
                               
                                    
                               <p> {$page['Labels']['appointment_time_defaults']|escape:'html'}</p>
                                      
                                    <p>&nbsp;</p>
                                    
                                    <p>
                                        <label style="width: 185px;" >
                                            
                                         {$page['Labels']['earliest']|escape:'html'}
                                         
                                        </label>
                                        
                                        <label style="width: 85px;" >  
                                         {$page['Labels']['latest']|escape:'html'}
                                        </label>
                                        
                                        <label style="width: 115px;" >  
                                         {$page['Labels']['cell_colour']|escape:'html'}
                                        </label>
                                    </p>
                                   
                                    <p>   
                                            <label style="width: 130px;" > AM <sup>*</sup>&nbsp;&nbsp;</label>
                                            <input type="text" name="AMTimeFrom" id="AMTimeFrom" value="{$AMTimeFrom|escape:'html'}" style="width:80px;"   >&nbsp;&nbsp;
                                            <input type="text" name="AMTimeTo" id="AMTimeTo" value="{$AMTimeTo|escape:'html'}" style="width:80px;"  >
                                            <input type="text" name="AMColour" id="AMColour" value="{$AMColour|escape:'html'}" style="width:60px;" maxlength="10"  >
                                            
                                            <img src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="TimeDefaultsAmHelp" class="helpTextIconQtip"  title="{$page['Text']['help_info']|escape:'html'}" alt="{$page['Text']['help_info']|escape:'html'}" width="15" height="15" >
                                    </p>   
                                    
                                    <p id="AMTimeFromError" style="padding:0px;margin:0px;" ></p>
                                    
                                    
                                    
                                    
                                    
                                   <p>
                                        
                                            <label style="width: 130px;"  >PM <sup>*</sup>&nbsp;&nbsp;</label>
                                            <input type="text" name="PMTimeFrom" value="{$PMTimeFrom|escape:'html'}" id="PMTimeFrom" value="" style="width:80px;"  >&nbsp;&nbsp;
                                            <input type="text" name="PMTimeTo"  value="{$PMTimeTo|escape:'html'}" id="PMTimeTo" value="" style="width:80px;"  >
                                            <input type="text" name="PMColour" id="PMColour" value="{$PMColour|escape:'html'}" style="width:60px;" maxlength="10"  > 
                                   </p>  
                                   
                                    <p id="PMTimeFromError" style="padding:0px;margin:0px;" ></p>
                                    
                                    
                                   <p  {if $def==2}style="display:none"{else}style="text-align:center;"{/if} > 
                                        <span  {if !$SuperAdmin} style="display:none"{/if}>{$page['Labels']['setup_unique_postcodes']|escape:'html'}</span>
                                          
                                    <input {if !$SuperAdmin} style="display:none"{/if} type="checkbox" name="SetupUniqueTimeSlotPostcodes" {if $SetupUniqueTimeSlotPostcodes eq 'Yes' } checked="checked" {/if} id="SetupUniqueTimeSlotPostcodes" value="Yes"   >
                                    
                                    
                                   </p>  
                                    <!-- 
                                    //andris-->
                                    {if $def!=2}
                                    <hr>
                                  {/if}


                                    <p{if $def==2} style="display:none"{/if}>
                                    Time Slider Start and Finish Times (hours)
                                    </p>
                                   <p {if $def==2} style="display:none"{/if}>
                                       <label style="width: 130px;"  >&nbsp;&nbsp;</label>
                                   <input type="text" name="engDefStart" value="{$engDefStart|date_format:'%H'}" id="engDefStart" value="" style="width:80px;"  >&nbsp;&nbsp;
                                            <input type="text" name="engDefEnd"  value="{$engDefEnd|date_format:'%H'}" id="engDefEnd" value="" style="width:80px;"  >
                                   <img src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="EngineerDefaultTimeHelp" class="helpTextIconQtip"  title="{$page['Text']['help_info']|escape:'html'}" alt="{$page['Text']['help_info']|escape:'html'}" width="15" height="15" >
                                   </p>
                                   <hr>
                                  
                                   <p >
                                       Deferred Time Slot
                                         <input type="hidden" name="df_count" id="df_count" value="{$timeDefRow|@count}">
                                        <img src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="DeferredTimeSlotHelp" class="helpTextIconQtip"  title="{$page['Text']['help_info']|escape:'html'}" alt="{$page['Text']['help_info']|escape:'html'}" width="15" height="15" >
                                  {$defCount=1}
                                   {for $e=0 to $timeDefRow|@count-1}
                                   <p>
                                       <br><br>
                                    &nbsp;&nbsp;Monday to Friday
                                   <p style="margin-left:14px;">
                                       
                                            
                                       <div style="width:100px;float:left;margin-left:14px;">
                                           {$page['Labels']['earliest']|escape:'html'}
                                           </div>
                                         
                                 
                                        
                                       <div style="width:90px;float:left;">
                                         {$page['Labels']['latest']|escape:'html'}
                                       </div>
                                        <div style="width:200px;float:left;">
                                         Postcode
                                         </div>
                                    </p>
                                    <p>
                                     <input type="text" name="EarliestWorkday_{$e}" value="{if isset($timeDefRow[$e].EarliestWorkday)}{$timeDefRow[$e].EarliestWorkday|date_format:'%H:%M'}{/if}" id="EarliestWorkday_{$e}"  style="width:80px;"  >&nbsp;&nbsp;
                                            <input type="text" name="LatestWorkday_{$e}"  value="{if isset($timeDefRow[$e].LatestWorkday)}{$timeDefRow[$e].LatestWorkday|date_format:'%H:%M'}{/if}" id="LatestWorkday_{$e}" style="width:80px;"  >
                                            <input type="text" name="DeffPostcodeWork_{$e}" id="DeffPostcode" value="{if isset($timeDefPostcodeRow[$timeDefRow[$e].DeferredTimeSlotID])}{foreach $timeDefPostcodeRow[$timeDefRow[$e].DeferredTimeSlotID	] as $dd}{if $dd.DpType=="Working"}{$dd.Postcode},{/if}{/foreach}{/if}" style="width:340px;"   > 
                                    </p>
                                        <br>&nbsp;&nbsp;Saturday & Sunday
                                   <p style="margin-left:14px;">
                                       
                                            
                                       <div style="width:100px;float:left;margin-left:14px;">
                                           {$page['Labels']['earliest']|escape:'html'}
                                           </div>
                                         
                                 
                                        
                                       <div style="width:90px;float:left;">
                                         {$page['Labels']['latest']|escape:'html'}
                                       </div>
                                        <div style="width:200px;float:left;">
                                         Postcode
                                         </div>
                                    </p>
                                    <p>
                                      
                                       
                                     <input type="text" name="EarliestWeekend_{$e}" value="{if isset($timeDefRow[$e].EarliestWeekend)}{$timeDefRow[$e].EarliestWeekend|date_format:'%H:%M'}{/if}" id="EarliestWeekend_{$e}"  style="width:80px;"  >&nbsp;&nbsp;
                                            <input type="text" name="LatestWeekend_{$e}"  value="{if isset($timeDefRow[$e].LatestWeekend)}{$timeDefRow[$e].LatestWeekend|date_format:'%H:%M'}{/if}" id="LatestWeekend_{$e}" style="width:80px;"  >
                                            <input type="text" name="DeffPostcodeWeekEnd_{$e}" id="DeffPostcode" value="{if isset($timeDefPostcodeRow[$timeDefRow[$e].DeferredTimeSlotID])}{foreach $timeDefPostcodeRow[$timeDefRow[$e].DeferredTimeSlotID	] as $dd}{if $dd.DpType=="WeekEnd"}{$dd.Postcode},{/if}{/foreach}{/if}" style="width:340px;"   > 
                                    </p>
                                   
                                    {/for}
                                     
                                   </p>
                                    <p id="deferredTimeSlotP">
                                   
                                   </p>
                                     <p>
                                     <div style="float:right" onclick="appendNewDefTime()"><img  src="{$_subdomain}/images/add_icon.png"></div>
                                    </p>
                                    <input type="hidden" id="retainpage" value="">
                                    <script>
                                    function appendNewDefTime_old(){
                                    
                                    $('#deferredTimeSlotP').append('<p>&nbsp;&nbsp;Monday to Friday<p style="margin-left:14px;"><div style="width:100px;float:left;margin-left:14px;">{$page['Labels']['earliest']|escape:'html'}</div><div>{$page['Labels']['latest']|escape:'html'}</div> <div style="width:200px;float:left;">Postcode</div></p><p><input type="text" name="EarliestWorkday_'+$("#df_count").val()+1+'" value="" id="DeffTimeFrom"  style="width:80px;"  >&nbsp;&nbsp;<input type="text" name="LatestWorkday_'+$("#df_count").val()+1+'"  value="" id="DeffTimeTo" style="width:80px;"  ><input type="text" name="DeffPostcodeWork_'+$("#df_count").val()+1+'" id="DeffPostcode" value="" style="width:340px;" maxlength="10"  ></p></p><br>&nbsp;&nbsp;Saturday & Sunday<p style="margin-left:14px;"><div style="width:100px;float:left;margin-left:14px;">{$page['Labels']['earliest']|escape:'html'}</div><div style="width:90px;float:left;">{$page['Labels']['latest']|escape:'html'}</div><div style="width:200px;float:left;">Postcode</div></p><p><input type="text" name="EarliestWeekend_'+$("#df_count").val()+1+'" value="" id="DeffTimeFrom"  style="width:80px;"  >&nbsp;&nbsp;<input type="text" name="DeffPostcodeWeekEnd_'+$("#df_count").val()+1+'" id="DeffPostcode" value="" style="width:340px;" maxlength="10"  ></p>');
                                    $('#df_count').val($('#df_count').val()+1);
                                    }
                                    
                                    function appendNewDefTime()
                                    {
                                        $('#retainpage').val("yes");
                                         $('#saveRecord1').trigger('click');
                                      $.post("{$_subdomain}/AppointmentDiary/addDeferredTime/spid={$ServiceProviderID}",false,
                                    function(data) {
                                    
                                    window.location="{$_subdomain}/AppointmentDiary/diaryDefaults/spID={$ServiceProviderID}/sPage=1";
                                        });

                                    }
                                    </script>
                                    <!--                                   andris-->
                                   <p>&nbsp;</p>        
                                   <p style="text-align:center" > 
                                       
                                       
                                       <input type="hidden" name="OriginalSetupUniqueTimeSlotPostcodes"  id="OriginalSetupUniqueTimeSlotPostcodes" value="{$SetupUniqueTimeSlotPostcodes|escape:'html'}"   >
                                       
                                       
                                       <input type="submit" name="saveRecord1" id="saveRecord1"    class="form-button"   value="{$page['Buttons']['save_record']|escape:'html'}" > 

                                       &nbsp;&nbsp;
                                       <input type="submit" name="cancelChanges1" id="cancelChanges1"   class="form-button cancelChanges"   value="{$page['Buttons']['cancel_changes']|escape:'html'}" > 

                                       <span id="processDisplayText1" style="color:red;display:none;" ><img src="{$_subdomain}/css/Skins/{$_theme}/images/loader.gif" width="16" height="16" >&nbsp;&nbsp;{$page['Buttons']['process_record']|escape:'html'}</span>            
                                  </p> 
                                    
                               
                               {if $DiaryWizardSetup}
                                      <p style="text-align:right;position:relative;bottom:-20px;" >
                                        
                                        <input type="submit" name="previousButton" id="previousButton" style="width:80px;"    class="form-button"   value="&laquo; {$page['Buttons']['previous']|escape:'html'}" > 
                                        
                                          
                                        &nbsp;&nbsp;
                                        <input type="submit" name="nextButton" id="nextButton"  style="width:80px;"  class="form-button"   value="{$page['Buttons']['next']|escape:'html'} &raquo;" > 
                                        
                                      </p>     
                              {/if} 
                               
                           </div>                
                                        
                                        
                           <div style="width:100%;display:none;" id="Default_ID2_Div" > 
                               
                                  
                                    <h3 class="SiteMapPageHeader" >{$page['Labels']['route_optimisation_defaults']|escape:'html'} </h3>
                                    <p>&nbsp;</p>
                                    <p>
                                        
                                        <label>{$page['Labels']['default_travel_time']|escape:'html'}: <sup>*</sup></label>
                                        &nbsp;&nbsp;
                                            <input type="text" name="DefaultTravelTime" id="DefaultTravelTime" value="{$DefaultTravelTime|escape:'html'}" style="width:80px;" > {$page['Labels']['mins']|escape:'html'} 
                                            <img src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="DefaultTravelTimeHelp" class="helpTextIconQtip"  title="{$page['Text']['help_info']|escape:'html'}" alt="{$page['Text']['help_info']|escape:'html'}" width="15" height="15" >
                                        
                                    </p>
                                    
                                   
                                    
                                    <p>
                                        
                                       <label>{$page['Labels']['default_travel_speed']|escape:'html'}: <sup>*</sup></label>
                                       &nbsp;&nbsp;
                                        
                                       <input type="text" name="DefaultTravelSpeed" id="DefaultTravelSpeed" value="{$DefaultTravelSpeed|escape:'html'}" style="width:80px;" > %
                                            
                                      
                                       
                                       <img src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="TravelSpeedInfoHelp" class="helpTextIconQtip"  title="{$page['Text']['help_info']|escape:'html'}" alt="{$page['Text']['help_info']|escape:'html'}" width="15" height="15" >
                                       <!--<img src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="TravelSpeedInfo" title="{$page['Text']['travel_speed_info']|escape:'html'}" alt="{$page['Text']['travel_speed_info']|escape:'html'}" width="15" height="15" >-->  
                                        
                                    </p>
                                    <p>
                                        
                                       <label>{$page['Labels']['BookingCapacityLimit']|escape:'html'}: <sup>*</sup></label>
                                       &nbsp;&nbsp;
                                        
                                       <input type="text" name="BookingCapacityLimit" id="BookingCapacityLimit" value="{$BookingCapacityLimit|default:'75'}" style="width:80px;" > %
                                            
                                      
                                       
                                       <img src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="BookingCapacityLimitHelp" class="helpTextIconQtip"  title="{$page['Text']['help_info']|escape:'html'}" alt="{$page['Text']['help_info']|escape:'html'}" width="15" height="15" >
                                       <!--<img src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="TravelSpeedInfo" title="{$page['Text']['travel_speed_info']|escape:'html'}" alt="{$page['Text']['travel_speed_info']|escape:'html'}" width="15" height="15" >-->  
                                        
                                    </p>
                                    <p>
                                        
                                       <label>Keep engineer within postcode Area: </label>
                                       &nbsp;&nbsp;
                                        
                                       <input type="checkbox" name="KeepEngInPCArea" id="KeepEngInPCArea" value="Yes" {if $KeepEngInPCArea=="Yes"}checked=checked{/if} > 
                                            
                                      
                                       
                                       <img src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="KeepEngInPCAreaHelp" class="helpTextIconQtip"  title="{$page['Text']['help_info']|escape:'html'}" alt="{$page['Text']['help_info']|escape:'html'}" width="15" height="15" >
                                       <!--<img src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="TravelSpeedInfo" title="{$page['Text']['travel_speed_info']|escape:'html'}" alt="{$page['Text']['travel_speed_info']|escape:'html'}" width="15" height="15" >-->  
                                        
                                    </p>
                                    
                                    
                                 
                                   <p>&nbsp;</p>        
                                   <p style="text-align:center" >          
                                        <input type="submit" name="saveRecord2" id="saveRecord2"    class="form-button"   value="{$page['Buttons']['save_record']|escape:'html'}" > 

                                       &nbsp;&nbsp;
                                        <input type="submit" name="cancelChanges2" id="cancelChanges2"   class="form-button cancelChanges"   value="{$page['Buttons']['cancel_changes']|escape:'html'}" > 
                                        <span id="processDisplayText2" style="color:red;display:none;" ><img src="{$_subdomain}/css/Skins/{$_theme}/images/loader.gif" width="16" height="16" >&nbsp;&nbsp;{$page['Buttons']['process_record']|escape:'html'}</span>            
                                  </p>                     
                                
                               {if $DiaryWizardSetup}
                                      <p style="text-align:right;position:relative;bottom:-30px;" >
                                        <input type="submit" name="previousButton" id="previousButton" style="width:80px;"    class="form-button"   value="&laquo; {$page['Buttons']['previous']|escape:'html'}" > 
                                        &nbsp;&nbsp;
                                        <input type="submit" name="nextButton" id="nextButton"  style="width:80px;"  class="form-button"   value="{$page['Buttons']['next']|escape:'html'} &raquo;" > 
                                        
                                      </p>     
                              {/if} 
                           </div>    
                                        
                           
                                        
                            <div style="width:100%;display:none;" id="Default_ID3_Div" > 
                                <img src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="TradeAccountHelp" class="helpTextIconQtip" style="float:right;"  title="{$page['Text']['help_info']|escape:'html'}" alt="{$page['Text']['help_info']|escape:'html'}" width="15" height="15" >
                                
                                <h3 class="SiteMapPageHeader" >{$page['Labels']['trade_account_defaults']|escape:'html'} </h3>
                                
                                
                                
                                 <div  id="TradeAccountResultsPanel" >
                                     
                                        <table id="TradeAccountResults" border="0" cellpadding="0" cellspacing="0" class="browse" >
                                            <thead>
                                                    <tr>
                                                            <th></th>
                                                            <th  title="{$page['Text']['trade_account']|escape:'html'}" >{$page['Text']['trade_account']|escape:'html'}</th>
                                                            <th width="80px;" title="{$page['Text']['post_code']|escape:'html'}" >{$page['Text']['post_code']|escape:'html'}</th>
                                                            <th width="20px" title="{$page['Text']['mon']|escape:'html'}" >{$page['Text']['mon']|escape:'html'}</th>
                                                            <th width="20px" title="{$page['Text']['tue']|escape:'html'}" >{$page['Text']['tue']|escape:'html'}</th>
                                                            <th width="20px" title="{$page['Text']['wed']|escape:'html'}" >{$page['Text']['wed']|escape:'html'}</th>
                                                            <th width="20px" title="{$page['Text']['thu']|escape:'html'}" >{$page['Text']['thu']|escape:'html'}</th>
                                                            <th width="20px" title="{$page['Text']['fri']|escape:'html'}" >{$page['Text']['fri']|escape:'html'}</th>
                                                            <th width="20px" title="{$page['Text']['sat']|escape:'html'}" >{$page['Text']['sat']|escape:'html'}</th>
                                                            <th width="20px" title="{$page['Text']['sun']|escape:'html'}" >{$page['Text']['sun']|escape:'html'}</th>
                                                    </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>  

                                  </div> 
                                
                                <div class="bottomButtonsPanel" >
                                </div>
                                                    
                                 {if $DiaryWizardSetup}
                                      <p style="text-align:right;position:relative;bottom:0px;" >
                                        <input type="submit" name="previousButton" id="previousButton" style="width:80px;"    class="form-button"   value="&laquo; {$page['Buttons']['previous']|escape:'html'}" > 
                                        &nbsp;&nbsp;
                                        <input type="submit" name="nextButton" id="nextButton"  style="width:80px;"  class="form-button"   value="{$page['Buttons']['next']|escape:'html'} &raquo;" > 
                                        
                                      </p>     
                              {/if}                    
                                
                            </div>   
                                                    
                                                    
                            <div style="width:100%;display:none;" id="Default_ID4_Div" > 
                                   
                                    <h3 class="SiteMapPageHeader" >{$page['Labels']['administrator_defaults']|escape:'html'} </h3>


                                    <p>&nbsp;</p>
                                    
                                    
                                    
                                    
                                    <p>
                                        
                                        <label>{$page['Labels']['email']|escape:'html'}:<sup>*</sup></label>&nbsp;&nbsp;
                                        
                                        
                                        <input type="text" name="DiaryAdministratorEmail" id="DiaryAdministratorEmail" value="{$DiaryAdministratorEmail|escape:'html'}" style="width:220px;" >
                                            
                                        
                                        <img src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="DiaryAdministratorEmailHelp" class="helpTextIconQtip"  title="{$page['Text']['help_info']|escape:'html'}" alt="{$page['Text']['help_info']|escape:'html'}" width="15" height="15" >
                                        
                                    </p>
                                    
                                    <p>
                                        
                                        <label>{$page['Labels']['unlocking_password']|escape:'html'}:<sup>*</sup></label>&nbsp;&nbsp;
                                        
                                        
                                        <input type="text" name="UnlockingPassword" id="UnlockingPassword" value="{$UnlockingPassword|escape:'html'}" style="width:220px;" >
                                            
                                        <img src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="UnlockingPasswordHelp" class="helpTextIconQtip"  title="{$page['Text']['help_info']|escape:'html'}" alt="{$page['Text']['help_info']|escape:'html'}" width="15" height="15" >
                                        
                                    </p>
                                    
                                   
                                    
                                    <p>
                                           
                                           <label> {$page['Labels']['password_protect_bookings']|escape:'html'}:</label>&nbsp;&nbsp;
                                            
                                           <input type="checkbox" name="PasswordProtectNextDayBookings" id="PasswordProtectNextDayBookings" value="Yes"  {if $PasswordProtectNextDayBookings eq 'Yes'} checked="checked" {/if}  >
                                        
                                           &nbsp;&nbsp;&nbsp;
                                           
                                           {$page['Labels']['after']|escape:'html'}
                                           
                                           <input type="text" name="PasswordProtectNextDayBookingsTime"  id="PasswordProtectNextDayBookingsTime" value="{$PasswordProtectNextDayBookingsTime|escape:'html'}" style="width:50px;" >
                                    
                                           <img src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="NextDayBookingsTimeHelp" class="helpTextIconQtip"  title="{$page['Text']['help_info']|escape:'html'}" alt="{$page['Text']['help_info']|escape:'html'}" width="15" height="15" >
                                        
                                    </p>
                                    
                                    
                                    
                                    
                              
                                  <p>&nbsp;</p>        
                                  <p style="text-align:center" >          
                                        <input type="submit" name="saveRecord4" id="saveRecord4"    class="form-button"   value="{$page['Buttons']['save_record']|escape:'html'}" > 

                                       &nbsp;&nbsp;
                                        <input type="submit" name="cancelChanges4" id="cancelChanges4"   class="form-button cancelChanges"   value="{$page['Buttons']['cancel_changes']|escape:'html'}" > 
                                        <span id="processDisplayText4" style="color:red;display:none;" ><img src="{$_subdomain}/css/Skins/{$_theme}/images/loader.gif" width="16" height="16" >&nbsp;&nbsp;{$page['Buttons']['process_record']|escape:'html'}</span>            
                                  </p>  

                                  {if $DiaryWizardSetup}
                                      <p style="text-align:right;position:relative;bottom:-100px;" >
                                        <input type="submit" name="previousButton" id="previousButton" style="width:80px;"    class="form-button"   value="&laquo; {$page['Buttons']['previous']|escape:'html'}" > 
                                        &nbsp;&nbsp;
                                        <input type="submit" name="nextButton" id="nextButton"  style="width:80px;"  class="form-button"   value="{$page['Buttons']['next']|escape:'html'} &raquo;" > 
                                        
                                      </p>     
                              {/if} 

                              </div>                             
                                                    
                               
                                  
                                  
                              <div style="width:100%;display:none;" id="Default_ID5_Div" > 
                                   
                                    <h3 class="SiteMapPageHeader" >{$page['Labels']['configuration_defaults']|escape:'html'} </h3>


                                    <p>&nbsp;</p>
                                    
                                    
                                    
                                    
                                    <p>
                                        
                                        <label>{$page['Labels']['ip_address']|escape:'html'}:<sup>*</sup></label>&nbsp;&nbsp;
                                        
                                        
                                        <input type="text" name="IPAddress" id="IPAddress" value="{$ServiceProviderRow.IPAddress|escape:'html'}" style="width:220px;" maxlength="40" >
                                            
                                        
                                    </p>
                                    
                                    <p>
                                        
                                        <label>{$page['Labels']['port_number']|escape:'html'}:<sup>*</sup></label>&nbsp;&nbsp;
                                        
                                        
                                        <input type="text" name="Port" id="Port" value="{$ServiceProviderRow.Port|escape:'html'}" style="width:220px;" >
                                            
                                        
                                    </p>
                                   {if !$SuperAdmin} <label  >{$page['Labels']['viamente_licence']|escape:'html'}{if $ServiceProviderID==64}(Brown){/if}:</label>&nbsp;&nbsp;&nbsp;
                                   {$ServiceProviderRow.ViamenteKey|escape:'html'}

                                   <input type="hidden" name="ViamenteKey" id="ViamenteKey" value="{$ServiceProviderRow.ViamenteKey|escape:'html'}" style="width:220px;" maxlength="50" >
                                   {/if}
                                     
                                    {if $SuperAdmin}
                                     <p>
                                        
                                        <label>{$page['Labels']['viamente_licence']|escape:'html'} {if $ServiceProviderID==64}(Brown){/if}:<sup>*</sup></label>&nbsp;&nbsp;
                                        
                                        
                                        <input type="text" name="ViamenteKey" id="ViamenteKey" value="{$ServiceProviderRow.ViamenteKey|escape:'html'}" style="width:220px;" maxlength="50" >
                                            
                                        
                                    </p>
                                        <p>
                                        
                                            <label>{$page['Labels']['number_of_vehicles']|escape:'html'}:</label>&nbsp;&nbsp;


                                            <input type="text" name="NumberOfVehicles" id="NumberOfVehicles" value="{$ServiceProviderRow.NumberOfVehicles|escape:'html'}" style="width:220px;" maxlength="50" >


                                        </p>

                                        <p>

                                            <label>{$page['Labels']['number_of_waypoints']|escape:'html'}:</label>&nbsp;&nbsp;


                                            <input type="text" name="NumberOfWaypoints" id="NumberOfWaypoints" value="{$ServiceProviderRow.NumberOfWaypoints|escape:'html'}" style="width:220px;" maxlength="50" >


                                        </p>
                                        
                                        
                                        
                                    
                                  
                                    {else}    
                                        
                                        
                                         <p>
                                        
                                            <label>{$page['Labels']['number_of_vehicles']|escape:'html'}:</label>&nbsp;&nbsp;


                                           {$ServiceProviderRow.NumberOfVehicles|escape:'html'}
                                           <input type="hidden" name="NumberOfVehicles" id="NumberOfVehicles" value="{$ServiceProviderRow.NumberOfVehicles|escape:'html'}" style="width:220px;" maxlength="50" >
                                            
                                        </p>

                                        <p>

                                            <label>{$page['Labels']['number_of_waypoints']|escape:'html'}:</label>&nbsp;&nbsp;

                                            {$ServiceProviderRow.NumberOfWaypoints|escape:'html'}
                                            <input type="hidden" name="NumberOfWaypoints" id="NumberOfWaypoints" value="{$ServiceProviderRow.NumberOfWaypoints|escape:'html'}" style="width:220px;" maxlength="50" >
                                         
                                        
                                        </p>
                                        
                                        
                                    {/if}    
                                    <br>
                                    {if $SuperAdmin}
                                          <p>
                                        
                                        <label>Viamente License (White):<sup>*</sup></label>&nbsp;&nbsp;
                                        
                                        
                                        <input type="text" name="ViamenteKey2" id="ViamenteKey2" value="{$ServiceProviderRow.ViamenteKey2|escape:'html'}" style="width:220px;" maxlength="50" >
                                            
                                        
                                    </p>
                                    
                                        <p>
                                        
                                            <label>{$page['Labels']['number_of_vehicles']|escape:'html'}:</label>&nbsp;&nbsp;


                                            <input type="text" name="NumberOfVehicles" id="ViamenteKey2MaxEng" value="{$ServiceProviderRow.ViamenteKey2MaxEng|escape:'html'}" style="width:220px;" maxlength="50" >


                                        </p>

                                        <p>

                                            <label>{$page['Labels']['number_of_waypoints']|escape:'html'}:</label>&nbsp;&nbsp;


                                            <input type="text" name="NumberOfWaypoints" id="ViamenteKey2MaxWayPoints" value="{$ServiceProviderRow.ViamenteKey2MaxWayPoints|escape:'html'}" style="width:220px;" maxlength="50" >


                                        </p>
                                        
                                        
                                        
                                    
                                  
                                    {elseif $ServiceProviderID==64}    
                                        
                                         <p>
                                        
                                        <label>Viamente License (White):<sup>*</sup></label>&nbsp;&nbsp;
                                        
                                        
                                        {$ServiceProviderRow.ViamenteKey2|escape:'html'}
                                            
                                        
                                    </p>
                                         <p>
                                        
                                            <label>{$page['Labels']['number_of_vehicles']|escape:'html'}:</label>&nbsp;&nbsp;


                                           {$ServiceProviderRow.ViamenteKey2MaxEng|escape:'html'}
                                           <input type="hidden" name="NumberOfVehicles" id="NumberOfVehicles" value="{$ServiceProviderRow.ViamenteKey2MaxEng|escape:'html'}" style="width:220px;" maxlength="50" >
                                            
                                        </p>

                                        <p>

                                            <label>{$page['Labels']['number_of_waypoints']|escape:'html'}:</label>&nbsp;&nbsp;

                                            {$ServiceProviderRow.ViamenteKey2MaxWayPoints|escape:'html'}
                                            <input type="hidden" name="NumberOfWaypoints" id="NumberOfWaypoints" value="{$ServiceProviderRow.ViamenteKey2MaxWayPoints|escape:'html'}" style="width:220px;" maxlength="50" >
                                         
                                        
                                        </p>
                                        
                                        
                                    {/if}
                                    
                                    
                                   
                                    
                              
                                  <p>&nbsp;</p>        
                                  <p style="text-align:center" >   
                                      
                                       <input type="hidden" name="OnlineDiary" id="OnlineDiary" value="{$ServiceProviderRow.OnlineDiary|escape:'html'}" style="width:220px;" maxlength="50" >
                                       
                                       <input type="submit" name="saveRecord5" id="saveRecord5"    class="form-button"   value="{$page['Buttons']['save_record']|escape:'html'}" > 

                                       &nbsp;&nbsp;
                                        <input type="submit" name="cancelChanges5" id="cancelChanges5"   class="form-button cancelChanges"   value="{$page['Buttons']['cancel_changes']|escape:'html'}" > 
                                       
                                       <span id="processDisplayText5" style="color:red;display:none;" ><img src="{$_subdomain}/css/Skins/{$_theme}/images/loader.gif" width="16" height="16" >&nbsp;&nbsp;{$page['Buttons']['process_record']|escape:'html'}</span>            
                                  </p>  
                                  
                                  

                                  {if $DiaryWizardSetup}
                                      <p style="text-align:right;position:relative;bottom:-100px;" >
                                        {*
                                          <input type="submit" name="previousButton" id="previousButton" style="width:80px;"    class="form-button"   value="&laquo; {$page['Buttons']['previous']|escape:'html'}" > 
                                        &nbsp;&nbsp;
                                        *}
                                        <input type="submit" name="nextButton" id="nextButton"  style="width:80px;"  class="form-button"   value="{$page['Buttons']['next']|escape:'html'} &raquo;" > 
                                        
                                      </p>     
                                 {/if} 
                                  

                              </div>
                                                    
                                                    
                                 
                                  
                              
                            <div style="width:100%;display:none;" id="Default_ID6_Div" > 
                                   
                                    <h3 class="SiteMapPageHeader" >{$page['Labels']['insert_appointment_defaults']|escape:'html'} </h3>


                                    <p>&nbsp;</p>
                                    
                                    
                                    
                                    
                                    <p>
                                        
                                        <label style="text-align:left;margin-left:130px;width:140px;" >
                                            <input type="checkbox" name="AutoSelectDayChkBox" id="AutoSelectDayChkBox"  {if $ServiceProviderRow.AutoSelectDay neq ''} checked="checked"  {/if} value="1"  >
                                            
                                            {$page['Labels']['auto_select_day']|escape:'html'}</label>
                                        
                                            <div style="float:right;width:240px;text-align:left;" >
                                                <input type="radio" name="AutoSelectDay"  value="Today"  {if $ServiceProviderRow.AutoSelectDay eq "Today"} checked="checked"  {/if}  > {$page['Labels']['today']|escape:'html'}<br>
                                                <input type="radio" name="AutoSelectDay"  value="NextDay" {if $ServiceProviderRow.AutoSelectDay eq "NextDay"} checked="checked"  {/if}  > {$page['Labels']['next_day']|escape:'html'}
                                            
                                            <img src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="AutoSelectDayHelp" class="helpTextIconQtip"  title="{$page['Text']['help_info']|escape:'html'}" alt="{$page['Text']['help_info']|escape:'html'}" width="15" height="15" >
                                        
                                            </div>    
                                    
                                            
                                    </p>
                                    
                                   <p id="AutoSelectDayError" style="padding:0px;margin:0px;" ></p>
                                    
                                    <p>
                                       
                                        <label style="text-align:left;margin-left:130px;width:140px;" >
                                            <input type="checkbox" name="AutoDisplayTableChkBox" id="AutoDisplayTableChkBox"  {if $ServiceProviderRow.AutoDisplayTable neq ''} checked="checked"  {/if} value="1"  >
                                            
                                            {$page['Labels']['auto_display_table']|escape:'html'}</label>
                                        
                                            <div style="float:right;width:240px;text-align:left;" >
                                                <input type="radio" name="AutoDisplayTable"  value="Summary"  {if $ServiceProviderRow.AutoDisplayTable eq "Summary"} checked="checked"  {/if}  > {$page['Labels']['summary_table']|escape:'html'}<br>
                                                <input type="radio" name="AutoDisplayTable"  value="Appointment" {if $ServiceProviderRow.AutoDisplayTable eq "Appointment"} checked="checked"  {/if}  > {$page['Labels']['appointment_table']|escape:'html'}
                                           
                                            <img src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="AutoDisplayTableHelp" class="helpTextIconQtip"  title="{$page['Text']['help_info']|escape:'html'}" alt="{$page['Text']['help_info']|escape:'html'}" width="15" height="15" >
                                            </div>    
                                        
                                           
                                        
                                    </p>
                                   
                                    <p id="AutoDisplayTableError" style="padding:0px;margin:0px;" ></p>
                                    
                                    <p>
                                        
                                        <label style="text-align:left;margin-left:130px;width:320px;" >
                                            <input onclick="checkViamenteRunType()" type="checkbox" name="AutoSpecifyEngineerByPostcode" id="AutoSpecifyEngineerByPostcode"  {if $ServiceProviderRow.AutoSpecifyEngineerByPostcode eq 'Yes'} checked="checked"  {/if} value="Yes"  >
                                            
                                            Auto Specify Engineers by Postcode Allocation</label>
                                        
                                               
                                        <img src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="AutoSpecifyEngineerByPostcodeHelp" class="helpTextIconQtip"  title="{$page['Text']['help_info']|escape:'html'}" alt="{$page['Text']['help_info']|escape:'html'}" width="15" height="15" >
                                        
                                    </p>
                                    <p id="ViamenteRunTypeP"  {if $ServiceProviderRow.AutoSpecifyEngineerByPostcode != 'Yes'}style="display:none"{/if}>
                                        
                                        <label style="text-align:left;margin-left:130px;width:400px;" >
                                            &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="ViamenteRunType" id="ViamenteRunType1"  {if $ServiceProviderRow.ViamenteRunType eq 'AllEngineers'} checked="checked"  {/if} value="AllEngineers"     > Specify and lock an engineer to the appointment
                                             <img src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="AutoSpecifyEngineerByPostcodeHelp2" class="helpTextIconQtip"  title="{$page['Text']['help_info']|escape:'html'}" alt="{$page['Text']['help_info']|escape:'html'}" width="15" height="15" >
                                            <br>&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="ViamenteRunType" id="ViamenteRunType2"  {if $ServiceProviderRow.ViamenteRunType eq 'CurrentEngineer'} checked="checked"  {/if} value="CurrentEngineer"  > Temporarily allocate an engineer to the appointment 
                                            <img src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="AutoSpecifyEngineerByPostcodeHelp3" class="helpTextIconQtip"  title="{$page['Text']['help_info']|escape:'html'}" alt="{$page['Text']['help_info']|escape:'html'}" width="15" height="15" >
                                           </label>
                                        
                                               
                                       
                                        
                                        
                                    </p>
                                    
                                    
                                    <p>&nbsp;</p>
                                    <p>
                                        
                                        <label style="text-align:left;margin-left:135px;width:90px;" >
                                            
                                            {$page['Labels']['multimaps']|escape:'html'}
                                         </label>   
                                            <input type="radio" name="Multimaps" {if $ServiceProviderRow.Multimaps eq '2'} checked="checked"  {/if} value="2"  > 2
                                            <input type="radio" name="Multimaps" {if $ServiceProviderRow.Multimaps eq '4'} checked="checked"  {/if} value="4"  > 4
                                            <input type="radio" name="Multimaps" {if $ServiceProviderRow.Multimaps eq '6'} checked="checked"  {/if} value="6"  > 6
                                            
                                            <img src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="MultimapsHelp" class="helpTextIconQtip"  title="{$page['Text']['help_info']|escape:'html'}" alt="{$page['Text']['help_info']|escape:'html'}" width="15" height="15" >
                                         
                                        
                                               
                                        
                                        
                                    </p>
                                    <p>&nbsp;</p>
                                    <p>
                                       
                                           <label style="text-align:left;margin-left:130px;width:200px;">
                                            
                                           <input type="checkbox" name="AutoChangeTimeSlotLabel" id="AutoChangeTimeSlotLabel" value="Yes"  {if $AutoChangeTimeSlotLabel eq 'Yes'} checked="checked" {/if}  >
                                            
                                           {$page['Labels']['auto_change_time_slot_label']|escape:'html'} </label>
                                            
                                            
                                             
                                           <img src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="AutoChangeTimeSlotLabelHelp" class="helpTextIconQtip"  title="{$page['Text']['help_info']|escape:'html'}" alt="{$page['Text']['help_info']|escape:'html'}" width="15" height="15" >
                                        
                                    </p>
                                    
                                    <p>
                                       
                                           <label style="text-align:left;margin-left:130px;width:200px;">
                                            
                                           <input type="checkbox" name="DiaryShowSlotNumbers" id="DiaryShowSlotNumbers" value="Yes"  {if $ServiceProviderRow.DiaryShowSlotNumbers eq 'Yes'} checked="checked" {/if}  >
                                             Show Slot Numbers
                                            </label>
                                            
                                            
                                             
                                           <img src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="DiaryShowSlotNumbersHelp" class="helpTextIconQtip"  title="{$page['Text']['help_info']|escape:'html'}" alt="{$page['Text']['help_info']|escape:'html'}" width="15" height="15" >
                                        
                                    </p>
                                     <p>
                                       
                                           <label style="text-align:left;margin-left:130px;width:200px;">
                                            
                                           <input type="checkbox" name="EmailAppBooking" id="DiaryShowSlotNumbers" value="Yes"  {if $ServiceProviderRow.EmailAppBooking eq 'Yes'} checked="checked" {/if}  >
                                             Send One Touch App Email
                                            </label>
                                            
                                            
                                             
                                           <img src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="EmailAppBookingHelp" class="helpTextIconQtip"  title="{$page['Text']['help_info']|escape:'html'}" alt="{$page['Text']['help_info']|escape:'html'}" width="15" height="15" >
                                        
                                    </p>
                              
                                  <p>&nbsp;</p>        
                                  <p style="text-align:center" >          
                                        <input type="submit" name="saveRecord6" id="saveRecord6"    class="form-button"   value="{$page['Buttons']['save_record']|escape:'html'}" > 

                                       &nbsp;&nbsp;
                                        <input type="submit" name="cancelChanges6" id="cancelChanges6"   class="form-button cancelChanges"   value="{$page['Buttons']['cancel_changes']|escape:'html'}" > 
                                        <span id="processDisplayText6" style="color:red;display:none;" ><img src="{$_subdomain}/css/Skins/{$_theme}/images/loader.gif" width="16" height="16" >&nbsp;&nbsp;{$page['Buttons']['process_record']|escape:'html'}</span>            
                                  </p>  

                                  {if $DiaryWizardSetup}
                                      <p style="text-align:right;position:relative;bottom:-100px;" >
                                        <input type="submit" name="previousButton" id="previousButton" style="width:80px;"    class="form-button"   value="&laquo; {$page['Buttons']['previous']|escape:'html'}" > 
                                        &nbsp;&nbsp;
                                        <input type="submit" name="nextButton" id="nextButton"  style="width:80px;"  class="form-button"   value="{$page['Buttons']['next']|escape:'html'} &raquo;" > 
                                        
                                      </p>     
                                  {/if} 

                              </div>      
                                  
                                  
                           {if $DiaryWizardSetup}
                           
                               <div style="width:100%;display:none;" id="Default_ID7_Div" > 
                                   
                                   
                                  
                                    
                                    
                                    {if $dataImportedToSB}
                                        
                                        <h3 class="SiteMapPageHeader" >{$page['Text']['setup_complete']|escape:'html'} </h3>


                                        <p>&nbsp;</p>

                                        <p>

                                             <h3>{$page['Text']['setup_complete_heading']|escape:'html'}</h3>  <br>
                                             
                                             {$page['Text']['setup_complete_text1']|escape:'html'}<br><br>
                                             {$page['Text']['setup_complete_text2']|escape:'html'}
                                             
                                             <br>
                                             
                                             <h3>{$page['Text']['setup_complete2_heading']|escape:'html'}</h3>  <br>
                                             
                                             {$page['Text']['setup_complete_text3']|escape:'html'}<br><br>
                                             {$page['Text']['setup_complete_text4']|escape:'html'} <input type="checkbox" name="step2" id="step2" value="1" >

                                        </p>
                                        
                                        <h3 id="step2TextHeading" >{$page['Text']['setup_complete3_heading']|escape:'html'}</h3>  <br>
                                        <p id="step2Text" >
                                            
                                             {$page['Text']['setup_complete_text5']}<br><br>
                                             {$page['Text']['setup_complete_text6']|escape:'html'}
                                            
                                        </p>   
                                        
                                        
                                         <p style="position:relative;bottom:-20px;" >
                                            {*<input type="submit" name="previousButton" id="previousButton" style="width:80px;float:left;"    class="form-button proceed"   value="{$page['Buttons']['proceed']|escape:'html'}" > 
                                            &nbsp;&nbsp;*}
                                            <input type="submit" name="nextButton" id="nextButton"  style="width:80px;float:right;"  class="form-button"   value="{$page['Buttons']['finish']|escape:'html'}" > 

                                          </p> 
                                      
                                     {else}
                                         
                                          <h3 class="SiteMapPageHeader" >{$page['Text']['setup_not_complete']|escape:'html'} </h3>


                                          <p>&nbsp;</p>
                                          
                                          <p style="text-align:center;" >
                                              {$page['Text']['setup_not_complete_text']}
                                          </p>
                                          <p style="position:relative;bottom:-20px;" >
                                           
                                            <input type="submit" name="retryButton" id="retryButton"  style="width:80px;float:right;"  class="form-button"   value="{$page['Buttons']['retry']|escape:'html'}" > 

                                          </p> 
                                        
                                     {/if}   
                                  
                                         
                                 

                                 </div>    
                                        
                                        
                                        
                                  <div style="width:100%;display:none;" id="Default_ID8_Div" > 
                                   
                                   
                                   <h3 class="SiteMapPageHeader" >{$page['Text']['running_communication_test']|escape:'html'} </h3>


                                     <p>&nbsp;</p>
                                    
                                    
                                    

                                     <p style="text-align:center;" >

                                         {if $sk_sb_test and $sk_va_test}
                                            <img src="{$_subdomain}/images/communication-test-blue-blue.png" width="443" height="285" > 
                                         {elseif $sk_sb_test}   
                                             <img src="{$_subdomain}/images/communication-test-blue-red.png" width="443" height="285" > 
                                         {elseif $sk_va_test}     
                                             <img src="{$_subdomain}/images/communication-test-red-blue.png" width="443" height="285" > 
                                         {else}
                                             <img src="{$_subdomain}/images/communication-test-red-red.png" width="443" height="285" >
                                         {/if}    
                                     </p>

                                        
                                    
                                  
                                      <p style="text-align:right;position:relative;bottom:-40px;" >
                                        <input type="submit" name="previousButton" id="previousButton" style="width:80px;"    class="form-button"   value="&laquo; {$page['Buttons']['previous']|escape:'html'}" > 
                                        &nbsp;&nbsp;
                                        <input type="submit" name="nextButton" id="nextButton"  style="width:80px;"  class="form-button"   value="{$page['Buttons']['next']|escape:'html'} &raquo;" > 
                                        
                                      </p>     
                                 

                                 </div>        
                                  
                            {/if}   
                                  
                                <div style="width:100%;display:none;text-align: center;" id="Default_ID9_Div" > 
                                   
                                   
                                   <h3 class="SiteMapPageHeader" >Grid Map Defaults </h3>


                                     <p>&nbsp;</p>
                                    
                                    
                                    

                                    
                                         <div style="width:100%;text-align:center">
                                            
                                     <table   style="text-align: center;">
                                         <tr>
                                             <td style="width:150px">
                                             Allocation Type:
                                             </td>
                                             <td colspan=2 style="background:none">
                                                 
                                                 <input {if !isset($data['DiaryAllocationType'])||(isset($data['DiaryAllocationType'])&&$data['DiaryAllocationType']=="Postcode")}checked="checked" disabled="disabled"{else}disabled="disabled"{/if} type="radio" name="allType" value="postcode"> Postcode
                                             
                                                 <input {if isset($data['DiaryAllocationType'])&&$data['DiaryAllocationType']=="GridMapping"}checked="checked"{/if} {if !isset($data['DiaryAllocationType'])||(isset($data['DiaryAllocationType'])&&$data['DiaryAllocationType']=="Postcode")} disabled="disabled" {/if} type="radio" name="allType" value="gridmap"> Map Area
                                             </td>
                                             
                                         </tr>
                                         {if isset($data['DiaryAllocationType'])}
                                         <tr><td style="background:none">Grid Position:</td>
                                             <td style="background:none">Latitude From:<input id="glat2" name="lat2" type="text" value="{$data['Lat2']}" ></td>
                                             <td style="background:none">Latitude To:<input id="glat1"  name="lat1" type="text" value="{$data['Lat1']}" ></td>
                                         </tr>
                                           <tr><td style="background:none"></td>
                                             <td style="background:none">Longitude From:<input id="glng2"  name="lng2" type="text" value="{$data['Lng2']}"  ></td>
                                             <td style="background:none">Longitude to:<input id="glng1" name="lng1" type="text" value="{$data['Lng1']}"></td>
                                         </tr>
                                           <tr><td style="background:none">Grid Cell Size</td>
                                             <td style="background:none"><input id="csize" onchnage="changeCsize()"  name="csize" type="text" value="{$data['CellSize']}"></td>
                                             <td style="background:none"><input  name="csizeEx" id="csizeEx" type="text" value=""></td>
                                         </tr>
                                         <tr>
                                             <td colspan=3 Style="text-align:center"><button  style="margin:auto;float:left" onclick="mapPrewiev()" type="button" class="gplus-blue">Preview Map</button><button type="button" onclick="saveGridDef();" style="margin:auto;float:right"  class="gplus-blue">Save</button></td>
                                         </tr>
                                         {/if}
                                     </table>
                                              </div>
                                    
                                    
                                     <script>
                                     function changeCsize()
                                     {
                                         ab=111.1*($('#csize').val()*1)
                                         ab=ab.toFixed(2)
                                     $('#csizeEx').val(ab+" X "+ab+" KM")
                                     }
                                     </script>
                                        
                                    
                                  
                                         
                                 

                                 </div>  
                                     
                                     <!--bank holiday def-->
                                     
                                      <div style="width:100%;display:none;text-align: center;" id="Default_ID10_Div" > 
                                   
                                   
                                   <div style="float: left;"><h3 class="SiteMapPageHeader" >Bank Holiday Defaults </h3></div>
                                   <div style="float: right; padding-top: 10px;">
                                        <img src="{$_subdomain}/css/Skins/{$_theme}/images/eng.ico" id="engIcon" title="{$page['Text']['engIcon_help_info']|escape:'html'}" alt="{$page['Text']['engIcon_help_info']|escape:'html'}" style="cursor: pointer;" onclick="displayCountryHolidays('1');" />&nbsp;
                                        <img src="{$_subdomain}/css/Skins/{$_theme}/images/sco.ico" id="scoIcon" title="{$page['Text']['scoIcon_help_info']|escape:'html'}" alt="{$page['Text']['scoIcon_help_info']|escape:'html'}" style="cursor: pointer;" onclick="displayCountryHolidays('3');" />&nbsp;
                                        <img src="{$_subdomain}/css/Skins/{$_theme}/images/ire.ico" id="ireIcon" title="{$page['Text']['ireIcon_help_info']|escape:'html'}" alt="{$page['Text']['ireIcon_help_info']|escape:'html'}" style="cursor: pointer;" onclick="displayCountryHolidays('2');" />
                                   </div>

                                     <p>&nbsp;</p>
                                    
                                    
                                    

                                    
                                         <div style="width:100%;text-align:center">
                                          
                                     <table border="0" cellpadding="0" cellspacing="0" class="browse" id="bank_holdiay_table"   style="text-align: center;">
                                         <thead>
                                             <tr>
                                         <th>ID</th>
                                         <th>Date</th>
                                         <th>National Bank Holiday</th>
                                         <th>Status</th>
                                         </tr>
                                         </thead>
                                         <tbody>
                                             {if isset($data)}
                                          {foreach $data as $a}
                                              <tr>
                                                  <td>{$a.NationalBankHolidayID	}</td>
                                                  <td>{$a.HolidayDate|date_format:"%d/%m/%Y"	}</td>
                                                  <td>{$a.HolidayName}</td>
                                                  <td>
                                                      <select id="bankHolSel_{$a.NationalBankHolidayID}" onchange="$('#processDisplayText_{$a.NationalBankHolidayID}').show();setBankHolidayStatus('{$a.NationalBankHolidayID}');" style="width:150px" name="bankHolidayStatus">
                                                      {if $cId eq 1}
                                                          <option value="Closed">Closed</option>
                                                          <option value="Open" {if $a.Status=="Open"}selected=selected{/if}>Open</option>
                                                      {else}
                                                          <option value="Open">Open</option>  
                                                          <option value="Closed" {if $a.Status=="Closed"}selected=selected{/if}>Closed</option>
                                                      {/if}
                                                      </select>
                                                      <span id="processDisplayText_{$a.NationalBankHolidayID}" style="color:red;display:none;" ><img src="{$_subdomain}/css/Skins/{$_theme}/images/loader.gif" width="16" height="16" ></span>   
                                                  </td>
                                              </tr>
                                         {/foreach}
                                         {/if}
                                         </tbody>
                                     </table>
                                     <div id="holidayButtons" class="bottomButtonsPanelHolder" style="position: relative; top: -41px; float:right;">
                                        <img src="{$_subdomain}/css/Skins/{$_theme}/images/add_icon.png" id="bhInsertBtn" title="{$page['Text']['addbankholiday_help_info']|escape:'html'}" alt="{$page['Text']['addbankholiday_help_info']|escape:'html'}" style="cursor: pointer;" />
                                    </div>
                                         <script>
                                         function displayCountryHolidays(cId)
                                        {
                                            location.href="{$_subdomain}/AppointmentDiary/diaryDefaults/spID="+urlencode({$ServiceProviderID})+"/sPage="+urlencode({$sPage})+"/cId="+urlencode(cId);
                                        }
                                         function setBankHolidayStatus(bid){
                                        
                                         $.post("{$_subdomain}/Diary/setBankHolidayStatus",
{ spid:$("#ServiceProviderID").val(),
    'bid':bid,
    status:$('#bankHolSel_'+bid).val()
    
},
function(data) {
$('#processDisplayText_'+bid).hide();

});
                                         }
                                         </script>
                                         
                                              </div>
                                    
                                    </div> 
                                     <!--bank holiday def-->
                                     
                                     
                                         <div style="width:100%;display:none;text-align: center;" id="Default_ID11_Div" > 
                       <h3 class="SiteMapPageHeader" >{$page['Labels']['finalise_day_defaults']|escape:'html'} <br><br></h3>
                                    
                                    <p>
                                       
                                       
                                           
                                            <label> {$page['Labels']['viamente_consolidation']|escape:'html'}: </label>&nbsp;&nbsp;
                                            
                                            <input style="float:left;margin-left:62px;width:136px; " type="text" name="ViamenteConsolidation" id="ViamenteConsolidation" value="" style="width:80px;margin:0px;" >
                                           
                                            <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="ViamenteConsolidationHelp" class="helpTextIconQtip"  title="{$page['Text']['help_info']|escape:'html'}" alt="{$page['Text']['help_info']|escape:'html'}" width="15" height="15" >
                                       
                                        
                                    </p>
                                    
                                   
                                    
                                     <p>
                                       
                                        <label>{$page['Labels']['individual_engineers']|escape:'html'}: </label>&nbsp;&nbsp;
                                              
                                        <input style="float:left;margin-left:62px " type="button" name="individual_engineers" id="individual_engineers" class="form-button" value="{$page['Buttons']['select_engineers']|escape:'html'}">
                                             
                                        <img style="float:left" src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="IndividualEngineersHelp" class="helpTextIconQtip"  title="{$page['Text']['help_info']|escape:'html'}" alt="{$page['Text']['help_info']|escape:'html'}" width="15" height="15" >
                                    </p>
                                    
                                    
                                    <p style="display:none">
                                       
                                        <label>{$page['Labels']['automatically_email_engineer_route_maps']|escape:'html'}: </label>&nbsp;&nbsp;
                                        <input type="text" name="SendRouteMapsEmail" id="SendRouteMapsEmail" value="{$ServiceProviderRow.SendRouteMapsEmail|escape:'html'}" style="width:80px;" maxlength="8" >
                                              
                                            
                                        <img src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="SendRouteMapsEmailHelp" class="helpTextIconQtip"  title="{$page['Text']['help_info']|escape:'html'}" alt="{$page['Text']['help_info']|escape:'html'}" width="15" height="15" >
                                    </p>
                                    
                                    
                                     
                                    
                                    <p>
                                       
                                       
                                           
                                            <label> {$page['Labels']['run_viamente_today']|escape:'html'}: </label>&nbsp;&nbsp;
                                            
                                            <input style="float:left;margin-left:62px " type="checkbox" name="RunViamenteToday" id="RunViamenteToday" value="Yes"  {if $RunViamenteToday eq 'Yes'} checked="checked" {/if}  >
                                             
                                       
                                           <img style="float:left" src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="RunViamenteTodayHelp" class="helpTextIconQtip"  title="{$page['Text']['help_info']|escape:'html'}" alt="{$page['Text']['help_info']|escape:'html'}" width="15" height="15" >
                                        
                                    </p>
                                    <br>
                                    <p>
                                       
                                       
                                           
                                            <label> Lock Appointment Time Window: </label>&nbsp;&nbsp;
                                            
                                            <input type="checkbox" name="LockAppWindow" id="LockAppWindow" value="Yes"  {if $LockAppWindow eq 'Yes'} checked="checked" {/if}  >
                                            <input style="width:80px;" type="text" name="LockAppWindowTime" value="{$ServiceProviderRow.LockAppWindowTime|escape:'html'}"> Mins
                                       
                                           <img src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="LockAppWindowHelp" class="helpTextIconQtip"  title="{$page['Text']['help_info']|escape:'html'}" alt="{$page['Text']['help_info']|escape:'html'}" width="15" height="15" >
                                        
                                    </p>
                                    
                                    
                                    
                                    
                                    
                                   <p id="individual_engineers_values" ></p>
                              
                                   <p>&nbsp;</p>        
                                   <p style="text-align:center" >          
                                        <input type="submit" name="saveRecord2" id="saveRecord2"    class="form-button"   value="{$page['Buttons']['save_record']|escape:'html'}" > 

                                       &nbsp;&nbsp;
                                        <input type="submit" name="cancelChanges2" id="cancelChanges2"   class="form-button cancelChanges"   value="{$page['Buttons']['cancel_changes']|escape:'html'}" > 
                                        <span id="processDisplayText2" style="color:red;display:none;" ><img src="{$_subdomain}/css/Skins/{$_theme}/images/loader.gif" width="16" height="16" >&nbsp;&nbsp;{$page['Buttons']['process_record']|escape:'html'}</span>            
                                  </p>
                                  <p style="text-align:center;">
                                        <input type="button" name="fDayHistroyDetails" id="fDayHistroyDetails" class="form-button" value="{$page['Buttons']['view_history']|escape:'html'}" style="display:none;">
                                    </p>
                      </div>
                                
                            <div style="width:100%;display:none;" id="formDiv" > 
                                 
                                <p>
                                    <h2>Speed Tuner</h2>
                                </p>
                                <p>
                                    {$page['Text']['travel_speed_info_text1']|escape:'html'}<br><br>
                                </p>
                                <p>
                                    {$page['Text']['travel_speed_info_text2']|escape:'html'}<br><br>
                                </p>
                                <p>
                                    {$page['Text']['travel_speed_info_text3']|escape:'html'}<br><br>
                                </p>
                                <p>
                                    {$page['Text']['travel_speed_info_text4']|escape:'html'}<br><br>
                                </p>
                                <p>
                                    <input type="submit" name="closeInfo" id="closeInfo"  style="float:right;"  class="form-button"   value="{$page['Buttons']['close']|escape:'html'}" > 
                                </p>
                            </div>  
                                
                        </div>
                       
                       <div id="thirdDiv" class="firstDiv assistDiv" >
                       
                             <input type="hidden" name="GeneralDefaults" id="GeneralDefaults" value="" >
                                            
                                          
                           
                           
                       </div>
                       
                       
                       <div id="fourthDiv" class="secondDiv assistDiv" >  
                       
                       </div>
                           
                      <input type="hidden" name="navigatePage" id="navigatePage" value="" >    
                   
                        
                      
                 
        
        </form>
    </div> 
                              
                              
       <div style="display:none;" >

        
         <div id="SelectIndividualEngineers" class="SystemAdminFormPanel" >
    
                <form id="SelectIndividualEngineersForm" name="SelectIndividualEngineersForm" method="post"  action="#" class="inline" >

                <fieldset>
                    <legend title="" >{$page['Labels']['individual_engineers']|escape:'html'}</legend>

                    <p><label id="suggestText" ></label></p>
                           
                        
                     <p>
                         <label>&nbsp;</label>
                         <span style="float:right;" >
                            <input type="checkbox" id="TagAllJobs" value="TagAll" >&nbsp;{$page['Labels']['tag_all']|escape:'html'}
                            &nbsp;&nbsp;&nbsp;
                            <input type="checkbox" id="UnTagAllJobs" value="UnTagAll" >&nbsp;{$page['Labels']['un_tag_all']|escape:'html'}
                         </span> 
                      </p>  



                        {foreach $EngineersList as $eng}
                            
                         <p style="text-align:left;padding-left:100px;" >
                            
                             <input  type="checkbox" style="width:30px;" class="text checkBoxPopupPage"  name="ServiceProviderEngineerID[]" value="{$eng.ServiceProviderEngineerID|escape:'html'}"   >&nbsp;&nbsp;{$eng.EngineerName|escape:'html'|trim}

                         </p>
                         {/foreach} 

                        


                        <p>

                            <span class= "bottomButtons" >

                                    <input type="submit" name="insert_save_btn" class="textSubmitButton" id="insert_save_btn"  value="{$page['Buttons']['ok']|escape:'html'}" >

                               &nbsp;
                                    
                                    <input type="submit" name="cancel_btn" class="textSubmitButton" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['cancel']|escape:'html'}" >

                                    <span id="processDisplayText" style="color:red;display:none;" ><img src="{$_subdomain}/css/Skins/{$_theme}/images/loader.gif" width="16" height="16" >&nbsp;&nbsp;{$page['Buttons']['process_record']|escape:'html'}</span>    

                            </span>

                        </p>







                </fieldset>    

                </form>        


        </div>
        
        

    </div>                       
                              
                              
                                    
</div>
                                    <div id="visualendayDiv" style="display:none">
                                        <h3>Please specify the Viamente key that you wish to reset:</h3>
                                        <input type="radio" name="vvggd" value="Brown">Brown Goods Key<br>
                                        <input type="radio" name="vvggd" value="White">White Goods Key<br>
                                        <input type="radio" checked=checked name="vvggd" value="Both">Both Keys<br>
                                        
                                        
                                        <a href="#" onclick="$.colorbox.close()" id="cancelAppraisal" class="btnCancel" style="margin-right:10px;float:right">Cancel</a>  
                                      <a href="#" onclick="removeVisualEndDay()" id="mainSave" class="btnConfirm" style="margin-right:10px;float:right">Save</a>
                                    
                                    </div>  
                                    
 <script>
    function removeVisualEndDay()
    {
        mode=$('input[name=vvggd]:checked').val()
        $.post("{$_subdomain}/AppointmentDiary/removeVisualEndDay",{ 'd':$('#ViamenteConsolidation').val(),'m':mode,'s':$("#ServiceProviderID").val() },
function(data) {
window.location="{$_subdomain}/AppointmentDiary/diaryDefaults/spID="+$("#ServiceProviderID").val();
});

    }
 </script>
{/block}
