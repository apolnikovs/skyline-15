{extends "DemoLayout.tpl"}

{block name=config}
    {$Title = $page['Text']['page_title']|escape:'html'}
    {$PageId = $ClosedJobsPage}
{/block}


{block name=afterJqueryUI}
{/block}


{block name=scripts}

<script type="text/javascript" src="{$_subdomain}/js/TableTools.min.js"></script>
<script type="text/javascript" src="{$_subdomain}/js/datatables.api.js"></script>
<script type="text/javascript" src="{$_subdomain}/js/functions.js"></script>


<script>
    
    
    var table;
    
    
    $(document).ready(function() {
	
	
	table = $("#reportTable").dataTable({
            bAutoWidth:	false,
            aoColumns: [
		{ bVisible: false },
                { sWidth: "30%" },
                { sWidth: "65%" },
		{ sWidth: "5%", bSortable: false }
            ],
            bDestroy:           true,
            bStateSave:         false,
            bServerSide:        false,
            bProcessing:        false,
            htmlTableId:        "reportTable",
            sDom:               "ft<'#dataTables_child'>Trpli",
            sPaginationType:    "full_numbers",
            bPaginate:          true,
            bSearch:            false,
            iDisplayLength:     10,
	    aLengthMenu:	[[10, 25, 50, 100, -1],[10, 25, 50, 100, "All"]],
            sAjaxSource:        "{$_subdomain}/Report/getUserReports",
            oTableTools: {
                sRowSelect: "single",
                aButtons: [
                    {
                        sExtends:	"text",
                        sButtonText:    "Run",
			fnInit: function(nButton, oConfig) {
			    $(nButton).attr("style", "width:60px; margin-right:10px;");
			},
                        fnClick: function(nButton, oConfig, oFlash) { 
			    document.body.style.cursor = "wait";
			    var id;
			    var tbl = TableTools.fnGetInstance("reportTable");
			    var highlighted = getSelected();
			    if (highlighted.length == 0) {
				alert("nothing is selected");
				return false;
			    } else if (highlighted.length > 1) {
				alert("You have selected multiple reports. You can only run one report at a time.");
				return false;
			    } else {
				id = table.fnGetData(highlighted[0])[0];
			    }
			    
			    $.ajax({
				type:   "POST",
				url:    "{$_subdomain}/Report/getDateRangeModal",
				data:   { id: id }
			    }).done(function(response) {
				document.body.style.cursor = "wait";
				$.colorbox({
				    html :	response,
				    width:	"600px",
				    scrolling:	false,
				    onComplete: function() {
					document.body.style.cursor = "default";
					$("input[type=text]").datepicker({ 
					    dateFormat:	"dd/mm/yy",
					    changeMonth: true,
					    changeYear: true,
					    onClose: function(dateText, inst) {
					    }
					});
				    },
				    onClosed: function() {
					//kill modal event handlers
					$("input[type=text]").die();
					$("#run, #cancel").die();
				    }
				});
				return false;
			    });
			    
			    return false; 
			    
			    
			    /*
			    document.body.style.cursor = "wait";
			    var tbl = TableTools.fnGetInstance("reportTable");
			    var selected = table.fnGetData(tbl.fnGetSelected()[0]);
			    $.post("{$_subdomain}/Report/runUserReport", { reportID: selected[0] }, function(response) {
				var data = JSON.parse(response);
				document.body.style.cursor = "default";
				location.href = "{$_subdomain}/reports/" + data + ".xlsx";
			    });
			    */
			    
			}
                    },
                    {
                        sExtends:	"text",
                        sButtonText:    "Create",
			fnInit: function(nButton, oConfig) {
			    $(nButton).attr("style","width:60px; margin-right:10px;");
			},
                        fnClick: function(nButton, oConfig, oFlash) { 
			    location.href = "{$_subdomain}/Report/createUserReport";
			}
                    },
                    {
                        sExtends:	"text",
                        sButtonText:    "Edit",
			fnInit: function(nButton, oConfig) {
			    $(nButton).attr("style","width:60px; margin-right:10px;");
			},
                        fnClick: function(nButton, oConfig, oFlash) { 
			    var highlighted = getSelected();
			    if (highlighted.length == 0) {
				alert("Nothing is selected");
				return false;
			    } else if (highlighted.length > 1) {
				alert("You have selected multiple reports. You can only edit one report at a time.");
				return false;
			    } else {
				location.href = "{$_subdomain}/Report/editUserReport/?id=" + table.fnGetData(highlighted[0])[0];
			    }
			}
                    },
                    {
                        sExtends:	"text",
                        sButtonText:    "Delete",
			fnInit: function(nButton, oConfig) {
			    $(nButton).attr("style","width:60px;");
			},
                        fnClick: function(nButton, oConfig, oFlash) { 
			    var selected = getSelected();
			    //console.log(selected);
			    if (selected) {
				var html = "<p>Are you sure you want to delete selected reports?</p>\
					    <div style='text-align:center; margin-top:15px;'>\
						<a href='#' class='btnConfirm' style='width:50px; margin-right:10px;' id='confirmDelete'>Yes</a>\
						<a href='#' class='btnCancel' style='width:50px;' id='cancelDelete'>No</a>\
					    </div>\
					   ";
				$.colorbox({
				    html :	    html, 
				    width:	    "300px",
				    scrolling:	    false,
				    overlayClose:   false,
				    onComplete: function() {
					
				    },
				    onClosed: function() { 
					
				    }
				});
			    } else {
				alert("You must select at least one report.");
			    }
			}
                    }
                ],
                fnRowSelected: function(node) { }
            },
	    oLanguage: {
		sSearch: "<span id='searchLabel' style='float:left; top:8px; position:relative;'>Search within results:</span>&nbsp;"
	    },
	    fnInitComplete: function() {
		var html = '<a href="#" id="delSearch" style="top:14px; right:4px; position:absolute;">\
				<img style="position:relative; zoom:1; bottom:4px;" src="{$_subdomain}/css/Skins/skyline/images/close.png">\
			    </a>';
		$(html).insertAfter(".dataTables_filter input");
		$(document).on("click", "#delSearch", function() {
		    $(".dataTables_filter input").val("");
		    table.fnFilter("");
		    return false;
		});
	    },
	    fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
		$($(nRow).find("td")[2]).html("<input type='checkbox' />");
		$($(nRow).find("td")[2]).css("text-align", "center");
	    }
	});
	
	
	$(document).on("dblclick", "tr", function() {
	    location.assign("{$_subdomain}/Report/editUserReport/?id=" + table.fnGetData(this)[0]);
	});
	
	
	$(document).on("click", "#confirmDelete", function() {
	    document.body.style.cursor = "wait";
	    var selected = getSelected();
	    var ids = [];
	    for (var i = 0; i < selected.length; i++) {
		ids.push(table.fnGetData(selected[i])[0]);
	    }	    
	    $.ajax({
		type:   "POST",
		url:    "{$_subdomain}/Report/deleteReports",
		data:   { reports: JSON.stringify(ids) }
	    }).done(function(response) {
		$.colorbox.close();
		document.body.style.cursor = "default";
		table.fnReloadAjax(null, null, null, true);
	    });
	    return false;
	});
	
	
	$(document).on("click", "#cancelDelete", function() {
	    $.colorbox.close();
	    return false;
	});
	
	
	function getSelected() {
	    var selected = [];
	    var rows = table.fnGetNodes();
	    for (var i = 0; i < rows.length; i++) {
		if($($(rows[i]).find("td").toArray()[2]).find("input").is(":checked")) {
		    selected.push(rows[i]);
		}
	    }
	    if (selected.length == 0) {
		var tbl = TableTools.fnGetInstance("reportTable");
		var highlighted = tbl.fnGetSelected();
		if (highlighted.length == 0) {
		    return false;
		} else {
		    return [highlighted[0]];
		}
	    } else {
		return selected;
	    }
	}
	
	
    });
    
    
</script>

{/block}


{block name=body}


<div class="breadcrumb">
    <div>
        <a href="{$_subdomain}/index/index">{$page['Text']['home_page']|escape:'html'}</a> / &nbsp;{$page['Text']['page_title']|escape:'html'}
    </div>
</div>

{include file='include/menu.tpl'}


<div id="wrapper" style="display:block; position:relative; float:left; margin-top:20px; width:100%;">

    <fieldset style="margin-bottom:20px;">
	<legend>Report Generator</legend>
	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis urna tortor, egestas in rutrum eget, dignissim ut metus. 
	    Donec vitae risus non justo convallis fringilla a at leo. Cras sagittis. 
	</p>
    </fieldset>
    
    <table class="browse dataTable" id="reportTable" style="width:100%;">
	<thead>
	    <tr>
		<th></th>
		<th>Name</th>
		<th>Description</th>
		<th>Select</th>
	    </tr>
	</thead>
	<tbody>
	</tbody>
    </table>
    
</div>
    
                
{/block}
