{extends "DemoLayout.tpl"}


{block name=config}
    {$Title = $page['Text']['page_title']|escape:'html'}
    {$PageId = $TownAllocations}
{/block}
{block name=afterJqueryUI}
    <script type="text/javascript" src="{$_subdomain}/js/jquery.combobox.js"></script> 
    <link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/style.css" type="text/css" />
    <style type="text/css" >
    .ui-combobox-input {
        width:220px;
    }
    </style>
{/block}

{block name=scripts}

    {*<link rel="stylesheet" href="{$_subdomain}/css/colorbox/colorbox.css" type="text/css" charset="utf-8" />*} 
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.jlabel-1.3.min.js"></script>*} 
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.form.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.validate.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/additional-methods.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.dataTables.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.colorbox-min.js"></script>*} 
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.dataTablesPCCS.js"></script>*}
    

    <script type="text/javascript">
    
     var $statuses = [
                    {foreach from=$statuses item=st}
                       ["{$st.Name}", "{$st.Code}"],
                    {/foreach}
                    ]; 
                    
        
    function gotoEditPage($sRow)
    {
        
         
        $('#updateButtonId').removeAttr('disabled').removeClass('gplus-blue-disabled').addClass('gplus-blue');
        $('#updateButtonId').trigger('click');
       
       
    }

    /**
    *  This is used to change color of the row if its in-active.
    */
    function inactiveRow(nRow, aData)
    {
          
        if (aData[8]==$statuses[1][1])
        {  
            $(nRow).addClass("inactive");

            $('td:eq(7)', nRow).html( $statuses[1][0] );
        }
        else
        {
            $(nRow).addClass("");
            $('td:eq(7)', nRow).html( $statuses[0][0] );
        }
        
    }
    
    
    function gotoInsertPage($sRow)
    {
       
        $("#copyRowId").val($sRow[0]);
        $('#addButtonId').trigger('click');
        $("#copyRowId").val('');
        
        
    }
    

    $(document).ready(function() {
        $("#nId").combobox({
            change: function() {
                $(location).attr('href', '{$_subdomain}/JobAllocation/townAllocations/'+urlencode($("#nId").val())+"//"+urlencode($("#cId").val()));
            }
        });
        $("#mId").combobox({
            change: function() {
                $(location).attr('href', '{$_subdomain}/JobAllocation/townAllocations/'+urlencode($("#nId").val())+'/'+urlencode($("#mId").val())+"/"+urlencode($("#cId").val()));
            }
        });
        $("#cId").combobox({
            change: function() {
                $(location).attr('href', '{$_subdomain}/JobAllocation/townAllocations/'+urlencode($("#nId").val())+'/'+urlencode($("#mId").val())+"/"+urlencode($("#cId").val()));
            }
        });


                  //Click handler for finish button.
                    $(document).on('click', '#finish_btn', 
                                function() {

                                
                                     $(location).attr('href', '{$_subdomain}/SystemAdmin/index/JobAllocation');

                                });

                                      
                                      
                      {if $nId eq ''} 
                              
                          $("#cId").attr("disabled","disabled");    
                          $("#mId").attr("disabled","disabled"); 
                      {/if}

                      

                     /* Add a change handler to the network dropdown - strats here*/
                        /*$(document).on('change', '#nId', 
                            function() {
                                
                            
                                $(location).attr('href', '{$_subdomain}/JobAllocation/townAllocations/'+urlencode($("#nId").val())+"//"+urlencode($("#cId").val())); 
                            }      
                        );*/
                      /* Add a change handler to the network dropdown - ends here*/
                      
                        /* Add a change handler to the manufactrer dropdown - strats here*/
                        /*$(document).on('change', '#mId', 
                            function() {
                                
                                $(location).attr('href', '{$_subdomain}/JobAllocation/townAllocations/'+urlencode($("#nId").val())+'/'+urlencode($("#mId").val())+"/"+urlencode($("#cId").val())); 
                            }      
                        );*/
                       /* Add a change handler to the manufactrer dropdown - ends here*/
                     
              
                       /* Add a change handler to the country dropdown - strats here*/
                        /*$(document).on('change', '#cId', 
                            function() {
                                
                                $(location).attr('href', '{$_subdomain}/JobAllocation/townAllocations/'+urlencode($("#nId").val())+'/'+urlencode($("#mId").val())+"/"+urlencode($("#cId").val())); 
                            }      
                        );*/
                       /* Add a change handler to the country dropdown - ends here*/
                     
              
              
              

                    /* =======================================================
                    *
                    * set tab on return for input elements with form submit on auto-submit class...
                    *
                    * ======================================================= */

                    $('input[type=text],input[type=password]').keypress( function( e ) {
                            if (e.which == 13) {
                                $(this).blur();
                                if ($(this).hasClass('auto-submit')) {
                                    $('.auto-hint').each(function() {
                                        $this = $(this);
                                        if ($this.val() == $this.attr('title')) {
                                            $this.val('').removeClass('auto-hint').addClass('auto-hint-hide');
                                            if ($this.hasClass('auto-pwd')) {
                                                $this.prop('type','password');
                                            }
                                        }
                                    } );
                                    $(this).get(0).form.onsubmit();
                                } else {
                                    $next = $(this).attr('tabIndex') + 1;
                                    $('[tabIndex="'+$next+'"]').focus();
                                }
                                return false;
                            }
                        } );        



                    

                      var  displayButtons = "UPA";
                     
                     
                         
                         
                         
                       
                           
                    $columnsList    = [ 
                                                /* TownAllocationID */  {  "bVisible":    false },    
                                               
                                                /* County */   null,
                                                /* Town */   null,
                                                /* Repair Skill */   null,
                                                /* Client */   null,
                                                /* Job type */   null,
                                                /* Service Type */   null,
                                                /* Service Centre */   null,
                                                /* Status */  null
                                                
                                        ];
                          
                         
                       
     
                     
                   
                    
                    $('#TownAllocationsResults').PCCSDataTable( {
                              "aoColumns": $columnsList,
                               
                            displayButtons:  displayButtons,
                            addButtonId:     'addButtonId',
                            addButtonText:   '{$page['Buttons']['insert']|escape:'html'}',
                            createFormTitle: '{$page['Text']['insert_page_legend']|escape:'html'}',
                            createAppUrl:    '{$_subdomain}/JobAllocation/townAllocations/insert/'+urlencode($("#nId").val())+'/'+urlencode($("#mId").val())+"/"+urlencode($("#cId").val())+"/",
                            createDataUrl:   '{$_subdomain}/JobAllocation/ProcessData/TownAllocations/',
                            createFormFocusElementId:'NetworkID',
                            formInsertButton:'insert_save_btn',
                            copyInsertRowId: 'copyRowId',
                            
                            
                            frmErrorRules:   {
                                                    
                                                    NetworkID:
                                                        {
                                                            required: true
                                                        },
                                                    CountryID:
                                                        {
                                                            required: true
                                                        },
                                                    ManufacturerID:
                                                        {
                                                            required: true
                                                        },
                                                    CountyID:
                                                        {
                                                           required: true
                                                        }      
                                                        
                                                        
                                             },
                                                
                           frmErrorMessages: {
                                                
                                                    NetworkID:
                                                        {
                                                            required: "{$page['Errors']['network']|escape:'html'}"
                                                        },
                                                    CountryID:
                                                        {
                                                             required: "{$page['Errors']['country']|escape:'html'}"
                                                        },
                                                    ManufacturerID:
                                                        {
                                                             required: "{$page['Errors']['manufacturer']|escape:'html'}"
                                                        },
                                                    CountyID:
                                                        {
                                                            required: "{$page['Errors']['county']|escape:'html'}"
                                                        }     
                                                     
                                              },                     
                            
                            popUpFormWidth:  750,
                            popUpFormHeight: 430,
                            
                            pickButtonId:"copy_btn",
                            pickButtonText:"{$page['Buttons']['copy']|escape:'html'}",
                            pickCallbackMethod: "gotoInsertPage",
                            colorboxForceClose: false,
                           
                            
                            
                            updateButtonId:  'updateButtonId',
                            updateButtonText:'{$page['Buttons']['edit']|escape:'html'}',
                            updateFormTitle: '{$page['Text']['update_page_legend']|escape:'html'}',
                            updateAppUrl:    '{$_subdomain}/JobAllocation/townAllocations/update/',
                            updateDataUrl:   '{$_subdomain}/JobAllocation/ProcessData/TownAllocations/',
                            formUpdateButton:'update_save_btn',
                            updateFormFocusElementId:'NetworkID',
                            
                            colorboxFormId:  "TownAllocationsForm",
                            frmErrorMsgClass:"fieldError",
                            frmErrorElement: "label",
                            htmlTablePageId: 'TownAllocationsResultsPanel',
                            htmlTableId:     'TownAllocationsResults',
                            fetchDataUrl:    '{$_subdomain}/JobAllocation/ProcessData/TownAllocations/fetch/'+urlencode("{$nId}")+"/"+urlencode("{$mId}")+"/"+urlencode("{$cId}"),
                            formCancelButton:'cancel_btn',
                            dblclickCallbackMethod: 'gotoEditPage',
                            fnRowCallback:          'inactiveRow',
                            searchCloseImage:'{$_subdomain}/css/Skins/{$_theme}/images/close.png',
                            tooltipTitle:    "{$page['Text']['tooltip_title']|escape:'html'}",
                            iDisplayLength:  25,
                            formDataErrorMsgId: "suggestText",
                            frmErrorSugMsgClass:"formCommonError",
                            sDom: 'ft<"#dataTables_command">rpli',
                            bottomButtonsDivId:'dataTables_command'


                        });
                      

                        
                   

    });

    </script>

    {/block}

    {block name=body}

    <div class="breadcrumb">
        <div>

            <a href="{$_subdomain}/SystemAdmin" >{$page['Text']['system_admin_home_page']|escape:'html'}</a> / <a href="{$_subdomain}/SystemAdmin/index/jobAllocation" >{$page['Text']['job_allocation']|escape:'html'}</a> / {$page['Text']['page_title']|escape:'html'}

        </div>
    </div>



    <div class="main" id="home" >

               <div class="ServiceAdminTopPanel" >
                    <form id="TownAllocationsTopForm" name="TownAllocationsTopForm" method="post"  action="#" class="inline" >

                        <fieldset>
                        <legend title="" >{$page['Text']['legend']|escape:'html'}</legend>
                        <p>
                            <label>{$page['Text']['description']|escape:'html'}</label>
                        </p> 

                        </fieldset> 


                    </form>
                </div>  


                <div class="ServiceAdminResultsPanel" id="TownAllocationsResultsPanel" >
                    <form id="nIdForm">
                        {if $SupderAdmin eq true} 
                            {$page['Labels']['network_label']|escape:'html'}
                            <select name="nId" class="topDropDownList" id="nId" >
                                <option value="" {if $nId eq ''}selected="selected"{/if}>{$page['Text']['select_service_network']|escape:'html'}</option>

                                {foreach $networks as $network}

                                    <option value="{$network.NetworkID}" {if $nId eq $network.NetworkID}selected="selected"{/if}>{$network.CompanyName|escape:'html'}</option>

                                {/foreach}
                            </select>
                              

                        {else}

                            <input type="hidden" name="nId" id="nId" value="{$nId|escape:'html'}" >

                        {/if} 
                        {$page['Labels']['manufacturer_label']|escape:'html'}
                        <select name="mId" class="topDropDownList" id="mId" >
                            <option value="" {if $mId eq ''}selected="selected"{/if}>{$page['Text']['select_manufacturer']|escape:'html'}</option>

                            {foreach $manufacturers as $manufacturer}

                                <option value="{$manufacturer.ManufacturerID}" {if $mId eq $manufacturer.ManufacturerID}selected="selected"{/if}>{$manufacturer.ManufacturerName|escape:'html'}</option>

                            {/foreach}
                        </select> 
                        
                       {$page['Labels']['country_label']|escape:'html'}  
                       <select name="cId" class="topDropDownList" id="cId" >
                            <option value="" {if $cId eq ''}selected="selected"{/if}>{$page['Text']['select_country']|escape:'html'}</option>
                            
                            {foreach $countries as $country}

                                <option value="{$country.CountryID}" {if $cId eq $country.CountryID}selected="selected"{/if}>{$country.Name|escape:'html'}</option>

                            {/foreach}
                        </select> 
                        
                     </form>
                    
                        
                        
                    <form id="TownAllocationsResultsForm" class="dataTableCorrections">
                        <table id="TownAllocationsResults" border="0" cellpadding="0" cellspacing="0" class="browse" >
                            <thead>
                                    <tr>
                                            <th></th> 
                                            <th title="{$page['Text']['county']|escape:'html'}" >{$page['Text']['county']|escape:'html'}</th>
                                            <th title="{$page['Text']['town']|escape:'html'}" >{$page['Text']['town']|escape:'html'}</th>
                                            <th title="{$page['Text']['repair_skill']|escape:'html'}" >{$page['Text']['repair_skill']|escape:'html'}</th>
                                            <th title="{$page['Text']['client']|escape:'html'}" >{$page['Text']['client']|escape:'html'}</th>
                                            <th title="{$page['Text']['job_type']|escape:'html'}" >{$page['Text']['job_type']|escape:'html'}</th>
                                            <th title="{$page['Text']['service_type']|escape:'html'}" >{$page['Text']['service_type']|escape:'html'}</th>
                                            <th title="{$page['Text']['service_centre']|escape:'html'}" >{$page['Text']['service_centre']|escape:'html'}</th>
                                            <th title="{$page['Text']['status']|escape:'html'}"  >{$page['Text']['status']|escape:'html'}</th>
                                    </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>  
                        
                     </form>
                </div>        

                <div class="bottomButtonsPanel" >
                    <hr>
                    <button id="finish_btn" type="button" class="gplus-blue rightBtn" ><span class="label">{$page['Buttons']['finish']|escape:'html'}</span></button>
                    
                </div>        
                 <div class="centerInfoText" id="centerInfoText" style="display:none;" ></div>
               <input type="hidden" name="copyRowId" id="copyRowId" > 
                 
    </div>
                        
                        



{/block}



