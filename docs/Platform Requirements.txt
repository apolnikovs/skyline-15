Desktop Requirements

The desktop platform should be of adequate standard to support your chosen browser client (see browser specification for specific hardware/software requirements).
Skyline has been tested with Microsoft Windows 7, Windows Vista, Windows XP, Apple MAC 10.6, 10.7, 10.8, Linux Fedora 16.

Browser Requirements

The Skyline Application Platform has been tested with the following web browsers.  

Internet Explorer 9.0
Internet Explorer 8.0
Internet Explorer 7.0
Internet Explorer 6.0

Firefox 11.0

Chrome 22.0

Opera 12.0

Safari 6.0

Note:  If you choose to use a web browser which is not fully supported by Skyline and encounter a 
problem, please let us know.   

Bandwidth Requirements

It is recommended that a minimum of 56 Kbps (kilobits per second) of data transfer bandwidth is available 
when accessing the Skyline application platform.

Display Resolution
A minimum desktop resolution of 1024 x 768 pixels is recommended to prevent unnecessary scrolling.  



